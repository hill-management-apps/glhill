﻿using HelpfulClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GLHill.Forms
{
    public partial class frmBankDept03 : Form
    {
        #region "Fields"
        private frmBankDept _frmBankDept;
        private DataGridViewRow _theRow;
        #endregion

        #region "Constructors"
        public frmBankDept03(frmBankDept frmBankDept, DataGridViewRow theRow = null)
        {
            InitializeComponent();

            _frmBankDept = frmBankDept;
            _theRow = theRow;
        }
        #endregion

        #region "Event Handlers"
        private void frmBankDept03_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + " (MS SQL Server: " + Form1.sqlTransactions.GetDatabase().Trim() + ")";

            if (DateTime.TryParse(_theRow.Cells["ACTDATE"].Value.ToString(), out DateTime actDate))
            {
                dtpActualDepositDate.Value = actDate;
            }
            if (_theRow.Cells["BANKREF"].Value != DBNull.Value)
            {
                txtRefRecNum.Text = _theRow.Cells["BANKREF"].Value.ToString();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Wait();

                Dictionary<string, string> updateValues = new Dictionary<string, string>();
                Dictionary<string, string> whereValues = new Dictionary<string, string>();

                whereValues.Add("DEPOSITID", _theRow.Cells["DEPOSITID"].Value.ToString().Trim());

                updateValues = new Dictionary<string, string>();
                updateValues.Add("USERID", HelpfulFunctions.UserID.Trim());
                updateValues.Add("LASTDATE", DateTime.Now.ToString("yyyy-MM-dd"));
                updateValues.Add("ACTDATE", dtpActualDepositDate.Value.ToString("yyyy-MM-dd"));
                updateValues.Add("BANKREF", txtRefRecNum.Text.Trim());

                Form1.sqlTransactions.UpdateTableData("HS_BANKDEP", updateValues, whereValues);

                _frmBankDept.RefreshForm(null, null);
                this.Useable();
                this.Close();
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                this.Useable();
            }
        }
        #endregion
    }
}
