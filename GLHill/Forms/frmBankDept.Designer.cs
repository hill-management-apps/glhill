﻿
namespace GLHill.Forms
{
    partial class frmBankDept
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvDeposits = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Previewbutton = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cboProperty = new System.Windows.Forms.ComboBox();
            this.cboDateRanges = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.radIntDate = new System.Windows.Forms.RadioButton();
            this.radBsnDate = new System.Windows.Forms.RadioButton();
            this.txtDepositId = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeposits)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvDeposits
            // 
            this.dgvDeposits.AllowUserToAddRows = false;
            this.dgvDeposits.AllowUserToDeleteRows = false;
            this.dgvDeposits.AllowUserToResizeRows = false;
            this.dgvDeposits.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvDeposits.Location = new System.Drawing.Point(12, 95);
            this.dgvDeposits.MultiSelect = false;
            this.dgvDeposits.Name = "dgvDeposits";
            this.dgvDeposits.ReadOnly = true;
            this.dgvDeposits.RowHeadersVisible = false;
            this.dgvDeposits.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDeposits.ShowCellErrors = false;
            this.dgvDeposits.ShowEditingIcon = false;
            this.dgvDeposits.ShowRowErrors = false;
            this.dgvDeposits.Size = new System.Drawing.Size(1415, 444);
            this.dgvDeposits.TabIndex = 9;
            this.dgvDeposits.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDeposits_CellClick);
            this.dgvDeposits.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvDeposits_CellFormatting);
            this.dgvDeposits.SelectionChanged += new System.EventHandler(this.dgvDeposits_SelectionChanged);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.Previewbutton);
            this.panel1.Controls.Add(this.btnPrint);
            this.panel1.Controls.Add(this.btnAdd);
            this.panel1.Location = new System.Drawing.Point(1433, 65);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(95, 474);
            this.panel1.TabIndex = 8;
            // 
            // Previewbutton
            // 
            this.Previewbutton.Location = new System.Drawing.Point(4, 107);
            this.Previewbutton.Name = "Previewbutton";
            this.Previewbutton.Size = new System.Drawing.Size(84, 46);
            this.Previewbutton.TabIndex = 2;
            this.Previewbutton.Text = "Preview Undeposited";
            this.Previewbutton.UseVisualStyleBackColor = true;
            this.Previewbutton.Click += new System.EventHandler(this.previewbutton_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(3, 55);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(84, 46);
            this.btnPrint.TabIndex = 1;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(4, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(84, 46);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(449, 65);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(84, 21);
            this.btnRefresh.TabIndex = 7;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.RefreshForm);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Property:";
            // 
            // cboProperty
            // 
            this.cboProperty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboProperty.FormattingEnabled = true;
            this.cboProperty.Location = new System.Drawing.Point(88, 13);
            this.cboProperty.Name = "cboProperty";
            this.cboProperty.Size = new System.Drawing.Size(293, 21);
            this.cboProperty.TabIndex = 0;
            // 
            // cboDateRanges
            // 
            this.cboDateRanges.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDateRanges.FormattingEnabled = true;
            this.cboDateRanges.Items.AddRange(new object[] {
            "Custom",
            "This Week",
            "Last Week",
            "This Month",
            "Last Month",
            "This Year"});
            this.cboDateRanges.Location = new System.Drawing.Point(88, 40);
            this.cboDateRanges.Name = "cboDateRanges";
            this.cboDateRanges.Size = new System.Drawing.Size(121, 21);
            this.cboDateRanges.TabIndex = 1;
            this.cboDateRanges.SelectedIndexChanged += new System.EventHandler(this.cboDateRanges_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Date Range:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(319, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "To";
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEndDate.Location = new System.Drawing.Point(345, 40);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(98, 20);
            this.dtpEndDate.TabIndex = 3;
            this.dtpEndDate.ValueChanged += new System.EventHandler(this.dateChange);
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpStartDate.Location = new System.Drawing.Point(215, 40);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(98, 20);
            this.dtpStartDate.TabIndex = 2;
            this.dtpStartDate.ValueChanged += new System.EventHandler(this.dateChange);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 26;
            this.label3.Text = "Deposit Id:";
            // 
            // radIntDate
            // 
            this.radIntDate.AutoSize = true;
            this.radIntDate.Checked = true;
            this.radIntDate.Location = new System.Drawing.Point(449, 44);
            this.radIntDate.Name = "radIntDate";
            this.radIntDate.Size = new System.Drawing.Size(93, 17);
            this.radIntDate.TabIndex = 4;
            this.radIntDate.TabStop = true;
            this.radIntDate.Text = "Intended Date";
            this.radIntDate.UseVisualStyleBackColor = true;
            // 
            // radBsnDate
            // 
            this.radBsnDate.AutoSize = true;
            this.radBsnDate.Location = new System.Drawing.Point(548, 44);
            this.radBsnDate.Name = "radBsnDate";
            this.radBsnDate.Size = new System.Drawing.Size(93, 17);
            this.radBsnDate.TabIndex = 5;
            this.radBsnDate.Text = "Business Date";
            this.radBsnDate.UseVisualStyleBackColor = true;
            // 
            // txtDepositId
            // 
            this.txtDepositId.Location = new System.Drawing.Point(88, 67);
            this.txtDepositId.Name = "txtDepositId";
            this.txtDepositId.Size = new System.Drawing.Size(94, 20);
            this.txtDepositId.TabIndex = 6;
            this.txtDepositId.Leave += new System.EventHandler(this.txtDepositId_Leave);
            // 
            // frmBankDept
            // 
            this.AcceptButton = this.btnRefresh;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1538, 551);
            this.Controls.Add(this.txtDepositId);
            this.Controls.Add(this.radBsnDate);
            this.Controls.Add(this.radIntDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cboDateRanges);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtpEndDate);
            this.Controls.Add(this.dtpStartDate);
            this.Controls.Add(this.cboProperty);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dgvDeposits);
            this.MaximizeBox = false;
            this.Name = "frmBankDept";
            this.Text = "SiteLink Bank Deposits";
            this.Load += new System.EventHandler(this.frmBankDept_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeposits)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvDeposits;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.ComboBox cboProperty;
        public System.Windows.Forms.ComboBox cboDateRanges;
        public System.Windows.Forms.DateTimePicker dtpEndDate;
        public System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.Button Previewbutton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton radIntDate;
        private System.Windows.Forms.RadioButton radBsnDate;
        private System.Windows.Forms.TextBox txtDepositId;
    }
}