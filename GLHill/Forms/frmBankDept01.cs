﻿using HelpfulClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GLHill.Forms
{
    public partial class frmBankDept01 : Form
    {
        #region "Fields"
        private const string printOut = @"<html>
                                        <head>
                                            <style type='text/css'>
                                                .tableFilter {
                                                    width: 700px;
                                                    font-family: arial;
                                                    background: #cccccc;
                                                    border: outset 2px black;
                                                }

                                                    .tableFilter .fieldColumn {
                                                        width: 15%;
                                                        text-align: left;
                                                        border: hidden;
                                                    }

                                                .tableHeader {
                                                    width: 700px;
                                                    font-family: arial;
                                                    text-align: center;
                                                    background: #a1a1a1;
                                                    border: outset 2px black;
                                                }

                                                .tableLines {
                                                    width: 700px;
                                                    font-family: arial;
                                                    background: #a1a1a1;
                                                    border: outset 2px black;
                                                    border-collapse: collapse;
                                                }

                                                    .tableLines footer {
                                                    }

                                                    .tableLines td {
                                                        border-right: outset 2px black;
                                                        text-align: center;
                                                        border-spacing: 0px;
                                                        border-collapse: collapse;
                                                        background: #cccccc;
                                                    }

                                                    .tableLines th {
                                                        border-right: outset 2px black;
                                                        text-align: center;
                                                        border-spacing: 0px;
                                                        border-collapse: collapse;
                                                    }

                                                    .tableLines tr {
                                                        border-top: outset 2px black;
                                                        border-bottom: outset 2px black;
                                                        border-spacing: 0px;
                                                        border-collapse: collapse;
                                                    }
                                            </style>
                                        </head>
                                        <body>
                                            <table style='border-collapse:collapse;padding:0px;'>
                                                <tr>
                                                    <td>
                                                        <table class='tableHeader'>
                                                            <tr>
                                                                <th><h3>{0}</h3></th>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table class='tableFilter'>
                                                            <tr>
                                                                <th class='tableFilter fieldColumn'>
                                                                    Property:
                                                                </th>
                                                                <td colspan='3'>
                                                                    {1}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class='tableFilter fieldColumn'>
                                                                    Cash Type:
                                                                </th>
                                                                <td colspan='3'>
                                                                    {2}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class='tableFilter fieldColumn'>
                                                                    Start Date:
                                                                </th>
                                                                <td>
                                                                    {3}
                                                                </td>
                                                                <th class='tableFilter fieldColumn'>
                                                                    End Date:
                                                                </th>
                                                                <td>
                                                                    {4}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class='tableFilter fieldColumn'>
                                                                    Keyed By:
                                                                </th>
                                                                <td colspan='3'>
                                                                    {5}
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table class='tableLines'>
                                                            <tr>
                                                                <th>Receipt Date</th>
                                                                <th>Receipt #</th>
                                                                <th>Check #</th>
                                                                <th>Property</th>
                                                                <th>Amount</th>
                                                                <th>Keyed By</th>
                                                            </tr>
                                                            {6}
                                                            <tr>
                                                                <td colspan='3'></td>
                                                                <th>Total:</th>
                                                                <th>{7}</th>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </body>
                                        </html>";
        private const string printLine = @" <tr>
                                              <td>{0}</td>
                                              <td>{1}</td>
                                              <td>{2}</td>
                                              <td>{3}</td>
                                              <td>{4}</td>
                                              <td>{5}</td>
                                            </tr>";
        private ACTION _action;
        private frmBankDept _frmBankDept;
        private DataGridViewRow _theRow;
        private decimal runningTotal;
        private Boolean modified;
        private int deposit = 0;
        #endregion

        #region "Properties"
        public decimal RunningTotal
        {
            set
            {
                runningTotal = value;
                lblRunningTotal.Text = runningTotal.ToString("c2");
            }
            get
            {
                return runningTotal;
            }
        }
        #endregion

        #region "Enumerations"
        public enum ACTION
        {
            INSERT,
            UPDATE,
            DELETE,
            PREVIEW
        }
        #endregion

        #region "Constructors"
        public frmBankDept01(ACTION action, frmBankDept frmBankDept, DataGridViewRow theRow = null)
        {
            InitializeComponent();

            _frmBankDept = frmBankDept;
            _action = action;
            _theRow = theRow;

            if (theRow != null)
            {
                deposit = int.Parse(theRow.Cells["DEPOSITID"].Value.ToString().Trim());
            }
        }
        #endregion

        #region "Event Handlers"
        private void btnPrintReceipts_Click(object sender, EventArgs e)
        {
            try
            {
                this.Wait();
                decimal amount = 0M;
                //prepares the values to match with fields on the print
                string[] values = new string[8];
                string printLines = "";

                //match the datagridview information
                foreach(DataGridViewRow row in dgvReceipts.Rows)
                {
                    printLines += string.Format(printLine, row.Cells["RECEIPTDAT"].Value.ToString().Trim(), row.Cells["IRECEIPTNU"].Value.ToString().Trim(),
                                                          row.Cells["CKORCHGNO"].Value.ToString().Trim(), row.Cells["SITEID"].Value.ToString().Trim(),
                                                         "$ " + row.Cells["RECEIPTAMT"].Value.ToString().Trim(), row.Cells["KeyedBy"].Value.ToString().Trim());

                    if(decimal.TryParse(row.Cells["RECEIPTAMT"].Value.ToString().Trim(), out decimal receiptAmount))
                    {
                        amount += receiptAmount;
                    }
                    else
                    {
                        amount += 0M;
                    }
                }

                values[0] = "Undeposited Receipts";
                values[1] = cboProperty.Text.Trim();
                values[2] = cboCashType.Text.Trim();
                values[3] = dtpStartDate.Value.ToString("MM/dd/yyyy");
                values[4] = dtpEndDate.Value.ToString("MM/dd/yyyy");
                values[5] = cboKeyedBy.Text.Trim();
                values[6] = printLines.Trim();
                values[7] = amount.ToString("c2");

                string newHtml = printOut;

                //replace the data in HTML
                for (int i = values.Length - 1; i >= 0; i--)
                {
                    newHtml = newHtml.Replace("{" + i + "}", values[i].Trim());
                }

                //write the html to an output file
                File.WriteAllText(@"\\hmfile01.hillmgt.com\Finance\Applications\printDespositSlipListing.html", newHtml);

                //display the print out in chrome
                System.Diagnostics.Process.Start(@"chrome.exe", @"\\hmfile01.hillmgt.com\Finance\Applications\printDespositSlipListing.html");
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            finally
            {
                this.Useable();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveData(sender, e);
            _frmBankDept.RefreshForm(btnSave, null);
            this.Close();
        }

        private void cboDateRanges_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboDateRanges.Text.Trim() != "Custom")
                {
                    int startDay;
                    int endDay;

                    switch (cboDateRanges.Text.Trim().ToUpper())
                    {
                        case "THIS WEEK":
                            startDay = DayOfWeek.Sunday - DateTime.Now.Date.DayOfWeek;
                            endDay = DayOfWeek.Saturday - DateTime.Now.Date.DayOfWeek;

                            dtpStartDate.Value = DateTime.Now.AddDays(startDay);
                            dtpEndDate.Value = DateTime.Now.AddDays(endDay);
                            break;
                        case "LAST WEEK":
                            startDay = (DayOfWeek.Sunday - DateTime.Now.Date.DayOfWeek) - 7;
                            endDay = (DayOfWeek.Saturday - DateTime.Now.Date.DayOfWeek) - 7;

                            dtpStartDate.Value = DateTime.Now.AddDays(startDay);
                            dtpEndDate.Value = DateTime.Now.AddDays(endDay);
                            break;
                        case "THIS MONTH":
                            startDay = -1 * (DateTime.Now.Date.Day - 1);
                            endDay = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) - DateTime.Now.Date.Day;

                            dtpStartDate.Value = DateTime.Now.AddDays(startDay);
                            dtpEndDate.Value = DateTime.Now.AddDays(endDay);
                            break;
                        case "LAST MONTH":
                            startDay = -1 * ((DateTime.Now.Day) + DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
                            endDay = (DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) - DateTime.Now.Date.Day) - DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);

                            dtpStartDate.Value = DateTime.Now.AddDays(startDay);
                            dtpEndDate.Value = DateTime.Now.AddDays(endDay);
                            break;
                        case "THIS YEAR":
                            startDay = -1 * (DateTime.Now.DayOfYear - 1);
                            endDay = 365 - DateTime.Now.DayOfYear;

                            dtpStartDate.Value = DateTime.Now.AddDays(startDay);
                            dtpEndDate.Value = DateTime.Now.AddDays(endDay);
                            break;
                    }
                }
                else
                {
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void dateChange(object sender, EventArgs e)
        {
            try
            {
                //determine if the control that raised the event is a DateTimePicker
                if (sender.GetType() == typeof(DateTimePicker))
                {
                    //set the control a local variable
                    DateTimePicker control = (DateTimePicker)sender;

                    //check if the control is the End Date
                    if (control.Name == dtpEndDate.Name)
                    {
                        //if the start date is greater than the end date
                        if (dtpStartDate.Value > dtpEndDate.Value)
                        {
                            //set the start date back 7 days from the end date
                            dtpStartDate.Value = dtpEndDate.Value.AddDays(-7);
                        }
                    }//check if the control is the Start Date
                    else if (control.Name == dtpStartDate.Name)
                    {
                        //if the start date is greater than the end date
                        if (dtpStartDate.Value > dtpEndDate.Value)
                        {
                            //set the end date to 7 days after the start date
                            dtpEndDate.Value = dtpStartDate.Value.AddDays(7);
                        }
                    }
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void dgvReceipts_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                //if there is at least one record in the grid go into the code
                if (dgvReceipts.SelectedRows.Count > 0)
                {
                    List<DataGridViewRow> rowsCash = new List<DataGridViewRow>();
                    List<DataGridViewRow> rowsSelected = new List<DataGridViewRow>();
                    decimal ovramt;
                    decimal receiptamt;
                    String ovrckorchgno;
                    DataGridViewRow firstRow;
                    MenuItem mnuCrtAmt;
                    MenuItem mnuNeverDeposited;
                    string menutitle;
                    DialogResult dialog;

                    rowsCash = (from DataGridViewRow r in dgvReceipts.SelectedRows
                                where r.Cells["SCATEGORY"].Value.ToString().Trim() == "Cash"
                                select r).ToList();

                    rowsSelected = (from DataGridViewRow r in dgvReceipts.SelectedRows
                                    select r).ToList();

                    firstRow = rowsSelected[0];

                    Prompt promptSetCheckNum = new Prompt("Please enter a check number to set the ones missing the check number.", "Please enter a check number for the one missing.");
                    ovrckorchgno = firstRow.Cells[dgvReceipts.Columns["OVRCkorChgNo"].Index].Value.ToString();
                    if (ovrckorchgno == "")
                    {
                        menutitle = "Enter the new Check Number";
                    }
                    else
                    {
                        menutitle = "Remove Check Number Override";
                    }

                    //create dynamically Set Check Number menu item with delegate methods for each record in grid
                    MenuItem mnuSetChkNum = new MenuItem(menutitle, delegate
                    {
                        if (ovrckorchgno == "")
                        {
                            promptSetCheckNum.ShowDialog(this);
                            if (promptSetCheckNum.Result == DialogResult.OK)
                            {
                                modified = true;
                                foreach (DataGridViewRow row in rowsSelected)
                                {
                                    row.Cells["ovrckorchgno"].Value = promptSetCheckNum.Response.Trim();
                                }
                                dgvReceipts.Refresh();
                            }
                        }
                        else
                        {
                            dialog = MessageBox.Show("Are you sure you want to clear this check # ?", "Attention!!!", MessageBoxButtons.YesNo,
                                                                  MessageBoxIcon.Question);
                            if (dialog == DialogResult.Yes)
                            {
                                modified = true;
                                foreach (DataGridViewRow row in rowsSelected)
                                {
                                    row.Cells["ovrckorchgno"].Value = DBNull.Value;
                                }
                                dgvReceipts.Refresh();
                            }
                        }
                    });

                    Prompt promptCrtAmt;

                    try
                    {
                        ovramt = decimal.Parse(firstRow.Cells[dgvReceipts.Columns["OVRAMT"].Index].Value.ToString());
                    }
                    catch { ovramt = 0; }
                    try
                    {
                        receiptamt = decimal.Parse(firstRow.Cells[dgvReceipts.Columns["RECEIPTAMT"].Index].Value.ToString());
                    }
                    catch { receiptamt = 0; }

                    if (ovramt == 0)
                    {
                        menutitle = "Set the Deposit Amount";
                    }
                    else
                    {
                        menutitle = "Remove Amount Override";
                    }

                    //create dynamically the Corrected Amount menu item with a delegate method for each record in grid
                    mnuCrtAmt = new MenuItem(menutitle, delegate
                    {
                        Decimal overrideAmount = 0;
                    tryAgain:
                        if (ovramt == 0)
                        {
                            promptCrtAmt = new Prompt("Set the correct amount for this transaction.", "Please set the correct amount for this transaction.");
                            promptCrtAmt.ShowDialog(this);
                            dialog = promptCrtAmt.Result;
                            if (dialog == DialogResult.OK)
                            {
                                if (Decimal.TryParse(promptCrtAmt.Response.Trim(), out decimal o))
                                {
                                    overrideAmount = o;
                                }
                                else
                                {
                                    MessageBox.Show("You must enter a numeric value.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    goto tryAgain;
                                }
                            }
                        }
                        else
                        {
                            dialog = MessageBox.Show("Are you sure you want to clear this amount override?", "Attention!!!", MessageBoxButtons.YesNo,
                                                                  MessageBoxIcon.Question);
                        }
                        if (dialog == DialogResult.OK || dialog == DialogResult.Yes)
                        {
                            modified = true;
                            foreach (DataGridViewRow row in rowsSelected)
                            {
                                if (ovramt == 0 && dialog == DialogResult.OK)
                                {
                                    row.Cells["OVRAMT"].Value = overrideAmount;
                                    row.Cells["NETAMT"].Value = overrideAmount;
                                }
                                else if (dialog == DialogResult.Yes)
                                {
                                    row.Cells["OVRAMT"].Value = DBNull.Value;
                                    row.Cells["NETAMT"].Value = receiptamt;
                                }
                            }
                            dgvReceipts.Refresh();
                            RunningTotal = dgvReceipts_CalcRunningTotal();
                        }
                    }
                );
                    int NotDeposited;
                    int.TryParse(firstRow.Cells["NOTDEPOSITED"].Value.ToString(), out NotDeposited);
                    if (NotDeposited == 0)
                    {
                        menutitle = "Set item as not deposited";
                    }
                    else
                    {
                        menutitle = "Clear not deposited flag";

                    }

                    //create dynamically the never deposited menu item with delegate methods for each record in grid
                    mnuNeverDeposited = new MenuItem(menutitle, delegate
                    {
                        if (NotDeposited != 0)
                        {
                            dialog = MessageBox.Show("Are you sure you want to clear the Not Deposited Flag?", "Attention!!!", MessageBoxButtons.YesNo,
                                                                  MessageBoxIcon.Question);
                            if (dialog == DialogResult.Yes)
                            {
                                modified = true;
                                foreach (DataGridViewRow row in rowsSelected)
                                {
                                    row.Cells["NOTDEPOSITED"].Value = DBNull.Value;
                                    row.Cells["NOTE"].Value = "";
                                    row.Cells["NETAMT"].Value = row.Cells["RECEIPTAMT"].Value;

                                }
                                dgvReceipts.Refresh();
                                RunningTotal = dgvReceipts_CalcRunningTotal();
                            }

                        }
                        else
                        {
                            Prompt promptNeverDeposited = new Prompt("Please enter a note about why this item was not deposited.", "Please enter a note.");
                            promptNeverDeposited.ShowDialog(this);
                            if (promptNeverDeposited.Result == DialogResult.OK)
                            {
                                modified = true;
                                foreach (DataGridViewRow row in rowsSelected)
                                {
                                    row.Cells["NOTDEPOSITED"].Value = "1";
                                    row.Cells["NOTE"].Value = promptNeverDeposited.Response.Trim();
                                    row.Cells["NETAMT"].Value = "0";
                                }
                                RunningTotal = dgvReceipts_CalcRunningTotal();
                                dgvReceipts.Refresh();
                            }
                        }
                    }
                );

                    //if the user does a right click and the form is not in preview mode
                    if (e.Button == MouseButtons.Right & _action != ACTION.PREVIEW)
                    {
                        //display addition options for updating selected deposit lines
                        ContextMenu menu = new ContextMenu();
                        menu.MenuItems.Add(mnuSetChkNum);
                        menu.MenuItems.Add(mnuCrtAmt);
                        menu.MenuItems.Add(mnuNeverDeposited);
                        menu.Show((Control)sender, e.Location);
                    }
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void dgvReceipts_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    if (e.ColumnIndex == dgvReceipts.Columns["SelBtn"].Index)
                    {
                        modified = true;
                        if (int.Parse(dgvReceipts.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()) == 1)
                        {
                            dgvReceipts.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;
                            RunningTotal -= decimal.Parse(dgvReceipts.Rows[e.RowIndex].Cells[dgvReceipts.Columns["NETAMT"].Index].Value.ToString());
                        }
                        else
                        {
                            dgvReceipts.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 1;
                            RunningTotal += decimal.Parse(dgvReceipts.Rows[e.RowIndex].Cells[dgvReceipts.Columns["NETAMT"].Index].Value.ToString());
                        }
                    }
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void dgvReceipts_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                DataGridViewRow row = dgvReceipts.Rows[e.RowIndex];
                if (e.ColumnIndex == dgvReceipts.Columns["CKORCHGNO"].Index)
                {
                    if (row.Cells[dgvReceipts.Columns["OVRCKORCHGNO"].Index].Value.ToString() != "")
                    {
                        e.Value = row.Cells[dgvReceipts.Columns["OVRCKORCHGNO"].Index].Value.ToString();
                        foreach (DataGridViewCell cell in dgvReceipts.Rows[e.RowIndex].Cells)
                        {
                            cell.Style.SelectionBackColor = Color.FromArgb(255, 210, 210, 255);
                            cell.Style.BackColor = Color.FromArgb(255, 210, 210, 255);
                        }
                    }
                    else if (e.Value.ToString().Trim() == "0")
                    {
                        e.Value = "Cash";
                    }
                }
                if (e.ColumnIndex == dgvReceipts.Columns["RECEIPTAMT"].Index)
                {

                    decimal ovramt;
                    decimal receiptamt;
                    try
                    {
                        ovramt = decimal.Parse(row.Cells[dgvReceipts.Columns["OVRAMT"].Index].Value.ToString());
                    }
                    catch { ovramt = 0; }
                    try
                    {
                        receiptamt = decimal.Parse(row.Cells[dgvReceipts.Columns["RECEIPTAMT"].Index].Value.ToString());
                    }
                    catch { receiptamt = 0; }

                    if (ovramt > 0)
                    {
                        e.Value = String.Format("{0:0.00} (${1})", ovramt ,
                                 decimal.Parse(e.Value.ToString()));
                    }

                    if (row.Cells[dgvReceipts.Columns["NOTDEPOSITED"].Index].Value.ToString() == "1")
                    {
                        e.Value = String.Format("Not Deposited (${0})", receiptamt);
                    }
                    }

            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void dgvReceipts_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if(e.Button == MouseButtons.Right)
            {
                if (!dgvReceipts.Rows[e.RowIndex].Cells[e.ColumnIndex].Selected)
                {
                    dgvReceipts.CurrentCell = dgvReceipts.Rows[e.RowIndex].Cells[e.ColumnIndex];
                }
            }
        }

        private void dgvReceipts_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if(e.ColumnIndex == dgvReceipts.Columns["SelBtn"].Index)
            {
                bool HasUnselected = (from DataGridViewRow dgvRow in dgvReceipts.Rows
                                      where dgvRow.Cells["SelBtn"].Value.ToString().Trim() == "0"
                                      select dgvRow).Count() > 0 ? true : false;
                if (HasUnselected)
                {
                    (from DataGridViewRow dgvRow in dgvReceipts.Rows
                     where dgvRow.Cells["SelBtn"].Value.ToString().Trim() == "0"
                     select dgvRow).ToList().ForEach(x => x.Cells["SelBtn"].Value = "1");

                    RunningTotal = 0;

                    (from DataGridViewRow dgvRow in dgvReceipts.Rows
                     select dgvRow).ToList().ForEach(x => RunningTotal += decimal.Parse(x.Cells["NETAMT"].Value.ToString().Trim()));
                }
                else
                {
                    (from DataGridViewRow dgvRow in dgvReceipts.Rows
                     select dgvRow).ToList().ForEach(x => x.Cells["SelBtn"].Value = "0");

                    RunningTotal = 0;
                }
            }
        }

        private void dgvReceipts_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                int receiptsSelected = 0;
                decimal receiptsTotal = 0;

                receiptsSelected = dgvReceipts.SelectedRows.Count;

                foreach (DataGridViewRow dvr in dgvReceipts.Rows)
                {
                    if (dvr.Selected)
                    {
                        receiptsTotal += decimal.Parse(dvr.Cells["NETAMT"].Value.ToString());
                    }
                }

                lblRecepits.Text = receiptsSelected.ToString();
                lblAmount.Text = receiptsTotal.ToString("c2");
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private decimal dgvReceipts_CalcRunningTotal()
        {
            try
            {
                decimal receiptsTotal = 0;
                foreach (DataGridViewRow dvr in dgvReceipts.Rows)
                {
                    string sel = dvr.Cells["SelBtn"].Value.ToString();
                    if (sel == "1")

                    {
                        receiptsTotal += decimal.Parse(dvr.Cells["NETAMT"].Value.ToString());
                    }
                }
                return receiptsTotal;
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                return 0;
            }
        }

        private void frmBankDept01_Load(object sender, EventArgs e)
        {
            try
            {
                this.Wait();

                CreateDropdowns();

                this.Text = this.Text + " (MS SQL Server: " + Form1.sqlTransactions.GetDatabase().Trim() + ")";

                //set the keyed by to the first item as it not bound to a dataset but uses a set list
                cboKeyedBy.SelectedIndex = 0;

                lblAmount.Text = "$0.00";
                lblRecepits.Text = "0";
                lblRunningTotal.Text = "$0.00";
                btnSave.Text = "Save";

                //this the most commonly used type so it will default to this
                cboCashType.SelectedValue = "SL";

                if (_action == ACTION.INSERT)
                {
                    cboDateRanges.Text = _frmBankDept.cboDateRanges.Text.Trim();
                    dtpStartDate.Value = _frmBankDept.dtpStartDate.Value;
                    dtpEndDate.Value = _frmBankDept.dtpEndDate.Value;
                }
                else if (_action == ACTION.PREVIEW)
                {
                    cboDateRanges.Text = _frmBankDept.cboDateRanges.Text.Trim();
                    dtpStartDate.Value = _frmBankDept.dtpStartDate.Value;
                    dtpEndDate.Value = _frmBankDept.dtpEndDate.Value;
                    btnSave.Text = "Exit";
                }
                else if (_action == ACTION.UPDATE)
                {
                    cboProperty.SelectedValue = _theRow.Cells["ENTITYID"].Value.ToString().Trim();
                    cboCashType.SelectedValue = _theRow.Cells["CASHTYPE"].Value.ToString().Trim();

                    btnSave.Enabled = true;
                    cboProperty.Enabled = false;
                    cboCashType.Enabled = false;

                    string strSQL = "";
                    DataTable locTbl = new DataTable();

                    strSQL = $@"SELECT MIN(RECEIPTDAT) as STARTDATE,
                            max(INTDATE) as ENDDATE
                            FROM HS_BANKDEPLINE AS A
                            JOIN HS_BANKDEP D on D.DEPOSITID = A.DEPOSITID 
                            LEFT JOIN HS_SLRECEIPTS AS B ON A.RECEIPTNO = B.IRECEIPTNU and CAST(d.ENTITYID as NUMERIC) = CAST(b.SITEID as NUMERIC)
                            WHERE A.DEPOSITID = {deposit.ToString().Trim().ToString().Trim()}";

                    locTbl = Form1.sqlTransactions.ExecuteQuery(strSQL);

                    if (locTbl.Rows.Count > 0)
                    {
                        if (locTbl.Rows[0]["STARTDATE"] != DBNull.Value)
                        {
                            dtpStartDate.Value = DateTime.Parse(locTbl.Rows[0]["STARTDATE"].ToString().Trim());
                        }
                        else
                        {
                            dtpStartDate.Value = DateTime.Now.AddDays(-7);
                        }
                        if (locTbl.Rows[0]["ENDDATE"] != DBNull.Value)
                        {
                            dtpEndDate.Value = DateTime.Parse(locTbl.Rows[0]["ENDDATE"].ToString().Trim());
                        }
                        else
                        {
                            dtpEndDate.Value = DateTime.Now;
                        }
                    }
                }

                if (_action == ACTION.INSERT | _action == ACTION.PREVIEW)
                {
                    cboProperty.SelectedValue = _frmBankDept.cboProperty.SelectedValue.ToString().Trim();
                }

                PopulateData(cboProperty.SelectedValue.ToString().Trim(), dtpStartDate.Value, dtpEndDate.Value);

                //add the event handlers
                cboProperty.SelectedIndexChanged += Refresh;
                cboCashType.SelectedIndexChanged += Refresh;
                cboCashType.MouseWheel += MouseWheelIgnore;
                cboDateRanges.MouseWheel += MouseWheelIgnore;
                cboProperty.MouseWheel += MouseWheelIgnore;
                cboKeyedBy.MouseWheel += MouseWheelIgnore;
                dtpStartDate.Leave += Refresh;
                dtpEndDate.Leave += Refresh;
                cboKeyedBy.SelectedIndexChanged += Refresh;
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            finally
            {
                this.Useable();
            }
        }

        private void MouseWheelIgnore(object sender, MouseEventArgs e)
        {
            ((HandledMouseEventArgs)e).Handled = true;
        }

        private void Refresh(object sender, EventArgs e)
        {
            try
            {
                this.Wait();
                if (((Control)sender).Name.Trim() == "dtpStartDate" | ((Control)sender).Name.Trim() == "dtpEndDate")
                {
                    cboDateRanges.SelectedIndex = 0;
                }

                if (dtpEndDate.Value < dtpStartDate.Value)
                {
                    dtpStartDate.Value = dtpStartDate.Value.AddDays(-7);
                    return;
                }
                if (modified)
                {
                     DialogResult dialog = MessageBox.Show("Do you wish to save your changes before continuing? ", 
                                                           "You have Unsaved Changes", MessageBoxButtons.YesNoCancel,
                                                            MessageBoxIcon.Question);
                    if (dialog == DialogResult.Yes )
                    {
                        SaveData(sender,  e);                        // savechanges
                        modified = false;
                        //since the data is saved, it is now an update and it will pull in what is already added
                        string strSQL = "";
                        DataTable locTbl = new DataTable();

                        //if the deposit has already been set it not an add becoming an update
                        if (deposit > 0)
                        {

                        }
                        else
                        {//get the new deposit id and set the make this deposit an update
                            strSQL = "SELECT MAX(DEPOSITID) as DEPOSITID FROM HS_BANKDEP";

                            locTbl = Form1.sqlTransactions.ExecuteQuery(strSQL);

                            if (locTbl.Rows.Count > 0)
                            {
                                deposit = int.Parse(locTbl.Rows[0]["DEPOSITID"].ToString().Trim());
                            }

                            _action = ACTION.UPDATE;
                        }
                    }
                    if (dialog == DialogResult.No)
                        modified = false;
                }
                if (! modified )
                  PopulateData(cboProperty.SelectedValue.ToString().Trim(), dtpStartDate.Value, dtpEndDate.Value);
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            finally
            {
                this.Useable();
            }
        }
        #endregion

        #region "Methods"
        /// <summary>
        /// Creates the data in the Drop Down's on the form.
        /// </summary>
        private void CreateDropdowns()
        {
            try
            {
                cboProperty.Items.Clear();

                string strSQL = "";
                DataTable locTbl = new DataTable();

                strSQL = @"SELECT ENTITYID, (RTRIM(LTRIM(ENTITYID)) + '-' + NAME) as NAME FROM ENTITY
                           WHERE ENTITYID IN (SELECT HSVAL FROM HS_CODEVAL WHERE HSTABLE = 'MINISTORAGE' AND HSCOL = 'ENTITYID'
                           AND HSCOL <> '@@@@@')
                           ORDER BY NAME ASC";

                locTbl = Form1.sqlTransactions.ExecuteQuery(strSQL);

                //add the ALL item
                DataRow dr = locTbl.NewRow();
                dr["ENTITYID"] = "";
                dr["NAME"] = "All";
                locTbl.Rows.InsertAt(dr, 0);

                cboProperty.DataSource = locTbl;
                cboProperty.DisplayMember = "NAME";
                cboProperty.ValueMember = "ENTITYID";

                cboCashType.Items.Clear();

                strSQL = @"SELECT CASHTYPE, CASHDESC FROM CTYP
                           ORDER BY CASHDESC ASC";

                locTbl = Form1.sqlTransactions.ExecuteQuery(strSQL);

                cboCashType.DataSource = locTbl;
                cboCashType.DisplayMember = "CASHDESC";
                cboCashType.ValueMember = "CASHTYPE";
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void FormatHeaders()
        {
            try
            {
                dgvReceipts.Columns["CKORCHGNO"].HeaderText = "Check Number";
                dgvReceipts.Columns["TENANT"].HeaderText = "Tenant Name";
                dgvReceipts.Columns["IRECEIPTNU"].HeaderText = "Receipt Number";
                dgvReceipts.Columns["RECEIPTDAT"].HeaderText = "Receipt Date";
                dgvReceipts.Columns["RECEIPTAMT"].HeaderText = "Amount";
                dgvReceipts.Columns["KeyedBy"].HeaderText = "Keyed By";
                dgvReceipts.Columns["Note"].HeaderText = "Notes";
                dgvReceipts.Columns["SITEID"].HeaderText = "Site Id";

                //hides a duplicate button column
                dgvReceipts.Columns[1].Visible = false;
                dgvReceipts.Columns["SCATEGORY"].Visible = false;
                dgvReceipts.Columns["OVRAMT"].Visible = false;
                dgvReceipts.Columns["NotDeposited"].Visible = false;
                dgvReceipts.Columns["NETAMT"].Visible = false;
                dgvReceipts.Columns["OVRCKORCHGNO"].Visible = false;
                if(_action == ACTION.PREVIEW)
                {
                    dgvReceipts.Columns["SelBtn"].Visible = false;
                    dgvReceipts.Columns["SITEID"].Visible = true;
                }
                else
                {
                    dgvReceipts.Columns["SITEID"].Visible = false;
                }
                dgvReceipts.Focus();
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private bool IsValid()
        {
            bool HasRecords = false;

            if (_action == ACTION.UPDATE | _action == ACTION.INSERT)
            {
                HasRecords = (from DataGridViewRow dgvRow in dgvReceipts.Rows
                              where dgvRow.Cells["SelBtn"].Value.ToString().Trim() == "1"
                              select dgvRow).Count() > 0 ? true : false;

                if (!HasRecords)
                {
                    MessageBox.Show("Please selected at least one line. You cannot save a deposit with no line(s).",
                                    "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
            }

            return true;
        }

        private void PopulateData(string entityId, DateTime startDate, DateTime endDate)
        {
            try
            {
                //the current collection of selected receipts
                List<DataRow> curCol = new List<DataRow>();
                if (dgvReceipts.DataSource != null)
                {
                    curCol = (from DataRow row in ((DataTable)dgvReceipts.DataSource).Rows
                                            where row["SelBtn"].ToString() == "1"
                                            select row).ToList();
                }

                dgvReceipts.DataSource = null;

                string strSQL = "";
                DataTable locTbl = new DataTable();

                if (_action == ACTION.UPDATE)
                {
                    strSQL = $@"SELECT 1 as SelBtn, Convert(varchar, max(OVRCKORCHgNO)) OVRCKORCHGNO, CKORCHGNO, A.SITEID, SCATEGORY,
                        SUM(RECEIPTAMT) as RECEIPTAMT, RECEIPTDAT, SBY as KeyedBy, TENANT, IRECEIPTNU, SUM(OVRAMT)/count(*) as OVRAMT, 
                        note, notDeposited, coalesce(notDeposited -1, sum(OVRAMT)/count(*), SUM(RECEIPTAMT)) NetAmt
                        FROM HS_SLRECEIPTS as A
                        LEFT JOIN HS_BANKDEPLINE as B ON A.IRECEIPTNU = B.RECEIPTNO AND B.DEPOSITID = '{deposit.ToString().Trim()}'
                        WHERE SCATEGORY IN ('Check', 'MoneyOrder', 'Cash') AND SITEID = '{entityId.PadLeft(3, '0')}' 
                        and (PMT_CASH > 0 or PMT_CHECK > 0)
                        AND IRECEIPTNU IN
						(SELECT RECEIPTNO FROM HS_BANKDEPLINE AS A
						LEFT JOIN HS_BANKDEP AS B ON A.DEPOSITID = B.DEPOSITID
						WHERE A.DEPOSITID = '{deposit.ToString().Trim()}')
                        GROUP BY CKORCHGNO, A.SITEID,
                        RECEIPTDAT, SBY, TENANT, IRECEIPTNU, SCATEGORY, Note, Notdeposited
                        UNION";
                }
                strSQL += $@" SELECT 0 as SelBtn, convert(varchar,NULL) OVRCKORCHGNO,
                        CKORCHGNO, A.SITEID, SCATEGORY,
                        SUM(RECEIPTAMT) as RECEIPTAMT, RECEIPTDAT, SBY as KeyedBy, 
                        TENANT, IRECEIPTNU, SUM(OVRAMT)/count(*) as OVRAMT, note, notDeposited, coalesce(notDeposited -1, 
                        sum(OVRAMT)/count(*), SUM(RECEIPTAMT)) NetAmt
                        FROM HS_SLRECEIPTS as A
                        LEFT JOIN HS_BANKDEPLINE as B ON A.IRECEIPTNU = B.RECEIPTNO
                        WHERE SCATEGORY IN ('Check', 'MoneyOrder', 'Cash') {(cboProperty.SelectedValue.ToString().Trim() != "" ? $@"
                        AND SITEID = '{entityId.PadLeft(3, '0')}'" : "")} and (PMT_CASH > 0 or PMT_CHECK > 0)
                        AND IRECEIPTNU NOT IN
						(SELECT RECEIPTNO FROM HS_BANKDEPLINE AS A
						LEFT JOIN HS_BANKDEP AS B ON A.DEPOSITID = B.DEPOSITID
						{(cboProperty.SelectedValue.ToString().Trim() != "" ? $" WHERE B.ENTITYID = '{entityId.PadLeft(3, '0')}'" : "")}) 
                        AND CAST(RECEIPTDAT as DATE) BETWEEN '{startDate.ToString("yyyy-MM-dd 00:00:00")}' 
                        AND '{endDate.ToString("yyyy-MM-dd 23:59:59")}'";

                if (cboKeyedBy.Text.ToString().ToUpper().Trim() != "ALL")
                {
                    if (cboKeyedBy.Text.ToString().ToUpper().Trim() == "EP")
                    {
                        strSQL += " AND SBY LIKE '%@%'";
                    }
                    else if (cboKeyedBy.Text.ToString().ToUpper().Trim() == "FIELD")
                    {
                        strSQL += " AND SBY NOT LIKE '%@%'";
                    }
                }

                strSQL += @" GROUP BY CKORCHGNO, A.SITEID,
                             RECEIPTDAT, SBY, TENANT, IRECEIPTNU, SCATEGORY, note, notdeposited
                             ORDER BY RECEIPTDAT ASC";

                locTbl = Form1.sqlTransactions.ExecuteQuery(strSQL);

                RunningTotal = 0;

                //checks if the receipt number was already checked before changing the filter
                foreach(DataRow row in curCol)
                {
                    foreach (DataRow innerRow in locTbl.Rows)
                    {
                        if (row["IRECEIPTNU"].ToString().Trim() == innerRow["IRECEIPTNU"].ToString().Trim())
                        {
                            innerRow["SelBtn"] = "1";
                        }
                        else
                        {

                        }
                    }
                }

                if (locTbl.Rows.Count > 0)
                {

                    dgvReceipts.DataSource = locTbl;

                    dgvReceipts.AutoResizeColumns();

                    DataGridViewCheckBoxColumn selBtn = new DataGridViewCheckBoxColumn();
                    selBtn.Name = "SelBtn";
                    selBtn.DataPropertyName = "SelBtn";
                    selBtn.HeaderText = "Select";
                    selBtn.Width = 50;
                    selBtn.ReadOnly = false;
                    selBtn.Frozen = true;
                    selBtn.FalseValue = 0;
                    selBtn.TrueValue = 1;
                    dgvReceipts.Columns.Insert(0, selBtn);

                    FormatHeaders();
                    RunningTotal = dgvReceipts_CalcRunningTotal();
                    modified = false;

                    dgvReceipts.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void SaveData(object sender, EventArgs e)
        {
            try
            {
                if (IsValid())
                {
                    if (_action == ACTION.INSERT)
                    {
                        Dictionary<string, string> insertValues;
                        DataTable locTbl = new DataTable();
                        DataTable receipts = (DataTable)dgvReceipts.DataSource;
                        string strSQL = "";
                        int depositId = 1;

                        insertValues = new Dictionary<string, string>();
                        insertValues.Add("CASHTYPE", cboCashType.SelectedValue.ToString().Trim());
                        insertValues.Add("LASTDATE", DateTime.Now.ToString());
                        insertValues.Add("USERID", HelpfulClasses.HelpfulFunctions.UserID.Trim());
                        insertValues.Add("ENTITYID", cboProperty.SelectedValue.ToString().Trim().PadLeft(3, '0'));
                        insertValues.Add("INTDATE", dtpEndDate.Value.ToString("yyyy-MM-dd"));
                        insertValues.Add("TOTAL", RunningTotal.ToString());

                        Form1.sqlTransactions.InsertTableData("HS_BANKDEP", insertValues);

                        strSQL = @"SELECT MAX(DEPOSITID) as DEPOSITID FROM HS_BANKDEP";

                        locTbl = Form1.sqlTransactions.ExecuteQuery(strSQL);

                        if (locTbl.Rows.Count > 0)
                        {
                            if (locTbl.Rows[0]["DEPOSITID"] != DBNull.Value)
                            {
                                depositId = int.Parse(locTbl.Rows[0]["DEPOSITID"].ToString().Trim());
                            }
                        }

                        foreach (DataRow row in receipts.Rows)
                        {
                            if (row["SelBtn"].ToString().Trim() == "1")
                            {
                                insertValues = new Dictionary<string, string>();
                                insertValues.Add("DEPOSITID", depositId.ToString().Trim());
                                insertValues.Add("RECEIPTNO", row["IRECEIPTNU"].ToString().Trim());
                                if (row["OVRAMT"].ToString().Trim() != "")
                                    insertValues.Add("OVRAMT", row["OVRAMT"].ToString().Trim());
                                if (row["OVRCKORcHGNO"].ToString().Trim() != "")
                                    insertValues.Add("OVRCkORCHGNO", row["OVRCkORCHGNO"].ToString().Trim());

                                insertValues.Add("note", row["note"].ToString().Trim());
                                if (row["notDeposited"].ToString().Trim() != "")
                                    insertValues.Add("notDeposited", row["notDeposited"].ToString().Trim());
                                Form1.sqlTransactions.InsertTableData("HS_BANKDEPLINE", insertValues);
                            }
                        }
                    }
                    else if (_action == ACTION.UPDATE)
                    {
                        Dictionary<string, string> updateValues = new Dictionary<string, string>();
                        Dictionary<string, string> whereValues = new Dictionary<string, string>();
                        DataTable receipts = (DataTable)dgvReceipts.DataSource;

                        whereValues.Add("DEPOSITID", deposit.ToString().Trim().ToString().Trim());

                        Form1.sqlTransactions.DeleteTableData("HS_BANKDEPLINE", whereValues);

                        foreach (DataRow row in receipts.Rows)
                        {
                            if (row["SelBtn"].ToString().Trim() == "1")
                            {
                                updateValues = new Dictionary<string, string>();
                                updateValues.Add("DEPOSITID", deposit.ToString().Trim().ToString().Trim());
                                updateValues.Add("RECEIPTNO", row["IRECEIPTNU"].ToString().Trim());
                                if (row["OVRAMT"].ToString().Trim() != "")
                                    updateValues.Add("OVRAMT", row["OVRAMT"].ToString().Trim());
                                updateValues.Add("note", row["note"].ToString().Trim());
                                if (row["notDeposited"].ToString().Trim() != "")
                                    updateValues.Add("notDeposited", row["notDeposited"].ToString().Trim());
                                if (row["OVRCKORcHGNO"].ToString().Trim() != "")
                                    updateValues.Add("OVRCkORCHGNO", row["OVRCkORCHGNO"].ToString().Trim());
                                Form1.sqlTransactions.InsertTableData("HS_BANKDEPLINE", updateValues);
                            }
                        }

                        updateValues.Clear();

                        updateValues = new Dictionary<string, string>();
                        updateValues.Add("LASTDATE", DateTime.Now.ToString());
                        updateValues.Add("USERID", HelpfulClasses.HelpfulFunctions.UserID.Trim());
                        updateValues.Add("ENTITYID", cboProperty.SelectedValue.ToString().Trim().PadLeft(3, '0'));
                        updateValues.Add("INTDATE", dtpEndDate.Value.ToString("yyyy-MM-dd"));
                        updateValues.Add("TOTAL", RunningTotal.ToString());

                        Form1.sqlTransactions.UpdateTableData("HS_BANKDEP", updateValues, whereValues);
                    }
                    else if (_action == ACTION.PREVIEW)
                    {
                        // don't do anything
                    }
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }
        #endregion
    }
}
