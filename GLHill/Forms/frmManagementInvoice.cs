﻿using HelpfulClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GLHill.Forms
{
    public partial class frmManagementInvoice : Form
    {
        #region "Fields"

        #endregion

        #region "Constructors"
        public frmManagementInvoice()
        {
            InitializeComponent();
        }
        #endregion

        #region "Event Handlers"
        private void btnCreateInvoice_Click(object sender, EventArgs e)
        {
            try
            { 
                this.Wait();

                decimal invoiceAmount = 0M;
                string sessionId = "";
                string strSQL = "";
                DataTable locTbl = new DataTable();
                Dictionary<string, string> insertValues = new Dictionary<string, string>();
                Prompt invoiceNumber = new Prompt("Please enter the invoice number.", "Attention!!!");
                invoiceNumber.StartPosition = FormStartPosition.CenterParent;

            //if the use clicks okay but there is validation error, jump here to prompt again
            tryAgain:
                //prompt the user the enter and invoice number
                invoiceNumber.ShowDialog(this);
                //if the user clicks "Submit"
                if (invoiceNumber.Result == DialogResult.OK)
                {
                    //if the invoice is not blank
                    if (invoiceNumber.Response.Trim() != "")
                    {
                        //if the invoice is less than or equal to 24 characters
                        if (invoiceNumber.Response.Trim().Length <= 24)
                        {
                            //create the where clause to search for any existing "HILMAN" invoices with same number in same period
                            Dictionary<string, string> whereValues = new Dictionary<string, string>();
                            whereValues.Add("VENDID", "HILMAN");
                            whereValues.Add("INVOICE", invoiceNumber.Response.Trim());
                            whereValues.Add("EXPPED", DateTime.Now.Period());

                            //if the no invoice exists
                            if (!Form1.sqlTransactions.RecordExists("INVC", whereValues))
                            {
                                //gets just the rows that fee is greater than or equal to 1 and less than or equal to -1 
                                List<DataGridViewRow> rows = (from DataGridViewRow row in dgvInvoices.Rows
                                                              where row.Cells["fee"].Value != DBNull.Value &&
                                                              (decimal.Parse(row.Cells["fee"].Value.ToString().Trim()) >= 1M | decimal.Parse(row.Cells["fee"].Value.ToString().Trim()) <= -1M)
                                                              select row).ToList();
                                //close existing to SQL server
                                Form1.sqlTransactions.Disconnect();

                                //start commitment control on the SQL server
                                Form1.sqlTransactions.BeginTransaction();

                                //lookup the exist session for the period
                                strSQL = $@"SELECT SESSION FROM SESS
                                            WHERE EXPPED = '{cboEndingPeriod.Text.Trim()}'";
                                locTbl = Form1.sqlTransactions.ExecuteQuery(strSQL);

                                //if the session is found
                                if (locTbl.Rows.Count > 0)
                                {
                                    //set the session id
                                    sessionId = locTbl.Rows[0]["SESSION"].ToString().Trim();
                                }

                                //total the invoice amount
                                invoiceAmount = (from DataGridViewRow row in rows
                                                 select decimal.Parse(row.Cells["fee"].Value.ToString().Trim())).Sum();

                                //setup the record to insert the invoice header information
                                insertValues.Add("VENDID", "HILMAN");
                                insertValues.Add("INVOICE", invoiceNumber.Response.Trim());
                                insertValues.Add("EXPPED", DateTime.Now.Period());
                                insertValues.Add("INVCDATE", dtpInvoiceDate.Value.ToString("yyyy-MM-dd"));
                                insertValues.Add("DUEDATE", dtpInvoiceDate.Value.ToString("yyyy-MM-dd"));
                                insertValues.Add("INVCAMT", invoiceAmount.ToString("n2"));
                                insertValues.Add("SESSION", sessionId.Trim());
                                insertValues.Add("LASTDATE", DateTime.Now.ToString());
                                insertValues.Add("USERID", HelpfulFunctions.UserID.Trim());
                                insertValues.Add("PARTIALPRG", "N");
                                insertValues.Add("ATAXAMT", "0.00");
                                insertValues.Add("INVCTOT", invoiceAmount.ToString("n2"));
                                insertValues.Add("SITEID", "@");
                                insertValues.Add("INCLATAXAMT", "0.00");
                                insertValues.Add("JOBCOST", "N");
                                insertValues.Add("DESCRPTN", string.Format("MGMT FEE {0} PAY DOWN", cboEndingPeriod.Text.Trim()));
                                insertValues.Add("OUTSIDEBUD", "N");
                                insertValues.Add("APINVCTYPEID", "INV");

                                Form1.sqlTransactions.InsertTableData("INVC", insertValues);

                                //create a counter for invoice line items
                                int count = 1;
                                //go through each invoice line item
                                foreach (DataGridViewRow row in rows)
                                {
                                    strSQL = "";
                                    string bankId = "";
                                    locTbl = new DataTable();

                                    //gets the bank id from the entity id and cash type
                                    strSQL = $@"DECLARE	@bankId nvarchar(6)

                                    EXEC	[dbo].[HS_GetBankIdFromEntityAndCash]
                                            @entityId = N'{row.Cells["entityid"].Value.ToString().Trim()}',
                                            @cashType = N'OP',
		                                    @bankId = @bankId OUTPUT

                                    SELECT	@bankId as N'@bankId'";

                                    locTbl = Form1.sqlTransactions.ExecuteQuery(strSQL);

                                    //if a bank id is found
                                    if (locTbl.Rows.Count > 0)
                                    {
                                        //set the bank id
                                        bankId = locTbl.Rows[0]["@bankId"].ToString().Trim();

                                        //clear the collection of value from the previous recordd inserted
                                        insertValues.Clear();

                                        insertValues.Add("VENDID", "HILMAN");
                                        insertValues.Add("INVOICE", invoiceNumber.Response.Trim());
                                        insertValues.Add("EXPPED", DateTime.Now.Period());
                                        insertValues.Add("ITEM", count.ToString().Trim());
                                        insertValues.Add("REF", string.Format("MGMT FEE {0} PAY DOWN", cboEndingPeriod.Text.Trim()));
                                        insertValues.Add("ENTITYID", row.Cells["entityid"].Value.ToString().Trim());
                                        insertValues.Add("ACCTNUM", "HS212000000000");
                                        insertValues.Add("ITEMAMT", row.Cells["fee"].Value.ToString().Trim());
                                        insertValues.Add("DISCAMT", "0.00");
                                        insertValues.Add("STATUS", "R");
                                        insertValues.Add("BALFLAG", "N");
                                        insertValues.Add("DISCTKN", "N");
                                        insertValues.Add("CASHTYPE", "OP");
                                        insertValues.Add("POLINENMBR", "0");
                                        insertValues.Add("ATAXAMT", "0.00");
                                        insertValues.Add("DEPARTMENT", "@");
                                        insertValues.Add("TAXITEM", "N");
                                        insertValues.Add("BANKID", bankId.Trim());
                                        insertValues.Add("LASTDATE", DateTime.Now.ToString());
                                        insertValues.Add("USERID", HelpfulFunctions.UserID.Trim());
                                        insertValues.Add("ECITEMAMT", "0.00");
                                        insertValues.Add("ECDISCAMT", "0.00");
                                        insertValues.Add("ORIGITEMAMT", "0.00");
                                        insertValues.Add("TRANSFERITEM", "N");
                                        insertValues.Add("DISCMOD", "N");
                                        insertValues.Add("SUBWITH", "N");
                                        insertValues.Add("JOBCOST", "N");
                                        insertValues.Add("AUTAXPAID", "N");
                                        insertValues.Add("APINVCTYPEID", "INV");
                                        insertValues.Add("WITHHOLDINGLINE", "N");

                                        Form1.sqlTransactions.InsertTableData("HIST", insertValues);

                                        count++;
                                    }
                                }
                            }
                            else
                            {
                                //clear the where clause values
                                whereValues.Clear();

                                //alert the user that the invoice already exists
                                MessageBox.Show(string.Format("The invoice {0} already exists. Please enter a new one.", invoiceNumber.Response.Trim()),
                                                "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                                //ask them for the invoice again
                                goto tryAgain;
                            }

                            //commit the transactions
                            Form1.sqlTransactions.CommitTransaction();

                            //let the user know the invoice was created successfully
                            MessageBox.Show("The invoice was created successfully.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            this.Useable();
                        }//if the invoice number exceeds 24 characters
                        else
                        {
                            //alert the user
                            MessageBox.Show("Please only enter an invoice number that is less than 25 characters.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            //prompt them again
                            goto tryAgain;
                        }
                    }//if the user did not enter anything for the invoice number
                    else
                    {
                        //alert the user
                        MessageBox.Show("Please enter an invoice number.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //prompt them again
                        goto tryAgain;
                    }
                }
            }
            catch (SqlException err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            finally
            {
                ResetForm();
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                GetData();

                //if there are line items
                if (dgvInvoices.Rows.Count > 0)
                {
                    btnCreateInvoice.Enabled = true;
                }//if there are no line items
                else
                {
                    btnCreateInvoice.Enabled = false;
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void cboEndingPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //clear the min and max dates so a validation error does not occur when set them to the current month below
                dtpInvoiceDate.MinDate = DateTime.Parse("1900-01-01");
                dtpInvoiceDate.MaxDate = DateTime.Parse("9998-01-01");

                //if the user selected a period
                if (cboEndingPeriod.Text.Trim() != "")
                {
                    //allow them to refresh
                    btnRefresh.Enabled = true;
                    //allow the to selected an invoice date only in the current period
                    dtpInvoiceDate.Enabled = true;
                    dtpInvoiceDate.MinDate = DateTime.Parse(string.Format("{0}/01/{1}", DateTime.Now.Month, DateTime.Now.Year));
                    dtpInvoiceDate.MaxDate = DateTime.Parse(string.Format("{0}/{1}/{2}", DateTime.Now.Month,
                                                            DateTime.DaysInMonth(DateTime.Now.Year,
                                                            DateTime.Now.Month), DateTime.Now.Year));
                    //default to the first day of the current period
                    dtpInvoiceDate.Value = dtpInvoiceDate.MinDate;
                }
                else
                {
                    ResetForm();
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void chkHideUnpostableTransactions_CheckedChanged(object sender, EventArgs e)
        {
            if (chkHideUnpostableTransactions.Checked)
            {
                foreach (DataGridViewRow row in dgvInvoices.Rows)
                {
                    if (row.Cells["fee"].Value.ToString().Trim() == "0.0000")
                    {
                        CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[dgvInvoices.DataSource];
                        currencyManager1.SuspendBinding();
                        dgvInvoices.Rows[row.Index].Visible = false;
                        currencyManager1.ResumeBinding();
                    }
                }
            }
            else
            {
                foreach (DataGridViewRow row in dgvInvoices.Rows)
                {
                    CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[dgvInvoices.DataSource];
                    currencyManager1.SuspendBinding();
                    dgvInvoices.Rows[row.Index].Visible = true;
                    currencyManager1.ResumeBinding();
                }
            }
        }

        private void dgvInvoices_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                //if the cell does not have a null value
                if (e.Value != DBNull.Value)
                {
                    //if the cell is in the "fee" column
                    if (e.ColumnIndex == dgvInvoices.Columns["fee"].Index)
                    {
                        //right align numbers because they are decimals
                        e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                        //format the value to a decimal with 2 places
                        e.Value = decimal.Parse(e.Value.ToString()).ToString("c2");
                    }
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void frmManagementInvoice_FormClosing(object sender, FormClosingEventArgs e)
        {
            Form1.sqlTransactions.Disconnect();
            Form1.sqlTransactions.Connect();
        }

        private void frmManagementInvoices_Load(object sender, EventArgs e)
        {
            try
            {
                //format the form title to display the form title and what database the program is connected to
                this.Text = this.Text + " (MS SQL Server: " + Form1.sqlTransactions.GetDatabase().Trim() + ")";

                CreateDropDownList();

                //set up the drop down list event handle, as doing it the designer code would cause it happen before values are in it
                cboEndingPeriod.SelectedIndexChanged += cboEndingPeriod_SelectedIndexChanged;
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }
        #endregion

        #region "Methods"
        /// <summary>
        /// Populate the drop down list with data.
        /// </summary>
        private void CreateDropDownList()
        {
            try
            {
                //add the periods to the period drop down list
                DataTable locTblPeriods = new DataTable();
                locTblPeriods.Columns.Add("DISPLAYTEXT");
                locTblPeriods.Columns.Add("VALUESELECTED");

                DataRow dr;
                //go back 3 perious period starting the previous period 
                for (int i = -3; i < 0; i++)
                {
                    dr = locTblPeriods.NewRow();

                    dr["DISPLAYTEXT"] = DateTime.Now.AddMonths(i).ToString("MM/yy");
                    dr["VALUESELECTED"] = DateTime.Now.AddMonths(i).ToString("yyyyMM");

                    locTblPeriods.Rows.Add(dr);
                }

                //add a blank item to first position in the drop down, to act as the default undecided period
                dr = locTblPeriods.NewRow();

                dr["DISPLAYTEXT"] = "";
                dr["VALUESELECTED"] = "";

                locTblPeriods.Rows.InsertAt(dr, 0);

                //bind the data to the drop down
                cboEndingPeriod.DataSource = locTblPeriods;
                cboEndingPeriod.DisplayMember = "DISPLAYTEXT";
                cboEndingPeriod.ValueMember = "VALUESELECTED";
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Formats the header row in the grid.
        /// </summary>
        private void FormatHeaders()
        {
            try
            {
                dgvInvoices.Columns["entityid"].HeaderText = "Entity Id";
                dgvInvoices.Columns["fee"].HeaderText = "Fee";
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Gets the data to populate in the grid.
        /// </summary>
        private void GetData()
        {
            try
            {
                //get all invoice lines items, which are the fees owed by each property
                DataTable locTbl = new DataTable();
                string strSQL = $@"with t1 as (select entityid, -sum(itemamt) as fee
                                    from hist 
                                    where vendid = 'HILMAN' and acctnum = 'HS212000000000' and ckcashglref is null and status <> 'D' 
                                    group by entityid
                                    UNION
                                    select s.entityid, (-1 * (sum(activity))) fee from glsum S 
                                    JOIN (select entityid , max(period) balfperiod 
                                    from period where balfor = 'B' group by entityid ) B
                                    ON S.ENTITYID = b.entityid and s.period >= balfperiod
                                    where acctnum = 'HS212000000000' and activity <> 0 and (s.entityid in 
                                    (select bldgid from bldg where mgmtrate > 0) or s.entityid in (select rmpropid from rmprop where mgmtrate <> 0) OR s.ENTITYID = 600) group by s.entityid)
								     select entityid, sum(fee) as fee from t1
								     group by entityid";

                locTbl = Form1.sqlTransactions.ExecuteQuery(strSQL);

                dgvInvoices.DataSource = locTbl;

                FormatHeaders();
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Reset the form, its controls, and fields back to their default states.
        /// </summary>
        private void ResetForm()
        {
            try
            {
                btnCreateInvoice.Enabled = false;
                btnRefresh.Enabled = false;
                cboEndingPeriod.SelectedIndex = 0;
                dtpInvoiceDate.Enabled = false;
                dtpInvoiceDate.MinDate = DateTime.Parse("1900-01-01");
                dtpInvoiceDate.MaxDate = DateTime.Parse("9998-01-01");
                dgvInvoices.DataSource = null;
                this.Useable();
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }
        #endregion
    }
}