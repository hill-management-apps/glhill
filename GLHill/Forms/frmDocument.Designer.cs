﻿
namespace GLHill.Forms
{
    partial class frmDocument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.webDocument = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // webDocument
            // 
            this.webDocument.AllowNavigation = false;
            this.webDocument.AllowWebBrowserDrop = false;
            this.webDocument.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webDocument.Location = new System.Drawing.Point(12, 12);
            this.webDocument.MinimumSize = new System.Drawing.Size(20, 20);
            this.webDocument.Name = "webDocument";
            this.webDocument.Size = new System.Drawing.Size(570, 565);
            this.webDocument.TabIndex = 0;
            // 
            // frmDocument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 589);
            this.Controls.Add(this.webDocument);
            this.Name = "frmDocument";
            this.Text = "frmDocument";
            this.Load += new System.EventHandler(this.frmDocument_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser webDocument;
    }
}