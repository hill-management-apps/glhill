﻿using HelpfulClasses;
using HillAPI.MRI;
using HillAPI.MRI.End_Points;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GLHill.Forms
{
    public partial class frmImportBase : Form
    {
        protected List<Department> departments = new List<Department>();
        protected List<Account> accounts = new List<Account>();
        protected List<Entity> entities = new List<Entity>();
        protected DataTable locTblAllocations = new DataTable();
        protected DataTable locTblJobCost = new DataTable();
        protected MRIAPI mriAPI;
        protected HillWebServices.HillWebServicesSoapClient hillWebService = new HillWebServices.HillWebServicesSoapClient();

        public frmImportBase()
        {
            mriAPI = new MRIAPI(string.Format("{0}/{1}/{2}/{3}", Properties.Settings.Default.ClientID.Trim(), Properties.Settings.Default.Database.Trim(), Properties.Settings.Default.UserID.Trim(),
                                                     Properties.Settings.Default.AccessCode.Trim()), Properties.Settings.Default.Password.Trim());

            //get the departments in one call so as to not make multiple each time a department is validated
            departments = mriAPI.GetDepartments();
            //get the accounts in one call so as to not make multiple each time an account is validated
            accounts = mriAPI.GetAccounts();
            //get the properties in one call so as to not make multiple each time a property is validated
            entities = mriAPI.GetEntities();
            Entity entity = new Entity();
            entity.EntityID = "726";
            entity.DispositionDate = DateTime.MinValue;
            entities.Add(entity);

            locTblAllocations = hillWebService.GetAllocations(DateTime.Now.Period(), 0M, "");

            locTblJobCost = hillWebService.GetJobCodes();
        }

        protected string GetJobAccount(string[] jobCostParts)
        {
            try
            {
                string account = "";

                if (jobCostParts.Length == 4)
                {
                    try
                    {
                        account = (from DataRow jobCostRow in locTblJobCost.Rows
                                   where jobCostRow["JOBCODE"].ToString().Trim() == jobCostParts[0].Trim() &
                                         jobCostRow["JC_PHASECODE"].ToString().Trim() == jobCostParts[1].Trim() &
                                         jobCostRow["JC_COSTLIST"].ToString().Trim() == jobCostParts[2].Trim() &
                                         jobCostRow["JC_COSTCODE"].ToString().Trim() == jobCostParts[3].Trim()
                                   select jobCostRow["ACCTNUM"].ToString().Trim()).Single();
                    }
                    catch (ArgumentNullException err)
                    {

                    }
                    catch (InvalidOperationException err)
                    {

                    }

                    return account.Trim();
                }
                else
                {
                    return account.Trim();
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                return "";
            }
        }

        protected string GetJobDepartment(string[] jobCostParts)
        {
            try
            {
                string department = "";

                if (jobCostParts.Length == 4)
                {
                    try
                    {
                        department = (from DataRow jobCostRow in locTblJobCost.Rows
                                      where jobCostRow["JOBCODE"].ToString().Trim() == jobCostParts[0].Trim() &
                                            jobCostRow["JC_PHASECODE"].ToString().Trim() == jobCostParts[1].Trim() &
                                            jobCostRow["JC_COSTLIST"].ToString().Trim() == jobCostParts[2].Trim() &
                                            jobCostRow["JC_COSTCODE"].ToString().Trim() == jobCostParts[3].Trim()
                                      select jobCostRow["DEPARTMENT"].ToString().Trim()).Single();
                    }
                    catch (ArgumentNullException err)
                    {

                    }
                    catch (InvalidOperationException err)
                    {

                    }

                    return department.Trim();
                }
                else
                {
                    return department.Trim();
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                return "";
            }
        }

        protected string GetJobEntityId(string[] jobCostParts)
        {
            try
            {
                string entityId = "";

                if (jobCostParts.Length == 4)
                {
                    try
                    {
                        entityId = (from DataRow jobCostRow in locTblJobCost.Rows
                                    where jobCostRow["JOBCODE"].ToString().Trim() == jobCostParts[0].Trim() &
                                          jobCostRow["JC_PHASECODE"].ToString().Trim() == jobCostParts[1].Trim() &
                                          jobCostRow["JC_COSTLIST"].ToString().Trim() == jobCostParts[2].Trim() &
                                          jobCostRow["JC_COSTCODE"].ToString().Trim() == jobCostParts[3].Trim()
                                    select jobCostRow["ENTITYID"].ToString().Trim()).Single();
                    }
                    catch (ArgumentNullException err)
                    {

                    }
                    catch (InvalidOperationException err)
                    {

                    }

                    return entityId.Trim();
                }
                else
                {
                    return entityId.Trim();
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                return "";
            }
        }

        protected string GetJobName(string[] jobCostParts)
        {
            try
            {
                string name = "";

                if (jobCostParts.Length == 4)
                {
                    try
                    {
                        name = (from DataRow jobCostRow in locTblJobCost.Rows
                                where jobCostRow["JOBCODE"].ToString().Trim() == jobCostParts[0].Trim() &
                                      jobCostRow["JC_PHASECODE"].ToString().Trim() == jobCostParts[1].Trim() &
                                      jobCostRow["JC_COSTLIST"].ToString().Trim() == jobCostParts[2].Trim() &
                                      jobCostRow["JC_COSTCODE"].ToString().Trim() == jobCostParts[3].Trim()
                                select jobCostRow["DESCRPTN"].ToString().Trim()).Single();
                    }
                    catch(ArgumentNullException err)
                    {

                    }
                    catch(InvalidOperationException err)
                    {

                    }

                    return name.Trim();
                }
                else
                {
                    return name.Trim();
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                return "";
            }
        }

        /// <summary>
        /// Checks if an account number is valid for the ledger code group.
        /// </summary>
        /// <param name="ledgerCode">The ledger code that indicates the group of accounts.</param>
        /// <param name="accountCode">The account code.</param>
        /// <returns>Return false is not found in the ledger code group and true if it is found.</returns>
        protected bool IsValidAccount(string ledgerCode, string accountCode)
        {
            try
            {
                int count = (from Account account in accounts
                             where account.Accountnumber == ledgerCode + accountCode.PadRight(12, '0')
                             select account).Count();

                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                return false;
            }
        }

        /// <summary>
        /// Checks if the department is a valid department.
        /// </summary>
        /// <param name="Department">The department id.</param>
        /// <returns>Returns false if the department id is not found, and true if the department id is found.</returns>
        protected bool IsValidDepartment(string Department)
        {
            try
            {
                int count = (from Department department in departments
                             where department.DepartmentID == Department.Trim()
                             select department).Count();

                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                return false;
            }
        }

        protected bool IsValidAllocationCode(string code)
        {
            try
            {
                return (from DataRow percentageRow in locTblAllocations.Rows
                        where percentageRow["ALLOCCODE"].ToString().Trim() == code.Trim()
                        select percentageRow).Count() >= 1 ? true : false;
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                return false;
            }
        }

        protected bool IsValidJob(string[] jobCostParts)
        {
            try
            {
                if (jobCostParts.Length == 4)
                {
                    return (from DataRow jobCostRow in locTblJobCost.Rows
                            where jobCostRow["JOBCODE"].ToString().Trim() == jobCostParts[0].Trim() &
                                  jobCostRow["JC_PHASECODE"].ToString().Trim() == jobCostParts[1].Trim() &
                                  jobCostRow["JC_COSTLIST"].ToString().Trim() == jobCostParts[2].Trim() &
                                  jobCostRow["JC_COSTCODE"].ToString().Trim() == jobCostParts[3].Trim()
                            select jobCostRow).Count() >= 1 ? true : false;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                return false;
            }
        }

        /// <summary>
        /// Checks if the property is valid property.
        /// </summary>
        /// <param name="property">The property id.</param>
        /// <returns>Returns false if the property id is not found, and true if the property is found.</returns>
        protected bool IsValidProperty(string property)
        {
            try
            {
                int count = (from Entity entity in entities
                             where entity.EntityID == property.PadLeft(3, '0') &
                                   entity.DispositionDate == DateTime.MinValue
                             select entity).Count();

                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                return false;
            }
        }
    }
}
