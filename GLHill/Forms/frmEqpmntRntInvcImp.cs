﻿using ExcelDataReader;
using GLHill.Classes;
using HelpfulClasses;
using HillAPI.MRI;
using HillAPI.MRI.End_Points;
using HillAPI.Nexus;
using iText.Commons.Utils;
using iText.Forms;
using iText.Forms.Fields;
using iText.IO.Util;
using iText.Kernel.Pdf;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GLHill.Forms
{
    public partial class frmEqpmntRntInvcImp : frmImportBase
    {
        #region "Fields"
        private IExcelDataReader excel;
        private List<EquipmentCharge> equipmentCharges;
        private new MRIAPI mriAPI;
        private List<HillAPI.MRI.End_Points.Invoice.entry> invoiceEntries = new List<HillAPI.MRI.End_Points.Invoice.entry>();
        private new List<Entity> entities = new List<Entity>();
        private NexusAPI nexusApi = new NexusAPI(Properties.Settings.Default.NexusURL, Properties.Settings.Default.NexusUsername,
                                                 Properties.Settings.Default.NexusSecret, Properties.Settings.Default.NexusKey, "", 1200);
        #endregion

        #region "constructors"
        public frmEqpmntRntInvcImp()
        {
            InitializeComponent();
        }
        #endregion

        #region "Methods"

        /// <summary>
        /// Creates the invoice PDF's based on the list passed in.
        /// </summary>
        /// <param name="equipmentChargeInvoices">The list of <see cref="EquipmentCharge"/> invoices.</param>
        private void CreateInvoicePDFs(List<EquipmentCharge> equipmentChargeInvoices)
        {
            try
            {
                PdfAcroForm pdfForm;
                //create the datatable for invoice errors
                DataTable locTblResponse = new DataTable();

                locTblResponse.Columns.Add("Detail");
                locTblResponse.Columns.Add("Error");

                //go through equipment charge
                foreach (EquipmentCharge equipmentCharge in equipmentChargeInvoices)
                {
                    //create a new invoice PDF from the template
                    PdfDocument document = new PdfDocument(new PdfReader(Directory.GetCurrentDirectory() +
                                           @"\Equipment Invoice Template.pdf"),
                                           new PdfWriter(Directory.GetCurrentDirectory() + string.Format(@"\Equipment Invoice {0}.pdf", equipmentCharge.Invoice.Trim())));
                    pdfForm = PdfAcroForm.GetAcroForm(document, true);
                    //gets the list of fields
                    LinkedDictionary<string, PdfFormField> pdfFields = (LinkedDictionary<string, PdfFormField>)pdfForm.GetFormFields();
                    try
                    {
                        //set the fields on the invoice
                        //check if it is a job code
                        string[] entityParts = equipmentCharge.Entity.Split(',');

                        if (entityParts.Length == 4)
                        {
                            pdfFields["Property"].SetValue(equipmentCharge.GetJobEntityInfo(entityParts));
                        }
                        else
                        {
                            pdfFields["Property"].SetValue(equipmentCharge.Entity.Trim().PadLeft(3, '0') + "-" + equipmentCharge.EntityName.Trim());
                        }

                        pdfFields["Vehicle/Equipment"].SetValue("Equipment Leasing");
                        pdfFields["Date"].SetValue(equipmentCharge.PayWeekEnding.ToString("MM/dd/yyyy"));
                        pdfFields["Invoice"].SetValue(equipmentCharge.Invoice.Trim());
                        pdfFields["Price"].SetValue(equipmentCharge.GetTotalCharges().ToString("n2"));
                        pdfFields["Total"].SetValue(equipmentCharge.GetTotalCharges().ToString("n2"));
                        pdfFields["Quantity"].SetValue(equipmentCharge.GetTotalHours().ToString("n2"));
                        pdfFields["EquipmentRate"].SetValue(equipmentCharge.Rate.ToString().Trim());

                        HillAPI.Nexus.End_Points.Invoice invoice = new HillAPI.Nexus.End_Points.Invoice("MRI", equipmentCharge.Entity.Trim().PadLeft(3, '0'),
                                                                                                        "HMCONT", "KMETZGER", equipmentCharge.ExpensePeriod.ToString("yyyy-MM-01"),
                                                                                                        "Regular")
                        {
                            Invoice_datetm = equipmentCharge.PayWeekEnding.ToString(),
                            Invoice_ref = equipmentCharge.Invoice.Trim().Replace(',', ' '),
                            Invoice_duedate = equipmentCharge.PayWeekEnding.AddDays(30).ToString(),
                            Control_amount = float.Parse(equipmentCharge.GetTotalCharges().ToString()),
                            Universial_field7 = string.Format("RP-EQUIPMENT LEASING {0}", equipmentCharge.PayWeekEnding.ToString("yyyy-MM-dd")),
                        };

                        //close the document and release memory
                        document.Close();
                        document = null;
                        pdfFields.Clear();
                        pdfFields = null;
                        pdfForm = null;

                        List<HillAPI.Nexus.End_Points.InvoiceLine> invoiceLines = new List<HillAPI.Nexus.End_Points.InvoiceLine>();

                        //test if the entity is really a job code
                        HillAPI.Nexus.End_Points.InvoiceLine invoiceLine = new HillAPI.Nexus.End_Points.InvoiceLine(equipmentCharge.Entity.Trim().PadLeft(3, '0'),
                                                                                                                    1, float.Parse(equipmentCharge.GetTotalCharges().ToString()),
                                                                                                                    "652000", string.Format("RP-EQUIPMENT LEASING {0}", equipmentCharge.PayWeekEnding.ToString("yyyy-MM-dd")));

                        entityParts = equipmentCharge.Entity.Split(',');

                        if (entityParts.Length == 4)
                        {
                            if (IsValidJob(entityParts))
                            {
                                invoiceLine.Jbjobcode_name = entityParts[0].Trim();
                                invoiceLine.Jbphasecode_name = entityParts[1].Trim();
                                invoiceLine.Jbcostcode_name = entityParts[3].Trim();
                                invoiceLine.Jbjobtype_name = entityParts[2].Trim();
                                invoiceLine.Property_id_alt = GetJobEntityId(entityParts);
                                invoiceLine.Glaccount_number = GetJobAccount(entityParts).ToUpper().
                                                               Replace("HS","").Substring(0,6);
                                //for some reason the codes in Nexus have period property after the code
                                invoiceLine.Jbjobcode_name += "." + invoiceLine.Property_id_alt.Trim();
                                invoice.Property_id_alt = invoiceLine.Property_id_alt.Trim();
                                invoice.Userprofile_username = "KMETZGER_JC";
                            }
                        }

                        //add the single invoice lines
                        invoiceLines.Add(invoiceLine);

                        invoice.SetInvoiceLines(invoiceLines);

                        nexusApi.RefreshToken(1200);
                        //get the new invoices id to add images to it
                        int newInvoiceId = nexusApi.CreateInvoice(invoice);
                        nexusApi.AttachImageToInvoice(newInvoiceId, invoice.Userprofile_username, Directory.GetCurrentDirectory() + string.Format(@"\Equipment Invoice {0}.pdf", equipmentCharge.Invoice.Trim()));
                    }
                    catch(WebException err)
                    {
                        if (err.Response != null)
                        {
                            StreamReader response = new StreamReader(err.Response.GetResponseStream());
                            Dictionary<string, string> extras = new Dictionary<string, string>();
                            string returnedResponse = response.ReadToEnd();

                            if (!returnedResponse.Contains("Header validation error: Field 'invoice_period', provided value does not exists"))
                            {
                                extras.Add("Response", returnedResponse.Trim());

                                string errorMessage = returnedResponse.Trim();

                                JToken errors = JsonConvert.DeserializeObject<JToken>(errorMessage.Trim());

                                foreach (string error in errors["errors"].ToList())
                                {
                                    DataRow row = locTblResponse.NewRow();
                                    row["Detail"] = $"Invoice {equipmentCharge.Invoice.Trim()} for property {equipmentCharge.Entity.Trim()} - \n" +
                                        $"{equipmentCharge.EntityName.Trim()} cannot be uploaded. \n" +
                                        $"Please remove the other invoices from Nexus and try again.";
                                    row["Error"] = $@"{error.Trim()}";
                                    locTblResponse.Rows.Add(row);
                                }

                                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                                //HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                            }
                            else
                            {
                                DataRow row = locTblResponse.NewRow();
                                row["Detail"] = $"Invoice {equipmentCharge.Invoice.Trim()} for property {equipmentCharge.Entity.Trim()} - \n" +
                                                $"{equipmentCharge.EntityName.Trim()} cannot be uploaded. Please remove the other invoices from Nexus and try again.";
                                row["Error"] = $"This invoice cannot be uploaded yet. \n" +
                                               $"The period provided is not available. \n" +
                                               $"Please make sure that the expense period is the month you are in. \n" +
                                               $"Also, please check to make sure that period is not closed and the Equipment Rentals \n" +
                                               $"are being uploaded to the correct period.";
                                locTblResponse.Rows.Add(row);
                            }

                            response.Close();
                            response.Dispose();
                        }
                        else
                        {
                            //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                            //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                            //HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);

                            MessageBox.Show("Some of the invoices were unable to be added to Nexus. \n" +
                                            "Please remove the current ones and try the import again. \n" +
                                            "If the this persists contact IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch(HillAPI.Nexus.End_Points.Invoice.MismatchedLinesException err)
                    {
                        DataRow row = locTblResponse.NewRow();
                        row["Detail"] = $"Invoice {equipmentCharge.Invoice.Trim()} for property {equipmentCharge.Entity.Trim()} - \n" +
                            $"{equipmentCharge.EntityName.Trim()} cannot be uploaded. \n" +
                            $"Please remove the other invoices from Nexus and try again.";
                        row["Error"] = $@"{err.Message.Trim()}.";
                        locTblResponse.Rows.Add(row);
                    }
                    catch (Exception err)
                    {
                        //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                        //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                        //HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);

                        MessageBox.Show("Some of the invoices were unable to be added to Nexus. \n" +
                                        "Please remove the current ones and try the import again. \n" +
                                        "If the this persists contact IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        dgvErrors.DataSource = locTblResponse;
                        dgvErrors.RowsDefaultCellStyle.WrapMode = DataGridViewTriState.True;
                        dgvErrors.AllowUserToResizeRows = true;
                        dgvErrors.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.TopLeft;


                        dgvErrors.Columns[0].FillWeight = 3;
                        dgvErrors.Columns[1].FillWeight = 3;
                    }
                }
            }
            catch (IOException err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                //HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);

                MessageBox.Show("Some of the invoices were unable to be added to Nexus. " +
                                "Please remove the current ones and try the import again. \n" +
                                "If the this persists contact IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);

                MessageBox.Show("Some of the invoices were unable to be added to Nexus. " +
                                "Please remove the current ones and try the import again. \n" +
                                "If the this persists contact IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Checks if the property is valid property.
        /// </summary>
        /// <param name="property">The property id.</param>
        /// <returns>Returns false if the property id is not found, and true if the property is found.</returns>
        private bool IsValidProperty(string property)
        {
            try
            {
                int count = (from Entity entity in entities
                             where entity.EntityID == property.PadLeft(3, '0') &
                                   entity.DispositionDate == DateTime.MinValue
                             select entity).Count();

                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                return false;
            }
        }

        /// <summary>
        /// Reset the forms, its controls, and field values back to their default values.
        /// </summary>
        private void RefreshForm()
        {
            try
            {
                dgvEquipmentCharges.DataSource = null;
                invoiceEntries.Clear();
                Cursor = Cursors.Default;
                if (excel != null && !excel.IsClosed)
                {
                    excel.Close();
                }
                btnApprove.Enabled = false;
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }
        #endregion

        #region "Event Handlers"
        private void frmEqpmntRntInvcImp_Load(object sender, EventArgs e)
        {
            //formats the title bar to show the name and the databases selected
            this.Text = this.Text + " (MS SQL Server: " + Form1.sqlTransactions.GetDatabase().Trim() + ") (Nexus: " + Properties.Settings.Default.NexusURL + ")";

            mriAPI = new MRIAPI(string.Format("{0}/{1}/{2}/{3}", Properties.Settings.Default.ClientID.Trim(), Properties.Settings.Default.Database.Trim(), Properties.Settings.Default.UserID.Trim(),
                                                                 Properties.Settings.Default.AccessCode.Trim()), Properties.Settings.Default.Password.Trim());

            //get the properties in one call so as to not make multiple each time a property is validated
            entities = mriAPI.GetEntities();
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                this.Wait();

                dgvEquipmentCharges.DataSource = null;

                OpenFileDialog file = new OpenFileDialog()
                {
                    InitialDirectory = "C:\\",
                    Filter = "Excel files|*.xls;*.xlsx"
                };

                //allow the user to select a file
                file.ShowDialog();

                //ensure the user has selected a file
                if (file.FileName.Trim() != "")
                {
                    DataSet data = new DataSet();
                    int spreadSheetCount = 0;
                    int rowCount = 0;
                    int cellCount = 0;

                    try
                    {
                        excel = ExcelReaderFactory.CreateReader(File.Open(file.FileName, FileMode.Open, FileAccess.ReadWrite));
                    }
                    catch (IOException err)
                    {
                        MessageBox.Show("This file is already open. You cannot open a file that is already in open.",
                                        "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    data = excel.AsDataSet();

                    equipmentCharges = new List<EquipmentCharge>();

                    //go through each spreadsheet
                    foreach (DataTable sheet in data.Tables)
                    {
                        rowCount = 0;
                        decimal currentRate = 0M;
                        DateTime currentWeekEndingPeriod = DateTime.MinValue;
                        string currentEntity = "";
                        string currentName = "";
                        int currentPayPeriod = 0;
                        string currentEntityName = "";
                        DateTime currentExpensePeriod = DateTime.MinValue;
                        EquipmentCharge equipmentCharge = new EquipmentCharge();
                        //go through each row in the spreadsheet
                        foreach (DataRow row in sheet.Rows)
                        {
                            cellCount = 0;
                            EquipmentChargeLine equipmentChargeLine = new EquipmentChargeLine();
                            //go through each column in the row
                            foreach (object value in row.ItemArray)
                            {
                                //if the cell is not null
                                if (value != DBNull.Value)
                                {
                                    //if the row position is 0 and cell position is 0
                                    if (rowCount == 0 & cellCount == 0)
                                    {
                                        //set the equipment charge name
                                        equipmentCharge.Name = value.ToString().Trim();
                                        currentName = value.ToString().Trim();
                                    }

                                    //find the cell for rate
                                    if (value.ToString().Trim().ToUpper().Contains("RATE"))
                                    {
                                        //split the rate into 2 parts because it is formatted RATE:[value]
                                        string[] rateParts = value.ToString().Split(':');

                                        //if the parts of the field are not 2, it is not formatted correctly
                                        if (rateParts.Length != 2)
                                        {
                                            equipmentCharge.Errors.Add(string.Format("The rate for invoice {0} is not formatted" +
                                                                                     " properly on row {1}. It must entered as 'RATE:[Decimal].",
                                                                                     equipmentCharge.Name.Trim(), rowCount + 1));
                                        }
                                        else if (rateParts.Length == 2)
                                        {
                                            //if the second part is decimal
                                            if (decimal.TryParse(rateParts[1], out decimal rate))
                                            {
                                                //set the equipment charge rate to the value and current rate to the value
                                                equipmentCharge.Rate = rate;
                                                currentRate = rate;
                                            }//if the second part is not a decimal
                                            else
                                            {
                                                equipmentCharge.Errors.Add(string.Format("The rate for invoice {0} is not" +
                                                                                         " an number on row {1}. It must be a number.",
                                                                                         equipmentCharge.Name.Trim(), rowCount + 1));
                                            }
                                        }
                                    }

                                    //find the cell for the pay week ending
                                    if (value.ToString().Trim().ToUpper().Contains("PAY WEEK ENDING"))
                                    {
                                        //split the pay week ending into 2 parts because it is formatted PAY WEEK ENDING:[value]
                                        string[] payWeekEndingParts = value.ToString().Split(':');

                                        //if the field is not 2 parts, it is not formmatted properly
                                        if (payWeekEndingParts.Length != 2)
                                        {
                                            equipmentCharge.Errors.Add(string.Format("The Pay Week Ending Date for invoice {0} is not formatted" +
                                                                                     " properly on row {1}. It must entered as 'Pay Week Ending:[Date].",
                                                                                     equipmentCharge.Name.Trim(), rowCount + 1));
                                        }
                                        else if (payWeekEndingParts.Length == 2)
                                        {
                                            //try to parse the second part as a date time
                                            if (DateTime.TryParse(payWeekEndingParts[1], out DateTime payWeekEnding))
                                            {
                                                equipmentCharge.PayWeekEnding = payWeekEnding;
                                                currentWeekEndingPeriod = payWeekEnding;
                                            }//if it is not a date time, it is not formatted properly
                                            else
                                            {
                                                equipmentCharge.Errors.Add(string.Format("The Pay Week Ending Date for invoice {0} is not" +
                                                                                         " an date on row {1}. It must be a date.",
                                                                                         equipmentCharge.Name.Trim(), rowCount + 1));
                                            }
                                        }
                                    }

                                    //find the cell for the pay period
                                    if (value.ToString().Trim().ToUpper().Contains("PAY PERIOD"))
                                    {
                                        //split the pay period into 2 parts because it is formatted pay period:[value]
                                        string[] PayPeriodParts = value.ToString().Split(':');

                                        //if the field is not 2 parts, it is not formmatted properly
                                        if (PayPeriodParts.Length != 2)
                                        {
                                            equipmentCharge.Errors.Add(string.Format("The Pay Period for invoice {0} is not formatted" +
                                                                                     " properly on row {1}. It must entered as 'pay period:[number].",
                                                                                     equipmentCharge.Name.Trim(), rowCount + 1));
                                        }
                                        else if (PayPeriodParts.Length == 2)
                                        {
                                            //try to parse the second part as a number
                                            if (int.TryParse(PayPeriodParts[1], out int payPeriod))
                                            {
                                                equipmentCharge.PayPeriod = payPeriod;
                                                currentPayPeriod = payPeriod;
                                            }
                                            else if (payPeriod <= 0 | payPeriod >= 27){
                                                equipmentCharge.Errors.Add("The Pay Period must be between 1 and 26. Currently, it is not.");

                                            }//if it is not a number, it is not formatted properly
                                            else
                                            {
                                                equipmentCharge.Errors.Add(string.Format("The Pay Period for invoice {0} is not" +
                                                                                         " an number on row {1}. It must be a number.",
                                                                                         equipmentCharge.Name.Trim(), rowCount + 1));
                                            }
                                        }
                                    }

                                    //find the cell for the Expense Period
                                    if (value.ToString().Trim().ToUpper().Contains("EXPENSE PERIOD"))
                                    {
                                        //split the Expense Period into 2 parts because it is formatted PAY WEEK ENDING:[value]
                                        string[] expensePeriodParts = value.ToString().Split(':');

                                        //if the field is not 2 parts, it is not formmatted properly
                                        if (expensePeriodParts.Length != 2)
                                        {
                                            equipmentCharge.Errors.Add(string.Format("The Expense Period for invoice {0} is not formatted" +
                                                                                     " properly on row {1}. It must entered as 'Expense Period:[4 digit year][2 digit month].",
                                                                                     equipmentCharge.Name.Trim(), rowCount + 1));
                                        }
                                        else if (expensePeriodParts.Length == 2)
                                        {
                                            //try to parse the second part as a number
                                            if (DateTime.TryParse(expensePeriodParts[1], out DateTime expensePeriod))
                                            {
                                                equipmentCharge.ExpensePeriod = expensePeriod;
                                                currentExpensePeriod = expensePeriod;
                                            }//if it is not a number, it is not formatted properly
                                            else
                                            {
                                                equipmentCharge.Errors.Add(string.Format("The Expense Period for invoice {0} is not" +
                                                                                         " an date on row {1}. It must be a Date.",
                                                                                         equipmentCharge.Name.Trim(), rowCount + 1));
                                            }
                                        }
                                    }

                                    //if the position is cell 0 it is the entity formatted [ID]-[NAME]
                                    if (cellCount == 0)
                                    {

                                        //break the entity data into parts base on the ',' to detect if job cost
                                        string[] jobCostParts;
                                        jobCostParts = value.ToString().Split(',');

                                        //break the entity data into parts base on the '-'
                                        string[] entityParts;
                                        entityParts = value.ToString().Split('-');

                                        //if the entity is broken into 2 parts
                                        if (entityParts.Length == 2 | jobCostParts.Length == 4)
                                        {
                                            //check if it is a possible job cost first because the job cost 
                                            //can have a '-' which can be confused for an entity
                                            if (jobCostParts.Length == 4)
                                            {
                                                //if the current entity is not blank and the current row's entity id is not equal to the current entity id
                                                if (currentEntity != "" & currentEntity != entityParts[0].Trim())
                                                {
                                                    //add the equipment charge to the collection of equipment charges
                                                    equipmentCharges.Add(equipmentCharge);
                                                    //set the current entity to the entity id
                                                    currentEntity = value.ToString().Trim();
                                                    //set up new equipment charge
                                                    equipmentCharge = new EquipmentCharge();
                                                    //set the new equipment charge entity, payweek ending, and rate to the current values
                                                    equipmentCharge.Rate = currentRate;
                                                    equipmentCharge.PayWeekEnding = currentWeekEndingPeriod;
                                                    equipmentCharge.ExpensePeriod = currentExpensePeriod;
                                                    equipmentCharge.PayPeriod = currentPayPeriod;
                                                    equipmentCharge.Entity = currentEntity.Trim();
                                                    equipmentCharge.EntityName = "";
                                                    equipmentCharge.Name = "";
                                                }//if no current entity has been set
                                                else if (currentEntity == "")
                                                {
                                                    //set the current entity to current row entity
                                                    currentEntity = value.ToString().Trim();
                                                    equipmentCharge.Entity = currentEntity.Trim();
                                                    equipmentCharge.EntityName = "";
                                                    equipmentCharge.Name = "";
                                                }
                                            }
                                            else 
                                            { 
                                                //check that property is a valid property
                                                if (!IsValidProperty(entityParts[0].Trim().PadLeft(3, '0')))
                                                {
                                                    equipmentChargeLine.Errors.Add(string.Format("Equipment charge line on line {0} does not have a valid propery id.",
                                                                                                  rowCount + 1));
                                                }
                                                else
                                                {
                                                    //if the current entity is not blank and the current row's entity id is not equal to the current entity id
                                                    if (currentEntity != "" & currentEntity != entityParts[0].Trim())
                                                    {
                                                        //add the equipment charge to the collection of equipment charges
                                                        equipmentCharges.Add(equipmentCharge);
                                                        //set the current entity to the entity id
                                                        currentEntityName = entityParts[1].Trim();
                                                        currentEntity = entityParts[0].Trim();
                                                        //set up new equipment charge
                                                        equipmentCharge = new EquipmentCharge();
                                                        //set the new equipment charge entity, payweek ending, and rate to the current values
                                                        equipmentCharge.Rate = currentRate;
                                                        equipmentCharge.PayWeekEnding = currentWeekEndingPeriod;
                                                        equipmentCharge.ExpensePeriod = currentExpensePeriod;
                                                        equipmentCharge.PayPeriod = currentPayPeriod;
                                                        equipmentCharge.Entity = currentEntity.Trim();
                                                        equipmentCharge.EntityName = currentEntityName.Trim();
                                                        equipmentCharge.Name = currentName.Trim();
                                                    }//if no current entity has been set
                                                    else if (currentEntity == "")
                                                    {
                                                        //set the current entity to current row entity
                                                        currentEntityName = entityParts[1].Trim();
                                                        currentEntity = entityParts[0].Trim();
                                                        equipmentCharge.Entity = currentEntity.Trim();
                                                        equipmentCharge.EntityName = currentEntityName.Trim();
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    //if the position is cell 4 the rows under it are the hours
                                    if (cellCount == 4)
                                    {
                                        //if the current entity is set and the Week Endding Period is set and the Rate is set, and there is data in the cell
                                        if (currentEntity != "" & currentWeekEndingPeriod != DateTime.MinValue & currentRate != 0)
                                        {
                                            //try to parse out the hours
                                            if (decimal.TryParse(value.ToString().Trim(), out decimal hours))
                                            {
                                                equipmentChargeLine.Hours = hours;
                                                equipmentCharge.EquipmentChangeLines.Add(equipmentChargeLine);
                                                equipmentChargeLine = new EquipmentChargeLine();
                                            }//else the cell data is not formatted as a number
                                            else
                                            {
                                                equipmentChargeLine.Errors.Add(string.Format("The hours entered" +
                                                                                             " on row {0} is not a number. The hours need to be be number.",
                                                                                             rowCount + 1));
                                            }
                                        }
                                    }
                                }

                                cellCount++;
                            }

                            rowCount++;
                        }

                        //add the equipment charge to the collection of equipment charges
                        equipmentCharges.Add(equipmentCharge);

                        spreadSheetCount++;
                    }

                    DataTable locTblEquipmentCharges = new DataTable();

                    locTblEquipmentCharges.Columns.Add("Property");
                    locTblEquipmentCharges.Columns.Add("Pay Week Ending");
                    locTblEquipmentCharges.Columns.Add("Pay Period");
                    locTblEquipmentCharges.Columns.Add("Expense Period");
                    locTblEquipmentCharges.Columns.Add("Invoice");
                    locTblEquipmentCharges.Columns.Add("Rate");
                    locTblEquipmentCharges.Columns.Add("Quantity");
                    locTblEquipmentCharges.Columns.Add("Price");

                    foreach(EquipmentCharge equipment in equipmentCharges)
                    {
                        DataRow row = locTblEquipmentCharges.NewRow();

                        //check if it is a job code
                        string[] entityParts = equipment.Entity.Split(',');

                        if (entityParts.Length == 4)
                        {
                            row["Property"] = equipment.GetJobEntityInfo(entityParts);
                        }
                        else
                        {
                            row["Property"] = equipment.Entity.Trim() + "-" + equipment.EntityName.Trim();
                        }
                        row["Pay Week Ending"] = equipment.PayWeekEnding.ToString("MM/dd/yyyy");
                        row["Pay Period"] = equipment.PayPeriod.ToString().Trim();
                        row["Expense Period"] = equipment.ExpensePeriod.ToString().Trim();
                        row["Invoice"] = equipment.Invoice.Trim();
                        row["Rate"] = equipment.Rate;
                        row["Quantity"] = equipment.GetTotalHours();
                        row["Price"] = equipment.GetTotalCharges();

                        locTblEquipmentCharges.Rows.Add(row);
                    }

                    dgvEquipmentCharges.DataSource = locTblEquipmentCharges;

                    //close the excel spreadsheet so that it can be used by other programs
                    excel.Close();

                    //if there is at least one invoice with no errors and has at least one line, enable the approve button to allow the invoices to be sent to MRI
                    if (equipmentCharges.Where(x => x.EquipmentChangeLines.Count > 0 & !x.HasErrors).Count() > 0)
                    {
                        btnApprove.Enabled = true;
                    }

                    //translate the data from the invoices in the spreadsheet into the MRIAPI invoice
                    foreach (EquipmentCharge equipmentCharge in equipmentCharges.Where(x => !x.HasErrors & x.EquipmentChangeLines.Count > 0))
                    {
                        HillAPI.MRI.End_Points.Invoice.entry invoiceEntry = new HillAPI.MRI.End_Points.Invoice.entry();
                        invoiceEntry.InvoiceNumber = equipmentCharge.Invoice.Trim();
                        invoiceEntry.Description = equipmentCharge.Name.Trim();
                        invoiceEntry.DueDate = equipmentCharge.PayWeekEnding.ToString("yyyy-MM-dd HH:mm:ss.fff");
                        invoiceEntry.ExpensePeriod = equipmentCharge.ExpensePeriod.ToString().Trim();
                        invoiceEntry.VendorID = "HMCONT";
                        invoiceEntry.InvoiceDate = equipmentCharge.PayWeekEnding.ToString("yyyy-MM-dd HH:mm:ss.fff");
                        invoiceEntry.EntryCurrencyCode = "USD";
                        invoiceEntry.SeparateCheck = "Y";
                        invoiceEntry.InvoiceTypeID = "INV";

                        HillAPI.MRI.End_Points.Invoice.History invoiceLineEntries = new HillAPI.MRI.End_Points.Invoice.History();
                        //used to track the number of invoice lines
                        int numberOfInvoiceLines = 1;
                        HillAPI.MRI.End_Points.Invoice.History.Entry invoiceLineEntry = new HillAPI.MRI.End_Points.Invoice.History.Entry();

                        invoiceLineEntry.AccountNumber = "HS652000000000";
                        invoiceLineEntry.InvoiceNumber = equipmentCharge.Invoice.Trim();
                        invoiceLineEntry.ItemNumber = numberOfInvoiceLines;
                        invoiceLineEntry.EntityID = equipmentCharge.Entity.ToString().Trim().PadLeft(3, '0');
                        invoiceLineEntry.ItemAmount = equipmentCharge.GetTotalCharges();
                        invoiceLineEntry.Status = "R";
                        invoiceLineEntry.Reference = invoiceEntry.InvoiceNumber.Trim();
                        invoiceLineEntry.TaxAmount = 0M;
                        invoiceLineEntry.Department = "@";
                        invoiceLineEntry.TaxItem = "N";

                        numberOfInvoiceLines++;

                        invoiceLineEntries.Entries.Add(invoiceLineEntry);

                        invoiceEntry.History = invoiceLineEntries;

                        invoiceEntries.Add(invoiceEntry);
                    }
                    //create the datatable for invoice errors
                    DataTable locTblErrors = new DataTable();

                    locTblErrors.Columns.Add("Detail");
                    locTblErrors.Columns.Add("Error");

                    //go through each invoice headers errors and add to the invoice errors datatable
                    foreach (EquipmentCharge equipment in equipmentCharges.Where(x => x.EquipmentChangeLines.Count != 0))
                    {
                        DataRow row;
                        foreach (string error in equipment.Errors)
                        {
                            row = locTblErrors.NewRow();
                            row["Detail"] = string.Format("Invoice {0} Header", equipment.Name.Trim());
                            row["Error"] = error.Trim();

                            locTblErrors.Rows.Add(row);
                        }

                        //go through each invoice lines errors and add to the invoice errors datatable
                        foreach (EquipmentChargeLine equipmentLine in equipment.EquipmentChangeLines)
                        {
                            foreach (string error in equipmentLine.Errors)
                            {
                                row = locTblErrors.NewRow();
                                row["Detail"] = string.Format("Invoice {0} Property {1}", equipment.Name.Trim(),
                                                              equipment.Entity.Trim() + "-" + equipment.EntityName.Trim());
                                row["Error"] = error.Trim();

                                locTblErrors.Rows.Add(row);
                            }
                        }
                    }

                    //set the invoice errors datatable to the grid
                    dgvErrors.DataSource = locTblErrors;

                    //set the invoice datatable to the grid
                    dgvEquipmentCharges.DataSource = locTblEquipmentCharges;

                    dgvErrors.AutoResizeColumns();

                    dgvEquipmentCharges.AutoResizeColumns();

                    data.Clear();
                    data.Dispose();
                    data = null;
                }
            }
            catch (Exception err)
            {
                if ((excel != null) && (!excel.IsClosed))
                {
                    excel.Close();
                }
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            finally
            {
                if (excel != null && !excel.IsClosed)
                {
                    excel.Close();
                    excel.Dispose();
                    excel = null;
                }
                this.Useable();
            }
        }

        private void frmEqpmntRntInvcImp_FormClosing(object sender, FormClosingEventArgs e)
        {
            entities.Clear();
            entities = null;
            mriAPI = null;
        }

        private void dgvEquipmentCharges_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (dgvEquipmentCharges.Columns[e.ColumnIndex].Name.ToUpper() == "PRICE" & decimal.TryParse(e.Value.ToString().Trim(), out decimal formattedValue))
                {
                    if (e.Value.ToString().Trim() != "")
                    {
                        e.Value = formattedValue.ToString("c2");
                    }
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            RefreshForm();
            dgvErrors.DataSource = null;
        }

        private void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                this.Wait();

                CreateInvoicePDFs(equipmentCharges);
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            finally
            {
                RefreshForm();

                this.Useable();
            }
        }
        #endregion
    }
}
