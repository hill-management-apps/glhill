﻿using ExcelDataReader;
using HelpfulClasses;
using HillAPI.MRI;
using HillAPI.MRI.End_Points;
using SqlDataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using DataTable = System.Data.DataTable;

namespace GLHill.Forms
{
    public partial class frmInvcImp : frmImportBase
    {
        private IExcelDataReader excel;
        private List<HillAPI.MRI.End_Points.Invoice.entry> invoiceEntries = new List<HillAPI.MRI.End_Points.Invoice.entry>();
        private List<string> CompanyIDs = new List<string>() { "HM", "HSC", "PK", "PM", "WCC", "HO" };

        #region "Constructors"
        public frmInvcImp()
        {
            InitializeComponent();
        }
        #endregion

        #region "Methods"
        /// <summary>
        /// Reset the forms, its controls, and field values back to their default values.
        /// </summary>
        private void RefreshForm()
        {
            try
            {
                dgvInvoices.DataSource = null;
                dgvErrors.DataSource = null;
                invoiceEntries.Clear();
                Cursor = Cursors.Default;
                if (excel != null && !excel.IsClosed)
                {
                    excel.Close();
                }
                btnApprove.Enabled = false;
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }
        #endregion

        #region "Event Handlers"
        private void frmInvcImp_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                //formats the title bar to show the name and the databases selected
                this.Text = this.Text + " (MS SQL Server: " + Form1.sqlTransactions.GetDatabase().Trim() + ")";

                //sqlTransactions.Connect();
                mriAPI = new MRIAPI(string.Format("{0}/{1}/{2}/{3}", Properties.Settings.Default.ClientID.Trim(), Properties.Settings.Default.Database.Trim(), Properties.Settings.Default.UserID.Trim(),
                                                                     Properties.Settings.Default.AccessCode.Trim()), Properties.Settings.Default.Password.Trim());

                //get the departments in one call so as to not make multiple each time a department is validated
                departments = mriAPI.GetDepartments();
                //get the accounts in one call so as to not make multiple each time an account is validated
                accounts = mriAPI.GetAccounts();
                //get the properties in one call so as to not make multiple each time a property is validated
                entities = mriAPI.GetEntities();
            }
            catch(Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                this.Close();
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                OpenFileDialog file = new OpenFileDialog()
                {
                    InitialDirectory = "C:\\",
                    Filter = "Excel files|*.xls;*.xlsx"
                };
                List<Invoice> invoices = new List<Invoice>();
                DataTable locTblPayroll = new DataTable();
                DataSet dsPayroll = new DataSet();

                //allow the user to select a file
                file.ShowDialog();

                //ensure the user has selected a file
                if (file.FileName.Trim() != "")
                {

                    RefreshForm();

                    DataSet data = new DataSet();
                    int spreadSheetCount = 0;
                    int rowCount = 0;
                    int cellCount = 0;

                    excel = ExcelReaderFactory.CreateReader(File.Open(file.FileName, FileMode.Open, FileAccess.ReadWrite));

                    data = excel.AsDataSet();

                    //parse out the data into modified tables that catch extend upon allocation records
                    foreach (DataTable locTblDataSet in data.Tables)
                    {
                        locTblPayroll = locTblDataSet.Clone();
                        locTblPayroll.Columns.Add("JOBCOST");

                        foreach (DataRow row in locTblDataSet.Rows)
                        {
                            DataRow r;
                            string code = row[1].ToString().Trim();
                            double amount = 0D;
                            bool isAllocation = false;
                            bool isJobCost = false;
                            string strSQL = "";

                            try
                            {//check if it is an allocation code
                                isAllocation = (from DataRow percentageRow in locTblAllocations.Rows
                                                where percentageRow["ALLOCCODE"].ToString().Trim() == code.Trim()
                                                select percentageRow).Count() >= 1 ? true : false;
                            }
                            catch (ArgumentNullException err)
                            {

                            }
                            catch (InvalidOperationException err)
                            {

                            }

                            try
                            {//check if it is a job code
                                isJobCost = IsValidJob(code.Trim().Split(','));
                            }
                            catch (ArgumentNullException err)
                            {

                            }
                            catch (InvalidOperationException err)
                            {

                            }

                            double.TryParse(row[2].ToString().Trim(), out amount);

                            if (isAllocation)
                            {
                                CompanyIDs.Add(code.Trim());

                                int count = 0;
                                r = locTblAllocations.NewRow();

                                foreach (object columnValue in row.ItemArray)
                                {
                                    //check that none of the cells are null
                                    if (columnValue != DBNull.Value & columnValue != string.Empty)
                                    {
                                        if (decimal.TryParse(columnValue.ToString().Trim(), out decimal decimalValue))
                                        {

                                            DataTable locTblAllocations = hillWebService.GetAllocations(DateTime.Now.Period(), decimalValue, code.Trim());

                                                foreach (DataRow allocationLine in locTblAllocations.Rows)
                                                {
                                                    r = locTblPayroll.NewRow();
                                                    r[0] = row[0].ToString().Trim();
                                                    r[1] = allocationLine[2].ToString().Trim();
                                                    r[count] = allocationLine[3].ToString().Trim();

                                                    locTblPayroll.Rows.Add(r);
                                                }   
                                        }
                                    }

                                    count++;
                                }

                                count = 0;
                            }
                            else if (isJobCost)
                            {
                                CompanyIDs.Add(code.Trim());

                                int count = 0;
                                r = locTblAllocations.NewRow();

                                foreach (object columnValue in row.ItemArray)
                                {
                                    //check that none of the cells are null
                                    if (columnValue != DBNull.Value & columnValue != string.Empty)
                                    {
                                        if (decimal.TryParse(columnValue.ToString().Trim(), out decimal decimalValue))
                                        {
                                            string[] jobCostParts = code.Trim().Split(',');

                                            if (count == 4)
                                            {
                                                r = locTblPayroll.NewRow();
                                                r[0] = row[0].ToString().Trim();
                                                r[1] = GetJobEntityId(code.Trim().Split(','));
                                                r[2] = GetJobName(code.Trim().Split(','));
                                                r[count] = decimalValue.ToString().Trim();
                                                r["JOBCOST"] = code.Trim();

                                                locTblPayroll.Rows.Add(r);
                                            }
                                            else if (count > 4)
                                            {
                                                try
                                                {
                                                    r = locTblPayroll.Select($"Column1 = '{GetJobEntityId(code.Trim().Split(',')).ToString().Trim()}'")[0];
                                                    r[count] = decimalValue.ToString().Trim();
                                                }
                                                catch (Exception err)
                                                {
                                                    MessageBox.Show(err.Message.Trim(),
                                                                    "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                }
                                            }
                                        }
                                    }

                                    count++;
                                }

                                count = 0;
                            }
                            else
                            {
                                r = locTblPayroll.NewRow();

                                for (int i = 0; i < row.Table.Columns.Count - 1; i++)
                                {
                                    r[i] = row[i].ToString().Trim();
                                }

                                locTblPayroll.Rows.Add(r);
                            }

                            r = null;
                        }

                        //place all the data back into the modified data set with job cost and allocation data if applicable
                        dsPayroll.Tables.Add(locTblPayroll.Copy());
                        locTblPayroll.Clear();
                    }

                    //go through each spreadsheet
                    foreach (DataTable sheet in dsPayroll.Tables)
                    {
                        DateTime payDate = DateTime.MinValue;
                        DateTime weekEnding = DateTime.MinValue;
                        string currentCompanyCode = "";
                        Invoice invoice = new Invoice();

                        //this will hold the column position as key, of the Ledger Code and Account
                        Dictionary<int, string[]> ledgerCodeAccounts = new Dictionary<int, string[]>();

                        //go through each spreadsheet row
                        foreach (DataRow row in sheet.Rows)
                        {
                            invoice.InvoiceDescription = sheet.TableName.Trim() + " " + currentCompanyCode;
                            invoice._Invoice = sheet.TableName.Trim() + " " + currentCompanyCode;

                            //go through each column in the row
                            foreach (object value in row.ItemArray)
                            {
                                //if the cell is not null
                                if (value != DBNull.Value)
                                {

                                    //check if the week ending is in the correct place
                                    if (rowCount == 3 & cellCount == 0)
                                    {
                                        //Week ending cell is "WEEK ENDING: " + [end week date], so it checks that is at the beginning of the cell's value
                                        if (value.ToString().ToUpper().StartsWith("WEEK ENDING:"))
                                        {

                                            //check the value in the is a valid date after the "WEEK ENDING:"
                                            if (DateTime.TryParse(value.ToString().ToUpper().Substring("WEEK ENDING:".Length), out weekEnding))
                                            {
                                                invoice.WeekEnding = weekEnding;
                                            }
                                            else
                                            {
                                                invoice.Errors.Add("The 'Week Ending' date is not formatted as a date. Please make sure that it formatted" +
                                                                   " month / day / year.");
                                            }
                                        }
                                        else
                                        {
                                            invoice.Errors.Add("The week ending date is not a valid date. It must be in cell A4 and be formatted" +
                                                               " WEEK ENDING: [Date]");
                                        }
                                    }

                                    //check if the pay date is in the correct place
                                    if (rowCount == 4 & cellCount == 0)
                                    {
                                        //Pay date cell is "PAY DATE: " + [pay date], so it checks that is at the beginning of the cell's value
                                        if (value.ToString().ToUpper().StartsWith("PAY DATE:"))
                                        {

                                            //check the value in the is a valid date after the "PAY DATE:"
                                            if (DateTime.TryParse(value.ToString().ToUpper().Substring("PAY DATE:".Length), out payDate))
                                            {
                                                invoice.PayDate = payDate;
                                            }
                                            else
                                            {
                                                invoice.Errors.Add("The 'Pay Date' date is not formatted as a date. Please make sure that it formatted" +
                                                                   " month / day / year.");
                                            }
                                        }
                                        else
                                        {
                                            invoice.Errors.Add("The pay date is not a valid date. It must be in cell A4 and be formatted" +
                                                               " PAY DATE: [Date]");
                                        }
                                    }

                                    //if the first cell in row has "Set Ledger" the other cells are the Ledger Code and Ledger Account pairs, and they override the existing ones
                                    if (value.ToString().ToUpper().Trim() == "SET LEDGER")
                                    {
                                        //helps determine the position of the ledger code and ledger account and the array is only a partial array starting at index one so it needs to start at one
                                        int count = 1;

                                        //clears the ledger code account pairs to add the new ones
                                        ledgerCodeAccounts.Clear();

                                        //go through each column
                                        foreach (object columnValue in row.ItemArray.Slice(1, row.ItemArray.Length - 1))
                                        {

                                            //check that none of the cells are null
                                            if (columnValue != DBNull.Value & columnValue != string.Empty)
                                            {
                                                //split the value in the cell, since it is formatted [Ledger Code] [Ledger Account]
                                                string[] ledgerData = columnValue.ToString().Split(' ');

                                                //check if the ledger data in the cell is not 2 parts seperate by a space
                                                if (ledgerData.Length != 2)
                                                {
                                                    invoice.Errors.Add(string.Format("The value of {0} in row {1}, " +
                                                                                     "is not a valid Account. Please make sure " +
                                                                                     "that you format the Accounts [Ledger Code] [Account Code].", value.ToString().Trim(), rowCount + 1));
                                                } //check if the second part of the cell is a 6 digits, as any valid account must be 6 digits long
                                                else if ((ledgerData[1].Trim().Length != 6) | (!int.TryParse(ledgerData[1].Trim(), out int a)))
                                                {
                                                    invoice.Errors.Add(string.Format("Account {0} is not valid. All accounts must be 6 digits.", ledgerData[1].Trim()));
                                                }//check that the account and ledger code is valid
                                                else if (!IsValidAccount(ledgerData[0].Trim(), ledgerData[1].Trim()))
                                                {
                                                    invoice.Errors.Add(string.Format("Account {0} for ledger code {1} does not exists.", ledgerData[1].Trim(), ledgerData[0]));
                                                }
                                                else
                                                {
                                                    ledgerCodeAccounts.Add(count, ledgerData);
                                                }
                                            }

                                            count++;
                                        }

                                        count = 0;
                                    }

                                    //determine if there is a company id in the first column, if so parse the data as an invoice line
                                    if ((cellCount == 0) & CompanyIDs.Contains(value.ToString().Trim()))
                                    {
                                        //if there is no current company invoice items found, ignore this code
                                        if (currentCompanyCode.Trim() != "")
                                        {
                                            //compare the company code of the invoice line to last company code and if they not the same
                                            if (currentCompanyCode.Trim() != value.ToString().Trim())
                                            {
                                                //add the current invoice to list of invoices
                                                invoices.Add(invoice);
                                                //set the current company to this invoice line's company
                                                currentCompanyCode = value.ToString().Trim();
                                                //create a new invoice
                                                invoice = new Invoice();
                                                invoice.WeekEnding = weekEnding;
                                                invoice.PayDate = payDate;
                                                invoice.InvoiceDescription = row.Table.TableName.Trim();
                                                invoice._Invoice = row.Table.TableName.Trim();
                                            }
                                        }
                                        else
                                        {
                                            currentCompanyCode = value.ToString().Trim();
                                            invoice.InvoiceDescription += " " + currentCompanyCode.Trim();
                                            invoice._Invoice += " " + currentCompanyCode.Trim();
                                        }

                                        InvoiceLine invoiceLine = new InvoiceLine();
                                        //go through each account
                                        foreach (KeyValuePair<int, string[]> ledgerCodeAccount in ledgerCodeAccounts)
                                        {
                                            //check that the property column is not blank
                                            if (row.ItemArray[1].ToString().Trim() == "")
                                            {
                                                invoiceLine.Errors.Add(string.Format("Invoice line for " +
                                                                                     "company {0} on line {1} does not have a property.", value.ToString().Trim(), rowCount + 1));
                                            }
                                            //check that property is a valid property
                                            if (!IsValidProperty(row.ItemArray[1].ToString().Trim().Split('-')[0].PadLeft(3, '0')))
                                            {
                                                invoiceLine.Errors.Add(string.Format("Invoice line for company {0} on line {1} does not have a valid propery id.",
                                                                                     value.ToString().Trim(), rowCount + 1));
                                            }
                                            //check that account is a numbeer
                                            if (!int.TryParse(ledgerCodeAccount.Value[1].Trim(), out int account))
                                            {
                                                invoiceLine.Errors.Add(string.Format("The account on line {0} is not a number. The accounts are always numbers.", rowCount + 1));
                                            }
                                            //check that the amount for the ledger code and account is a number
                                            if ((!decimal.TryParse(row.ItemArray[ledgerCodeAccount.Key].ToString().Trim(), out decimal amount) && (row.ItemArray[ledgerCodeAccount.Key].ToString().Trim() != "")))
                                            {
                                                invoiceLine.Errors.Add(string.Format("The amount in column {0} of line {1} is not a number.", ledgerCodeAccount.Key, rowCount + 1));
                                            }

                                            //skip the invoice line if the amount is blank or 0
                                            if (row.ItemArray[ledgerCodeAccount.Key].ToString().Trim() != "" & amount != 0M)
                                            {
                                                string[] projectDepartment;
                                                projectDepartment = row.ItemArray[1].ToString().Trim().Split('-');

                                                invoiceLine.Project = projectDepartment[0].Trim();

                                                //if the project is not 322
                                                if (invoiceLine.Project != "322")
                                                {//set the department to "@"
                                                    invoiceLine.Department = "@";
                                                }
                                                else
                                                {
                                                    //if there is a dash seperating the project and department 
                                                    if (projectDepartment.Length > 1 & account >= 499999)
                                                    {//the department is the second part of the string
                                                        invoiceLine.Department = projectDepartment[1].Trim();
                                                    }
                                                    else//if there is not dash
                                                    {//set the department to "@"
                                                        invoiceLine.Department = "@";
                                                    }
                                                }

                                                //validate the department
                                                if (!IsValidDepartment(invoiceLine.Department.Trim()))
                                                {
                                                    invoiceLine.Errors.Add(string.Format("The department {0} on line {1} is not correct.", invoiceLine.Department.Trim(), rowCount + 1));
                                                }

                                                invoiceLine.ProjectDescription = row.ItemArray[2].ToString().Trim();
                                                invoiceLine.Account = account;
                                                invoiceLine.LedgerCode = ledgerCodeAccount.Value[0].Trim();
                                                invoiceLine.Amount = amount;
                                                invoiceLine.Company = value.ToString().Trim();

                                                //validate that ledger code and accounts are set properly for the company
                                                if (invoiceLine.Company.Trim() == "WCC" & ledgerCodeAccount.Value[0].Trim() != "WC")
                                                {
                                                    invoiceLine.Errors.Add(string.Format("The company WCC on line {0} can only WC ledger code " +
                                                                                         "and accounts. Please set the proper ledger code and accounts.", rowCount + 1));
                                                }
                                                else if (invoiceLine.Company.Trim() != "WCC" & ledgerCodeAccount.Value[0].Trim() != "HS")
                                                {
                                                    invoiceLine.Errors.Add(string.Format("The company {0} on line {1} " +
                                                                           "can only use HS ledger code and accounts. Please set the proper " +
                                                                           "ledger code and accounts.", invoiceLine.Company.Trim(), rowCount + 1));
                                                }

                                                if (row.ItemArray[row.ItemArray.Length - 1] != DBNull.Value & int.TryParse(row.ItemArray[row.ItemArray.Length - 1].ToString().Trim(), out int l))
                                                {
                                                    //check for any unforseen errors
                                                    try
                                                    {
                                                        invoiceLine.Check = int.Parse(row.ItemArray[row.ItemArray.Length - 1].ToString().Trim());
                                                    }
                                                    catch (Exception err)
                                                    {
                                                        invoiceLine.Errors.Add(string.Format("The check could not be parsed for line {0}. MESSAGE:{1}. STACKTRACE:{2}.", rowCount + 1, err.Message.Trim(), err.StackTrace.Trim()));
                                                    }
                                                }

                                                if (row.ItemArray[3] != DBNull.Value & int.TryParse(row.ItemArray[3].ToString().Trim(), out int m))
                                                {
                                                    //check for any unforseen errors
                                                    try
                                                    {
                                                        invoiceLine.Employee = int.Parse(row.ItemArray[3].ToString().Trim());
                                                    }
                                                    catch (Exception err)
                                                    {
                                                        invoiceLine.Errors.Add(string.Format("The employee could not be parsed for line {0}. MESSAGE:{1}. STACKTRACE:{2}.", rowCount + 1, err.Message.Trim(), err.StackTrace.Trim()));
                                                    }
                                                }

                                                if (row["JOBCOST"] != DBNull.Value)
                                                {
                                                    invoiceLine.JobCostParts = row["JOBCOST"].ToString().Trim();
                                                }

                                                invoice.InvoiceLines.Add(invoiceLine);
                                            }
                                            invoiceLine = new InvoiceLine();
                                        }
                                    }
                                }

                                //track the current cell
                                cellCount++;
                            }

                            //track the current row and reset the cell count for the next row
                            cellCount = 0;
                            rowCount++;
                        }

                        //trach the spreadsheet and reset the row count for the next spreadsheet
                        rowCount = 0;
                        spreadSheetCount++;

                        invoices.Add(invoice);

                        ledgerCodeAccounts.Clear();
                        ledgerCodeAccounts = null;
                        invoice = null;
                    }

                    //close the excel spreadsheet so that it can be used by other programs
                    excel.Close();

                    //if there is at least one invoice with no errors and has at least one line, enable the approve button to allow the invoices to be sent to MRI
                    if (invoices.Where(x => x.InvoiceLines.Count > 0 & !x.HasErrors).Count() > 0)
                    {
                        btnApprove.Enabled = true;
                    }

                    //translate the data from the invoices in the spreadsheet into the MRIAPI invoice
                    foreach (Invoice invoice in invoices.Where(x => !x.HasErrors & x.InvoiceLines.Count > 0))
                    {
                        HillAPI.MRI.End_Points.Invoice.entry invoiceEntry = new HillAPI.MRI.End_Points.Invoice.entry();
                        invoiceEntry.InvoiceNumber = invoice._Invoice.Trim();
                        invoiceEntry.Description = invoice.InvoiceDescription;
                        invoiceEntry.DueDate = invoice.PayDate.ToString("yyyy-MM-dd HH:mm:ss.fff");
                        invoiceEntry.ExpensePeriod = invoice.PayDate.Year.ToString().PadLeft(4, '0') + "" + invoice.PayDate.Month.ToString().PadLeft(2, '0');
                        invoiceEntry.VendorID = "HILLPR";
                        invoiceEntry.InvoiceDate = invoice.PayDate.ToString("yyyy-MM-dd HH:mm:ss.fff");
                        invoiceEntry.EntryCurrencyCode = "USD";
                        invoiceEntry.SeparateCheck = "Y";
                        invoiceEntry.InvoiceTypeID = "INV";

                        HillAPI.MRI.End_Points.Invoice.History invoiceLineEntries = new HillAPI.MRI.End_Points.Invoice.History();
                        //used to track the number of invoice lines
                        int numberOfInvoiceLines = 1;
                        foreach (InvoiceLine invoiceLine in invoice.InvoiceLines.Where(x => !x.HasErrors))
                        {
                            HillAPI.MRI.End_Points.Invoice.History.Entry invoiceLineEntry = new HillAPI.MRI.End_Points.Invoice.History.Entry();

                            invoiceLineEntry.AccountNumber = invoiceLine.LedgerCode.Trim() + invoiceLine.Account.ToString().Trim().PadRight(12, '0');
                            invoiceLineEntry.InvoiceNumber = invoice._Invoice.Trim();
                            invoiceLineEntry.ItemNumber = numberOfInvoiceLines;
                            invoiceLineEntry.EntityID = invoiceLine.Project.ToString().Trim().PadLeft(3, '0');
                            invoiceLineEntry.ItemAmount = invoiceLine.Amount;
                            invoiceLineEntry.Status = "R";
                            invoiceLineEntry.Reference = invoiceEntry.InvoiceNumber.Trim();
                            invoiceLineEntry.TaxAmount = 0M;
                            invoiceLineEntry.Department = invoiceLine.Department.Trim();
                            invoiceLineEntry.TaxItem = "N";

                            if(invoiceLine.JobCode != "")
                            {
                                invoiceLineEntry.JobCode = invoiceLine.JobCode.Trim();
                                invoiceLineEntry.PhaseCode = invoiceLine.PhaseCode.Trim();
                                invoiceLineEntry.CostList = invoiceLine.CostList.Trim();
                                invoiceLineEntry.CostCode = invoiceLine.CostCode.Trim();
                            }

                            numberOfInvoiceLines++;

                            invoiceLineEntries.Entries.Add(invoiceLineEntry);
                        }

                        invoiceEntry.History = invoiceLineEntries;

                        invoiceEntries.Add(invoiceEntry);
                    }

                    //clear the invoice errors to allow the new ones to be displayed
                    dgvErrors.DataSource = null;

                    //clear the invoices to allow the new ones to be displayed
                    dgvInvoices.DataSource = null;

                    //create the data table to display in the grid
                    DataTable locTblInvoices = new DataTable();

                    locTblInvoices.Columns.Add("Company");
                    locTblInvoices.Columns.Add("Project");
                    locTblInvoices.Columns.Add("ProjectDescription");
                    locTblInvoices.Columns.Add("Department");
                    locTblInvoices.Columns.Add("Employee");
                    locTblInvoices.Columns.Add("LedgerType");
                    locTblInvoices.Columns.Add("Account");
                    locTblInvoices.Columns.Add("LedgerCode");
                    locTblInvoices.Columns.Add("Amount");
                    locTblInvoices.Columns.Add("Check");

                    //go through each invoice
                    foreach (Invoice invoice in invoices.Where(x => !x.HasErrors & x.InvoiceLines.Count > 0))
                    {
                        DataRow newRow;
                        string lastLedgerCode = "";
                        string lastCompany = "";
                        //add all their lines to the data table
                        foreach (InvoiceLine invoiceLine in invoice.InvoiceLines)
                        {
                            newRow = locTblInvoices.NewRow();
                            newRow["Company"] = invoiceLine.Company.Trim();
                            newRow["Project"] = invoiceLine.Project.ToString().Trim();
                            newRow["ProjectDescription"] = invoiceLine.ProjectDescription.Trim();
                            newRow["Department"] = invoiceLine.Department.Trim();
                            newRow["Employee"] = invoiceLine.Employee.ToString().Trim();
                            newRow["LedgerType"] = invoiceLine.LedgerType.Trim();
                            newRow["Account"] = invoiceLine.Account.ToString().Trim();
                            newRow["LedgerCode"] = invoiceLine.LedgerCode.Trim();
                            newRow["Amount"] = invoiceLine.Amount.ToString().Trim();
                            newRow["Check"] = invoiceLine.Check.ToString().Trim();

                            locTblInvoices.Rows.Add(newRow);

                            lastLedgerCode = invoiceLine.LedgerCode.Trim();
                            lastCompany = invoiceLine.Company.Trim();
                        }
                        //add a blank record to seperate the invoices
                        newRow = locTblInvoices.NewRow();
                        newRow["Company"] = "";
                        newRow["Project"] = "";
                        newRow["ProjectDescription"] = "";
                        newRow["Department"] = "";
                        newRow["Employee"] = "";
                        newRow["LedgerType"] = "";
                        newRow["Account"] = "";
                        newRow["LedgerCode"] = string.Format("{0} Total:", lastCompany.Trim());
                        newRow["Amount"] = invoice.GetSummaryByCompany(lastCompany.Trim(), lastLedgerCode.Trim());
                        newRow["Check"] = "";

                        locTblInvoices.Rows.Add(newRow);
                    }

                    //create the datatable for invoice errors
                    DataTable locTblErrors = new DataTable();

                    locTblErrors.Columns.Add("Detail");
                    locTblErrors.Columns.Add("Error");

                    //go through each invoice headers errors and add to the invoice errors datatable
                    foreach (Invoice invoice in invoices.Where(x => x.InvoiceLines.Count != 0))
                    {
                        DataRow row;
                        foreach (string error in invoice.Errors)
                        {
                            row = locTblErrors.NewRow();
                            row["Detail"] = string.Format("Invoice {0} Header", invoice._Invoice.Trim());
                            row["Error"] = error.Trim();

                            locTblErrors.Rows.Add(row);
                        }

                        //go through each invoice lines errors and add to the invoice errors datatable
                        foreach (InvoiceLine invoiceLine in invoice.InvoiceLines)
                        {
                            foreach (string error in invoiceLine.Errors)
                            {
                                row = locTblErrors.NewRow();
                                row["Detail"] = string.Format("Invoice {0} Company {1} Property {2} Account {3}", invoice._Invoice.Trim(),
                                                              invoiceLine.Company.Trim(), invoiceLine.Project.ToString().Trim(),
                                                              invoiceLine.Account.ToString().Trim());
                                row["Error"] = error.Trim();

                                locTblErrors.Rows.Add(row);
                            }
                        }
                    }

                    //set the invoice errors datatable to the grid
                    dgvErrors.DataSource = locTblErrors;

                    //set the invoice datatable to the grid
                    dgvInvoices.DataSource = locTblInvoices;

                    dgvErrors.AutoResizeColumns();

                    dgvInvoices.AutoResizeColumns();

                    data.Clear();
                    data.Dispose();
                    data = null;
                }

                file.Dispose();
                file = null;
                invoices.Clear();
                invoices = null;
            }
            catch (IOException err)
            {
                MessageBox.Show("This file is already open. You cannot open a file that is already in open.",
                                "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception err)
            {
                if ((excel != null) && (!excel.IsClosed))
                {
                    excel.Close();
                }
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void dgvInvoices_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (dgvInvoices.Columns[e.ColumnIndex].Name.ToUpper() == "COMPANY")
                {
                    if (e.Value.ToString().Trim() != "")
                    {
                        e.CellStyle.BackColor = Color.FromArgb(200, 200, 255);
                    }
                }
                else if(dgvInvoices.Columns[e.ColumnIndex].Name.ToUpper() == "AMOUNT" & decimal.TryParse(e.Value.ToString().Trim(), out decimal formattedValue))
                {
                    if (e.Value.ToString().Trim() != "")
                    {
                        e.Value = formattedValue.ToString("c2");
                    }
                }
            }
            catch(Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            RefreshForm();
        }

        private void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string createInvoicesErrors = "";

                createInvoicesErrors = mriAPI.CreateInvoices(invoiceEntries);

                //if any errors are returned from MRI's API
                if (createInvoicesErrors != "")
                {
                    //Show the errors
                    MessageBox.Show("There were errors saving the invoices to MRI. Not all of the invoices are being saved.\nErrors:\n" + createInvoicesErrors.Trim(), "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    //go through each invoice imported
                    foreach (HillAPI.MRI.End_Points.Invoice.entry invoice in invoiceEntries)
                    {
                        Dictionary<string, string> whereValues = new Dictionary<string, string>();

                        whereValues.Add("INVOICE", invoice.InvoiceNumber);
                    }
                }
                else
                {
                    MessageBox.Show("The Payroll Invoices were imported to MRI.", "Attention!!!", MessageBoxButtons.OK,
                MessageBoxIcon.Information);
                }
            }
            catch (Exception err)
            {
                //go through each invoice imported
                foreach (HillAPI.MRI.End_Points.Invoice.entry invoice in invoiceEntries)
                {
                    Dictionary<string, string> whereValues = new Dictionary<string, string>();

                    whereValues.Add("INVOICE", invoice.InvoiceNumber);
                }

                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            finally
            {
                RefreshForm();

                Cursor.Current = Cursors.Default;
            }
        }

        private void frmInvcImp_FormClosing(object sender, FormClosingEventArgs e)
        {
            accounts.Clear();
            accounts = null;
            entities.Clear();
            entities = null;
            invoiceEntries.Clear();
            invoiceEntries = null;
            CompanyIDs.Clear();
            CompanyIDs = null;
            mriAPI = null;
        }
        #endregion
    }
}
