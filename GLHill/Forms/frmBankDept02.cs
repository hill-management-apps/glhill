﻿using HelpfulClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GLHill.Forms
{
    public partial class frmBankDept02 : Form
    {
        private frmBankDept _frmBankDept;
        private DataGridViewRow _theRow;

        public frmBankDept02(frmBankDept frmBankDept, DataGridViewRow theRow = null)
        {
            InitializeComponent();

            _frmBankDept = frmBankDept;
            _theRow = theRow;
        }

        private void frmBankDept02_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + " (MS SQL Server: " + Form1.sqlTransactions.GetDatabase().Trim() + ")";

            if (DateTime.TryParse(_theRow.Cells["INTDATE"].Value.ToString(), out DateTime intDate))
            {
                dtpIntendedDepositDate.Value = intDate;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<string, string> updateValues = new Dictionary<string, string>();
                Dictionary<string, string> whereValues = new Dictionary<string, string>();

                whereValues.Add("DEPOSITID", _theRow.Cells["DEPOSITID"].Value.ToString().Trim());

                updateValues = new Dictionary<string, string>();
                updateValues.Add("USERID", HelpfulFunctions.UserID.Trim());
                updateValues.Add("LASTDATE", DateTime.Now.ToString());
                updateValues.Add("INTDATE", dtpIntendedDepositDate.Value.ToString("yyyy-MM-dd"));

                Form1.sqlTransactions.UpdateTableData("HS_BANKDEP", updateValues, whereValues);

                _frmBankDept.RefreshForm(null, null);
                this.Close();
            }
            catch(Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }
    }
}
