﻿
namespace GLHill.Forms
{
    partial class frmBankDept01
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboProperty = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.cboCashType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvReceipts = new System.Windows.Forms.DataGridView();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblRunningTotal = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cboKeyedBy = new System.Windows.Forms.ComboBox();
            this.cboDateRanges = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lblRecepits = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblAmount = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnPrintReceipts = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReceipts)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // cboProperty
            // 
            this.cboProperty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboProperty.Enabled = false;
            this.cboProperty.FormattingEnabled = true;
            this.cboProperty.Location = new System.Drawing.Point(86, 7);
            this.cboProperty.Name = "cboProperty";
            this.cboProperty.Size = new System.Drawing.Size(282, 21);
            this.cboProperty.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Property:";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(1151, 12);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(79, 49);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cboCashType
            // 
            this.cboCashType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCashType.FormattingEnabled = true;
            this.cboCashType.Location = new System.Drawing.Point(86, 34);
            this.cboCashType.Name = "cboCashType";
            this.cboCashType.Size = new System.Drawing.Size(282, 21);
            this.cboCashType.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Cash Type:";
            // 
            // dgvReceipts
            // 
            this.dgvReceipts.AllowUserToAddRows = false;
            this.dgvReceipts.AllowUserToDeleteRows = false;
            this.dgvReceipts.AllowUserToResizeRows = false;
            this.dgvReceipts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvReceipts.Location = new System.Drawing.Point(12, 116);
            this.dgvReceipts.Name = "dgvReceipts";
            this.dgvReceipts.ReadOnly = true;
            this.dgvReceipts.RowHeadersVisible = false;
            this.dgvReceipts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvReceipts.ShowCellErrors = false;
            this.dgvReceipts.ShowEditingIcon = false;
            this.dgvReceipts.ShowRowErrors = false;
            this.dgvReceipts.Size = new System.Drawing.Size(1127, 390);
            this.dgvReceipts.TabIndex = 6;
            this.dgvReceipts.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvReceipts_CellClick);
            this.dgvReceipts.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvReceipts_CellFormatting);
            this.dgvReceipts.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvReceipts_CellMouseDown);
            this.dgvReceipts.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvReceipts_ColumnHeaderMouseClick);
            this.dgvReceipts.SelectionChanged += new System.EventHandler(this.dgvReceipts_SelectionChanged);
            this.dgvReceipts.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgvReceipts_MouseClick);
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpStartDate.Location = new System.Drawing.Point(213, 60);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(98, 20);
            this.dtpStartDate.TabIndex = 3;
            this.dtpStartDate.ValueChanged += new System.EventHandler(this.dateChange);
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEndDate.Location = new System.Drawing.Point(343, 60);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(98, 20);
            this.dtpEndDate.TabIndex = 4;
            this.dtpEndDate.ValueChanged += new System.EventHandler(this.dateChange);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(317, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "To";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Date Range:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(374, 2);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(149, 55);
            this.label5.TabIndex = 18;
            this.label5.Text = "Total:";
            // 
            // lblRunningTotal
            // 
            this.lblRunningTotal.AutoSize = true;
            this.lblRunningTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRunningTotal.Location = new System.Drawing.Point(529, 3);
            this.lblRunningTotal.Name = "lblRunningTotal";
            this.lblRunningTotal.Size = new System.Drawing.Size(57, 55);
            this.lblRunningTotal.TabIndex = 19;
            this.lblRunningTotal.Text = "X";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 90);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Keyed By:";
            // 
            // cboKeyedBy
            // 
            this.cboKeyedBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboKeyedBy.FormattingEnabled = true;
            this.cboKeyedBy.Items.AddRange(new object[] {
            "All",
            "EP",
            "Field"});
            this.cboKeyedBy.Location = new System.Drawing.Point(86, 87);
            this.cboKeyedBy.Name = "cboKeyedBy";
            this.cboKeyedBy.Size = new System.Drawing.Size(282, 21);
            this.cboKeyedBy.TabIndex = 5;
            // 
            // cboDateRanges
            // 
            this.cboDateRanges.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDateRanges.FormattingEnabled = true;
            this.cboDateRanges.Items.AddRange(new object[] {
            "Custom",
            "This Week",
            "Last Week",
            "This Month",
            "Last Month",
            "This Year"});
            this.cboDateRanges.Location = new System.Drawing.Point(86, 60);
            this.cboDateRanges.Name = "cboDateRanges";
            this.cboDateRanges.Size = new System.Drawing.Size(121, 21);
            this.cboDateRanges.TabIndex = 2;
            this.cboDateRanges.SelectedIndexChanged += new System.EventHandler(this.cboDateRanges_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 514);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "Receipts:";
            // 
            // lblRecepits
            // 
            this.lblRecepits.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblRecepits.AutoSize = true;
            this.lblRecepits.Location = new System.Drawing.Point(66, 514);
            this.lblRecepits.Name = "lblRecepits";
            this.lblRecepits.Size = new System.Drawing.Size(14, 13);
            this.lblRecepits.TabIndex = 23;
            this.lblRecepits.Text = "X";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(130, 514);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Amount:";
            // 
            // lblAmount
            // 
            this.lblAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblAmount.AutoSize = true;
            this.lblAmount.Location = new System.Drawing.Point(182, 514);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(14, 13);
            this.lblAmount.TabIndex = 25;
            this.lblAmount.Text = "X";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(255)))));
            this.panel1.Location = new System.Drawing.Point(486, 87);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(35, 20);
            this.panel1.TabIndex = 26;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(524, 91);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(10, 13);
            this.label9.TabIndex = 27;
            this.label9.Text = "-";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(536, 91);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(147, 13);
            this.label10.TabIndex = 28;
            this.label10.Text = "Check number is manually set";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.btnPrintReceipts);
            this.panel2.Location = new System.Drawing.Point(1145, 116);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(85, 390);
            this.panel2.TabIndex = 29;
            // 
            // btnPrintReceipts
            // 
            this.btnPrintReceipts.Location = new System.Drawing.Point(3, 3);
            this.btnPrintReceipts.Name = "btnPrintReceipts";
            this.btnPrintReceipts.Size = new System.Drawing.Size(79, 49);
            this.btnPrintReceipts.TabIndex = 0;
            this.btnPrintReceipts.Text = "Print Receipts";
            this.btnPrintReceipts.UseVisualStyleBackColor = true;
            this.btnPrintReceipts.Click += new System.EventHandler(this.btnPrintReceipts_Click);
            // 
            // frmBankDept01
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1242, 536);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblAmount);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblRecepits);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cboDateRanges);
            this.Controls.Add(this.cboKeyedBy);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblRunningTotal);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtpEndDate);
            this.Controls.Add(this.dtpStartDate);
            this.Controls.Add(this.dgvReceipts);
            this.Controls.Add(this.cboCashType);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.cboProperty);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "frmBankDept01";
            this.Text = "SiteLink Bank Deposits";
            this.Load += new System.EventHandler(this.frmBankDept01_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReceipts)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cboProperty;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cboCashType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgvReceipts;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblRunningTotal;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cboKeyedBy;
        private System.Windows.Forms.ComboBox cboDateRanges;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblRecepits;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnPrintReceipts;
    }
}