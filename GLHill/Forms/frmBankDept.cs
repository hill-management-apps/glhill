﻿using HelpfulClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace GLHill.Forms
{
    public partial class frmBankDept : Form
    {
        #region "Fields"
        private const string printOut = @"
                                    <html>
                                    <head>
                                        <style>
                                            .alternatingLines {
                                                border-collapse: collapse;
                                                font-size: 12px;
                                                width: 100%;
                                            }

                                                .alternatingLines td {
                                                    border-bottom: .5px dotted rgba(60, 70, 29, 0.90) !important;
                                                    border-right: none !important;
                                                    border-left: none !important;
                                                    border-top: none !important;
                                                    padding: 0px;
                                                    width: 0px !important;
                                                }

                                                .alternatingLines .leftAlignedColumns {
                                                    text-align: left;
                                                }

                                            .bankAccount {
                                                font-family: IDAutomationMICR;
                                                transform: rotate(90deg);
                                                width: 100%;
                                                left: 0px;
                                                top: -115px;
                                                position: relative;
                                                font-size: 15px;
                                                letter-spacing: 2px;
                                            }

                                            .checkLines {
                                                text-align: center;
                                                font: 12px arial;
                                                color: rgba(60, 70, 29, 0.90);
                                                border-spacing: 0px;
                                                width: 100%;
                                                border-collapse: collapse;
                                            }

                                                .checkLines td {
                                                    border: solid .5px rgba(60, 70, 29, 0.90);
                                                    padding: 0px;
                                                    text-align: right;
                                                    width: 10px;
                                                    height: 15px;
                                                }

                                                .checkLinesHeader{
                                                  border: solid .5px rgba(60, 70, 29, 0.90);
                                                    padding: 0px;
                                                    text-align: right;
                                                    width: 10px;
                                                    height: 15px;
                                                    padding-left: 5px!important;
                                                    padding-right: 5px!important;
                                                }

                                            .dateHeader {
                                                color: rgba(60, 70, 29, 0.90);
                                                text-align: right;
                                            }

                                            .dateArea {
                                                border-bottom-color: rgba(60, 70, 29, 0.90);
                                                border-bottom: solid .5px rgba(60, 70, 29, 0.90);
                                            }

                                            .dateMessage {
                                                color: rgba(60, 70, 29, 0.90);
                                                text-align: left;
                                                font-size: 5px;
                                                vertical-align: top;
                                            }

                                            .depositTicket {
                                                text-align: center;
                                                font: bold 8px arial;
                                            }

                                            .dividerLine {
                                                border-right: dashed .5px;
                                                width:10px;
                                            }

                                            .innerElementFormatting {
                                                text-align: center;
                                                font: bold 12px arial;
                                                width: 350px;
                                                justify-items: center;
                                                border-spacing: 0px;
                                                word-spacing: 0px;
                                            }

                                            .routingNumber {
                                                font-family: IDAutomationMICR;
                                                transform: rotate(90deg);
                                                width: 100%;
                                                left: 0px;
                                                top: 30px;
                                                position: relative;
                                                font-size: 15px;
                                                letter-spacing: 2px;
                                            }

                                            .totalLine {
                                                font-size: 12px !important;
                                            }

                                            .totalMessage {
                                                font-size: 8px !important;
                                                text-align:left;
                                            }

                                            .totalNumber {
                                                width: 10px;
                                                height: 10px;
                                                font-size: 8px;
                                                text-align: center;
                                                transform: rotate(90deg);
                                                display: inline-block;
                                                margin: 0 auto;
                                                background-color: #ffffff;
                                            }

                                            .totalOuterNumber {
                                                position: relative;
                                                left: -7px;
                                                width: 30px;
                                                height: 30px;
                                                width: 100%;
                                                text-align: center;
                                                background-color: rgb(163, 163, 163);
                                            }
                                        </style>
                                    </head>
                                    <body>
                                        <table>
                                            <tr>
                                                <td>
                                                    <table class='depositTicket innerElementFormatting'>
                                                        <tr>
                                                            <td style='width:43%'>
                                                            </td>
                                                            <td></td>
                                                            <td style='width:7%;'>
                                                            </td>
                                                            <td style='color: rgba(60, 70, 29, 0.90); width:50%;'>
                                                                DEPOSIT TICKET
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan='3'>
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                {0}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td style='font-size: 6px;'>
                                                                {1}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan='3'>
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan='3'></td>
                                                            <td>
                                                                <img src='{2}' alt='Bank Logo Here' width='75px' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan='2'>Deposit #: {95}</td>
                                                            <td class='dateHeader'>DATE</td>
                                                            <td class='dateArea'>{3}</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan='2'>Property #: {96}</td>
                                                            <td class='dateMessage' colspan='2'>DEPOSITS MAY NOT BE AVALABLE FOR IMMEDIATE WITHDRAWAL</td>
                                                        </tr>
                                                        <tr>
                                                            <td style='text-align: left !important;' rowspan='3'>
                                                                <div class='bankAccount'>A{5}]</div>
                                                                <div class='routingNumber'>{4}\</div>
                                                                </p>
                                                            </td>
                                                            <td style='vertical-align:bottom;'>
                                                                <div style='transform: rotate(90deg); position: relative; left: -7px;'>$</div>
                                                                <div>
                                                                    <table class='totalOuterNumber'>
                                                                        <tr>
                                                                            <td>
                                                                                <div class='totalNumber'>{6}</div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div class='totalNumber'>{7}</div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div class='totalNumber'>{8}</div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div class='totalNumber'>{9}</div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div class='totalNumber'>{10}</div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div class='totalNumber'>{11}</div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div class='totalNumber'>{12}</div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div class='totalNumber'>{13}</div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div style='background-color:rgb(163, 163, 163);' class='totalNumber'>.</div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div class='totalNumber'>{14}</div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div class='totalNumber'>{15}</div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                            <td colspan='2'>
                                                                <table class='checkLines'>
                                                                    <tr>
                                                                        <td style='border:none; width:50%;'>
                                                                        </td>
                                                                        <td class='checkLinesHeader' style='text-align:center; width:30%;'>DOLLARS</td>
                                                                        <td class='checkLinesHeader' style='text-align:center; width:20%;'>CENTS</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class='checkLinesHeader'>
                                                                            CASH
                                                                        </td>
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class='checkLinesHeader'>
                                                                            COIN
                                                                        </td>
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class='checkLinesHeader'>
                                                                            CHECKS
                                                                        </td>
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table class='alternatingLines'>
                                                                                <tr>
                                                                                    <td width='10%' class='leftAlignedColumns'>1</td>
                                                                                    <td width='90%'>{16}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>2</td>
                                                                                    <td>{17}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>3</td>
                                                                                    <td>{18}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>4</td>
                                                                                    <td>{19}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>5</td>
                                                                                    <td>{20}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>6</td>
                                                                                    <td>{21}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>7</td>
                                                                                    <td>{22}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>8</td>
                                                                                    <td>{23}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>9</td>
                                                                                    <td>{24}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>10</td>
                                                                                    <td>{25}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>11</td>
                                                                                    <td>{26}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>12</td>
                                                                                    <td>{27}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>13</td>
                                                                                    <td>{28}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>14</td>
                                                                                    <td>{29}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>15</td>
                                                                                    <td>{30}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>16</td>
                                                                                    <td>{31}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>17</td>
                                                                                    <td>{32}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>18</td>
                                                                                    <td>{33}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>19</td>
                                                                                    <td>{34}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>20</td>
                                                                                    <td>{35}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>21</td>
                                                                                    <td>{36}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>22</td>
                                                                                    <td>{37}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>23</td>
                                                                                    <td>{38}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>24</td>
                                                                                    <td>{39}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>25</td>
                                                                                    <td>{40}</td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td>
                                                                            <table class='alternatingLines'>
                                                                                <tr>
                                                                                    <td>{41}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{42}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{43}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{44}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{45}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{46}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{47}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{48}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{49}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{50}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{51}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{52}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{53}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{54}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{55}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{56}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{57}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{58}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{59}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{60}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{61}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{62}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{63}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{64}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{65}</td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td>
                                                                            <table class='alternatingLines'>
                                                                                <tr>
                                                                                    <td>{66}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{67}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{68}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{69}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{70}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{71}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{72}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{73}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{74}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{75}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{76}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{77}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{78}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{79}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{80}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{81}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{82}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{83}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{84}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{85}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{86}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{87}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{88}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{89}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{90}</td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class='checkLines totalMessage'>
                                                                        <td>
                                                                            TOTAL FROM OTHER SIDE
                                                                            OR ATTACHED LIST
                                                                        </td>
                                                                        <td>
                                                                            {91}
                                                                        </td>
                                                                        <td>
                                                                            {92}
                                                                        </td>
                                                                    </tr>
                                                                    <tr class='checkLines totalLine'>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                            {93}
                                                                        </td>
                                                                        <td>
                                                                            {94}
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td class='checkLines totalMessage' colspan='3'>
                                                                <div style='border: solid .5px rgba(60, 70, 29, 0.90);'>
                                                                    <p>
                                                                        Checks and other items are received for deposit
                                                                        subject to the provisions of the Uniform Commercial
                                                                        Code or any applicable collection agreement.
                                                                    </p>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan='3'>
                                                                *** Bank Copy ***
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class='dividerLine'>
                                                </td>
                                                <td>
                                                    <table class='depositTicket innerElementFormatting'>
                                                        <tr>
                                                            <td style='width:43%'>
                                                            </td>
                                                            <td></td>
                                                            <td style='width:7%;'>
                                                            </td>
                                                            <td style='color: rgba(60, 70, 29, 0.90); width:50%;'>
                                                                DEPOSIT TICKET
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan='3'>
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                {0}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td style='font-size: 6px;'>
                                                                {1}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan='3'>
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan='3'></td>
                                                            <td>
                                                                <img src='{2}' alt='Bank Logo Here' width='75px' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan='2'>Deposit #: {95}</td>
                                                            <td class='dateHeader'>DATE</td>
                                                            <td class='dateArea'>{3}</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan='2'>Property #: {96}</td>
                                                            <td class='dateMessage' colspan='2'>DEPOSITS MAY NOT BE AVALABLE FOR IMMEDIATE WITHDRAWAL</td>
                                                        </tr>
                                                        <tr>
                                                            <td style='text-align: left !important;' rowspan='3'>
                                                                <div class='bankAccount'>A{5}]</div>
                                                                <div class='routingNumber'>{4}\</div>
                                                                </p>
                                                            </td>
                                                            <td style='vertical-align:bottom;'>
                                                                <div style='transform: rotate(90deg); position: relative; left: -7px;'>$</div>
                                                                <div>
                                                                    <table class='totalOuterNumber'>
                                                                        <tr>
                                                                            <td>
                                                                                <div class='totalNumber'>{6}</div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div class='totalNumber'>{7}</div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div class='totalNumber'>{8}</div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div class='totalNumber'>{9}</div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div class='totalNumber'>{10}</div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div class='totalNumber'>{11}</div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div class='totalNumber'>{12}</div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div class='totalNumber'>{13}</div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div style='background-color:rgb(163, 163, 163);' class='totalNumber'>.</div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div class='totalNumber'>{14}</div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div class='totalNumber'>{15}</div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                            <td colspan='2'>
                                                                <table class='checkLines'>
                                                                    <tr>
                                                                        <td style='border:none; width:50%;'>
                                                                        </td>
                                                                        <td class='checkLinesHeader' style='text-align:center; width:30%;'>DOLLARS</td>
                                                                        <td class='checkLinesHeader' style='text-align:center; width:20%;'>CENTS</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class='checkLinesHeader'>
                                                                            CASH
                                                                        </td>
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class='checkLinesHeader'>
                                                                            COIN
                                                                        </td>
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class='checkLinesHeader'>
                                                                            CHECKS
                                                                        </td>
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table class='alternatingLines'>
                                                                                <tr>
                                                                                    <td width='10%' class='leftAlignedColumns'>1</td>
                                                                                    <td width='90%'>{16}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>2</td>
                                                                                    <td>{17}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>3</td>
                                                                                    <td>{18}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>4</td>
                                                                                    <td>{19}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>5</td>
                                                                                    <td>{20}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>6</td>
                                                                                    <td>{21}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>7</td>
                                                                                    <td>{22}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>8</td>
                                                                                    <td>{23}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>9</td>
                                                                                    <td>{24}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>10</td>
                                                                                    <td>{25}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>11</td>
                                                                                    <td>{26}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>12</td>
                                                                                    <td>{27}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>13</td>
                                                                                    <td>{28}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>14</td>
                                                                                    <td>{29}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>15</td>
                                                                                    <td>{30}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>16</td>
                                                                                    <td>{31}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>17</td>
                                                                                    <td>{32}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>18</td>
                                                                                    <td>{33}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>19</td>
                                                                                    <td>{34}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>20</td>
                                                                                    <td>{35}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>21</td>
                                                                                    <td>{36}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>22</td>
                                                                                    <td>{37}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>23</td>
                                                                                    <td>{38}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>24</td>
                                                                                    <td>{39}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class='leftAlignedColumns'>25</td>
                                                                                    <td>{40}</td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td>
                                                                            <table class='alternatingLines'>
                                                                                <tr>
                                                                                    <td>{41}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{42}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{43}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{44}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{45}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{46}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{47}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{48}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{49}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{50}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{51}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{52}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{53}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{54}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{55}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{56}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{57}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{58}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{59}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{60}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{61}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{62}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{63}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{64}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{65}</td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td>
                                                                            <table class='alternatingLines'>
                                                                                <tr>
                                                                                    <td>{66}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{67}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{68}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{69}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{70}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{71}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{72}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{73}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{74}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{75}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{76}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{77}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{78}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{79}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{80}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{81}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{82}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{83}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{84}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{85}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{86}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{87}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{88}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{89}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{90}</td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class='checkLines totalMessage'>
                                                                        <td>
                                                                            TOTAL FROM OTHER SIDE
                                                                            OR ATTACHED LIST
                                                                        </td>
                                                                        <td>
                                                                            {91}
                                                                        </td>
                                                                        <td>
                                                                            {92}
                                                                        </td>
                                                                    </tr>
                                                                    <tr class='checkLines totalLine'>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                            {93}
                                                                        </td>
                                                                        <td>
                                                                            {94}
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td class='checkLines totalMessage' colspan='3'>
                                                                <div style='border: solid .5px rgba(60, 70, 29, 0.90);'>
                                                                    <p>
                                                                        Checks and other items are received for deposit
                                                                        subject to the provisions of the Uniform Commercial
                                                                        Code or any applicable collection agreement.
                                                                    </p>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan='3'>
                                                                *** Customer Copy ***
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class='dividerLine'>
                                                </td>
                                            </tr>
                                        </table>
                                    </body>
                                    </html>";
        private bool IsDepositScreen = false;
        #endregion

        #region "Constructors"
        public frmBankDept(bool depositScreen = false)
        {
            InitializeComponent();

            IsDepositScreen = depositScreen;
        }
        #endregion

        #region "Methods"
        /// <summary>
        /// Creates the data in the Drop Down's on the form.
        /// </summary>
        private void CreateDropdowns()
        {
            try
            {
                cboProperty.Items.Clear();

                string strSQL = "";
                DataTable locTbl = new DataTable();

                strSQL = @"SELECT ENTITYID, (RTRIM(LTRIM(ENTITYID)) + '-' + NAME) as NAME FROM ENTITY
                           WHERE ENTITYID IN (SELECT HSVAL FROM HS_CODEVAL WHERE HSTABLE = 'MINISTORAGE' AND HSCOL = 'ENTITYID'
                           AND HSCOL <> '@@@@@')
                           ORDER BY NAME ASC";

                locTbl = Form1.sqlTransactions.ExecuteQuery(strSQL);

                //add the ALL item
                DataRow dr = locTbl.NewRow();
                dr["ENTITYID"] = "";
                dr["NAME"] = "ALL";
                locTbl.Rows.InsertAt(dr, 0);

                cboProperty.DataSource = locTbl;
                cboProperty.DisplayMember = "NAME";
                cboProperty.ValueMember = "ENTITYID";
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Deletes a deposit and all its receipts.
        /// </summary>
        /// <param name="depositId">The deposit id.</param>
        private void Delete(string depositId)
        {
            try
            {
                //display message that asks the user if the delete the selected deposit slip
                DialogResult dialog = MessageBox.Show("Are you sure you want to delete this deposit slip?", "Attention!!!", MessageBoxButtons.YesNo,
                                                      MessageBoxIcon.Question);

                //if the user selects 'Yes'
                if (dialog == DialogResult.Yes)
                {
                    Dictionary<string, string> whereValues = new Dictionary<string, string>
                    {
                        { "DEPOSITID", depositId.Trim() }
                    };

                    //deletes the receipts and the deposit slips
                    Form1.sqlTransactions.DeleteTableData("HS_BANKDEPLINE", whereValues);
                    Form1.sqlTransactions.DeleteTableData("HS_BANKDEP", whereValues);
                }

                //refresh the data on the screen
                RefreshForm(this, null);
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Edits the selected row based on the data provided.
        /// </summary>
        /// <param name="row">The deposits data.</param>
        private void Edit(DataGridViewRow row)
        {
            try
            {
                frmBankDept01 frmBankDep = new frmBankDept01(frmBankDept01.ACTION.UPDATE, this, row);
                frmBankDep.ShowDialog(this);
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Formats the grid headers. It changes the text of the headeer to be more legiable. It hides columns that do not need to be seen.
        /// </summary>
        private void FormatHeaders()
        {
            try
            {
                dgvDeposits.Columns["DEPOSITID"].HeaderText = "Deposit ID";
                dgvDeposits.Columns["CASHDESC"].HeaderText = "Cash Type";
                dgvDeposits.Columns["BANKNAME"].HeaderText = "Bank Name";
                dgvDeposits.Columns["ENTITYID"].HeaderText = "Entity ID";
                dgvDeposits.Columns["NAME"].HeaderText = "Entity Name";
                dgvDeposits.Columns["AMOUNT"].HeaderText = "Amount";
                dgvDeposits.Columns["INTDATE"].HeaderText = "Intended Deposit Date";
                dgvDeposits.Columns["ACTDATE"].HeaderText = "Business Date";
                dgvDeposits.Columns["BANKREF"].HeaderText = "Bank Reference/Receipt";

                dgvDeposits.Columns["CASHTYPE"].Visible = false;
                dgvDeposits.Columns["BANKID"].Visible = false;
                dgvDeposits.Columns["ENTITYID"].Visible = false;
                if (IsDepositScreen)
                {
                    dgvDeposits.Columns["actBtn"].Visible = false;
                }
                else
                {
                    dgvDeposits.Columns["actBtn"].Visible = false;
                    dgvDeposits.Columns["delBtn"].Visible = false;
                    dgvDeposits.Columns["edtBtn"].Visible = false;
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Populate the data on the form. 
        /// </summary>
        /// <param name="propId">The property id to get the Bank Deposits for.</param>
        /// <param name="startDate">The beginning date for which to get the deposits for.</param>
        /// <param name="endDate">The last date for which to get the deposits for.</param>
        /// <param name="depositId">The deposit id.</param>
        private void PopulateData(string propId, DateTime startDate, DateTime endDate, string depositId)
        {
            try
            {
                //clear the datagridview to prevent duplication
                dgvDeposits.DataSource = null;

                string strSQL = "";
                DataTable locTbl = new DataTable();

                //gets the bank deposits and related data
                /*
                 * This code will reset the totals on HS_BANKDEP.  SHould really be a trigger
                 * begin transaction
                update HS_BANKDEP  SET TOTAL = 
                TOtalDep  from 
                ( 
                select d.depositid, sum(r.RECEIPTAMT) TotalDep 

                from HS_BANKDEP D join hs_bankdepline l on l.depositid = d.DEPOSITID
                join HS_SLRECEIPTS as r on l.RECEIPTNO = r.IRECEIPTNU and CAST(d.ENTITYID as NUMERIC) = CAST(r.SITEID as NUMERIC)-- and (pmt_check > 0 or pmt_cash > 0)
                where (pmt_cash > 0 OR PMT_cHECK>0)
                group by d.DEPOSITID) Sd
                WHERE  Sd.depositID = hs_bankdeP.depositid


                rollback
                commit

                SELECT D.DEPOSITID, ENTITYID FROM HS_BANKDEPLINE L JOIN HS_BANKDEP D ON L.DEPOSITID = D.DEPOSITID WHERE OVRAMT  is not null
                GROUP BY D.DEPOSITID, ENTITYID
                 */


                strSQL = $@"With t1 as (SELECT {(IsDepositScreen ? "'+' as actBtn," : "'+' as delBtn, '+' as edtBtn, '+' as actBtn,")}
                            A.DEPOSITID, A.CASHTYPE, E.CASHDESC, F.BANKID, G.BANKNAME, A.ENTITYID, D.[NAME],
                            Total as AMOUNT, INTDATE, ACTDATE, BANKREF
                            FROM HS_BANKDEP as A
                            LEFT outer JOIN ENTITY AS D ON A.ENTITYID = D.ENTITYID 
                            LEFT outer JOIN CTYP AS E ON A.CASHTYPE = E.CASHTYPE
                            LEFT outer JOIN BMAP AS F ON A.CASHTYPE = F.CASHTYPE AND A.ENTITYID = F.ENTITYID
                            LEFT outer JOIN BANK AS G ON F.BANKID = G.BANKID
                            {(IsDepositScreen ? "" : (depositId.Trim() != "" ? "" :
                            $@" WHERE CAST({(radIntDate.Checked ? "INTDATE" : "ACTDATE")} as DATE) BETWEEN '{startDate.ToString("yyyy-MM-dd 00:00:00")}' AND '{endDate.ToString("yyyy-MM-dd 23:59:59")}'"))})
                            SELECT {(IsDepositScreen ? "'+' as actBtn," : "'+' as delBtn, '+' as edtBtn, '+' as actBtn,")}
                            DEPOSITID, CASHTYPE, CASHDESC, BANKID, BANKNAME, ENTITYID, [NAME],
                            SUM(AMOUNT) as AMOUNT, INTDATE, ACTDATE, BANKREF FROM t1
                            WHERE {(IsDepositScreen ? "ACTDATE IS NULL" : "1=1")}";

                //if a property is specified limit the grid to the property, otherwise show deposits for all properties
                if (propId.Trim() != "")
                {
                    strSQL += $" AND ENTITYID = '{propId.Trim()}'";
                }

                if(depositId.Trim() != "")
                {
                    strSQL += $" AND DEPOSITID = {depositId}";
                }

                strSQL += @" GROUP BY INTDATE, BANKREF, ACTDATE, DEPOSITID, CASHTYPE, CASHDESC, BANKID, BANKNAME, ENTITYID, [NAME]";
                strSQL += @" Order by INTDATE, ACTDATE";


                locTbl = Form1.sqlTransactions.ExecuteQuery(strSQL);

                if (IsDepositScreen)
                {
                    //get controls to hide
                    List<Control> ctl = (from Control c in this.Controls
                                         where c.GetType().Name.Trim() == "ComboBox" | c.GetType().Name.Trim() == "Label" | c.GetType().Name.Trim() == "DateTimePicker"
                                         select c).ToList();

                    //hide the controls
                    foreach (Control c in ctl)
                    {
                        c.Visible = false;
                    }

                    btnRefresh.Visible = false;
                }

                //if at least 1 deposit if found
                if (locTbl.Rows.Count > 0)
                {
                    //set the data to the datagridview
                    dgvDeposits.DataSource = locTbl;

                    if (IsDepositScreen)
                    {
                        //button that allows the user to set the date of deposit and the bank refence/receipt number
                        DataGridViewButtonColumn actBtn = new DataGridViewButtonColumn
                        {
                            DataPropertyName = "actBtn",
                            HeaderText = "Business Date",
                            Text = "+",
                            MinimumWidth = 100,
                            Frozen = true
                        };
                        dgvDeposits.Columns.Insert(0, actBtn);

                    }
                    else
                    {
                        //button that allows the user to set the date of deposit and the bank refence/receipt number
                        DataGridViewButtonColumn actBtn = new DataGridViewButtonColumn
                        {
                            DataPropertyName = "actBtn",
                            HeaderText = "Business Date",
                            Text = "+",
                            MinimumWidth = 100,
                            Frozen = true
                        };
                        dgvDeposits.Columns.Insert(0, actBtn);

                        //button that allows the user to edit the record
                        DataGridViewButtonColumn edtBtn = new DataGridViewButtonColumn
                        {
                            DataPropertyName = "edtBtn",
                            HeaderText = "Edit",
                            Text = "+",
                            MinimumWidth = 50,
                            Frozen = true
                        };
                        dgvDeposits.Columns.Insert(0, edtBtn);

                        //button that allows the user to delete the record
                        DataGridViewButtonColumn delBtn = new DataGridViewButtonColumn
                        {
                            DataPropertyName = "delBtn",
                            HeaderText = "Delete",
                            Text = "+",
                            MinimumWidth = 50,
                            Frozen = true
                        };
                        dgvDeposits.Columns.Insert(0, delBtn);
                    }

                    FormatHeaders();

                    dgvDeposits.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Calls the form that allows the user to set the Actual Deposit Date and the Bank Reference/Receipt.
        /// </summary>
        /// <param name="theRow">The record that the Actual Deposit Date and the Bank Reference/Receipt.</param>
        private void SetActualDepositDate(DataGridViewRow theRow)
        {
            frmBankDept03 frmBankDept03 = new frmBankDept03(this, theRow);
            frmBankDept03.ShowDialog();
            this.Wait();
            RefreshForm(null, null);
            this.Useable();
        }

        /// <summary>
        /// Calls the form that allows the user to set the Intended Deposit Date.
        /// </summary>
        /// <param name="theRow">The record that the Intended Deposit Date.</param>
        private void SetIntendDepositDate(DataGridViewRow theRow)
        {
            frmBankDept02 frmBankDept02 = new frmBankDept02(this, theRow);
            frmBankDept02.ShowDialog();
            this.Wait();
            RefreshForm(null, null);
            this.Useable();
        }
        #endregion

        #region "Event Handlers"
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                //if there is a property selected in the drop down
                if (cboProperty.Text.Trim().ToUpper() != "ALL")
                {
                    //display the form to add a new Bank Deposit
                    frmBankDept01 frmBankDep = new frmBankDept01(frmBankDept01.ACTION.INSERT, this);
                    frmBankDep.ShowDialog(this);
                    RefreshForm(null, null);
                }
                else
                {
                    //display a message to the user that they must select a property before adding a deposit
                    MessageBox.Show("Please select a property before adding a deposit.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                this.Wait();
                //prepares the values to match with fields on the print
                string[] values = new string[97];
                DataTable locTbl = new DataTable();
                //string strSQL = $@"SELECT E.BANKID, E.BANKNAME, F.[NAME], (CASE WHEN OVRCKORCHGNO IS NOT NULL THEN OVRCKORCHGNO ELSE CKORCHGNO END) as CKORCHGNO, A.SITEID, B.DEPOSITID, 
                //                   coalesce(notDeposited -1, sum(OVRAMT)/count(*), SUM(RECEIPTAMT)) as netamt FROM HS_SLRECEIPTS AS A
                //                   LEFT JOIN HS_BANKDEPLINE as B ON A.IRECEIPTNU = B.RECEIPTNO
                //                   LEFT JOIN HS_BANKDEP as C ON B.DEPOSITID = C.DEPOSITID
                //                   LEFT JOIN BMAP as D ON C.ENTITYID = D.ENTITYID AND C.CASHTYPE = D.CASHTYPE
                //                   LEFT JOIN BANK AS E ON D.BANKID = E.BANKID
                //                   LEFT JOIN ENTITY AS F ON (LEFT(REPLICATE('0', 1)  + CAST(A.SITEID as NVARCHAR), 3)) = F.ENTITYID
                //                   WHERE SCATEGORY IN ('Check', 'MoneyOrder', 'Cash') And NotDeposited is NULL AND A.SITEID = '{dgvDeposits.SelectedRows[0].Cells["ENTITYID"].Value.ToString().Trim()}' AND IRECEIPTNU IN (SELECT RECEIPTNO FROM HS_BANKDEPLINE AS A
                //                   LEFT JOIN HS_BANKDEP AS B ON A.DEPOSITID = B.DEPOSITID)
                //                   AND B.DEPOSITID = '{dgvDeposits.SelectedRows[0].Cells["DEPOSITID"].Value.ToString().Trim()}'
                //                   GROUP BY E.BANKID, E.BANKNAME, F.[NAME], CKORCHGNO, OVRCKORCHGNO, A.SITEID, B.DEPOSITID, notDeposited";

                string strSQL = $@"SELECT E.BANKID, E.BANKNAME, F.[NAME], (CASE WHEN OVRCKORCHGNO IS NOT NULL THEN OVRCKORCHGNO ELSE CKORCHGNO END) as CKORCHGNO, A.SITEID, B.DEPOSITID, 
                                   SUM(coalesce(OVRAMT, OVRAMT, RECEIPTAMT)) as netamt FROM HS_SLRECEIPTS AS A
                                   LEFT JOIN HS_BANKDEPLINE as B ON A.IRECEIPTNU = B.RECEIPTNO
                                   LEFT JOIN HS_BANKDEP as C ON B.DEPOSITID = C.DEPOSITID
                                   LEFT JOIN BMAP as D ON C.ENTITYID = D.ENTITYID AND C.CASHTYPE = D.CASHTYPE
                                   LEFT JOIN BANK AS E ON D.BANKID = E.BANKID
                                   LEFT JOIN ENTITY AS F ON (LEFT(REPLICATE('0', 1)  + CAST(A.SITEID as NVARCHAR), 3)) = F.ENTITYID
                                   WHERE SCATEGORY IN ('Check', 'MoneyOrder', 'Cash') And NotDeposited is NULL AND A.SITEID = '{dgvDeposits.SelectedRows[0].Cells["ENTITYID"].Value.ToString().Trim()}' AND IRECEIPTNU IN (SELECT RECEIPTNO FROM HS_BANKDEPLINE AS A
                                   LEFT JOIN HS_BANKDEP AS B ON A.DEPOSITID = B.DEPOSITID)
                                   AND B.DEPOSITID = '{dgvDeposits.SelectedRows[0].Cells["DEPOSITID"].Value.ToString().Trim()}'
                                   GROUP BY E.BANKID, E.BANKNAME, F.[NAME], CKORCHGNO, OVRCKORCHGNO, A.SITEID, B.DEPOSITID, notDeposited";

                locTbl = Form1.sqlTransactions.ExecuteQuery(strSQL);

                //match the header information
                if (locTbl.Rows.Count > 0)
                {
                    //match the deposit date
                    if (DateTime.TryParse(dgvDeposits.SelectedRows[0].Cells["INTDATE"].Value.ToString().Trim(), out DateTime depositDate))
                    {
                        values[3] = depositDate.ToString("MM-dd-yyyy");
                    }

                    int dollars = 0;
                    decimal cents = 0;
                    string total;

                    //match the check values, numbers, and total dollars and cents
                    for (int i = 16; i < (locTbl.Rows.Count + 16) & i <= 40; i++)
                    {
                        values[i] = locTbl.Rows[i - 16]["CKORCHGNO"].ToString().Trim();
                        values[i + 25] = locTbl.Rows[i - 16]["NETAMT"].ToString().Trim().Split('.')[0];
                        values[i + 50] = locTbl.Rows[i - 16]["NETAMT"].ToString().Trim().Split('.')[1];
                        dollars += int.Parse(values[i + 25]);
                        cents += int.Parse(values[i + 50]) * .01M;
                    }

                    //total the dollars and cents
                    total = (dollars + cents).ToString("c2");

                    total = total.Replace("$", "");
                    total = total.Replace(",", "");

                    //match the dollars and cents
                    values[93] = total.ToString().Split('.')[0];

                    if (total.ToString().Split('.').Length > 1)
                    {
                        values[94] = total.ToString().Split('.')[1];
                    }
                    else
                    {
                        values[94] = "00";
                    }

                    //split up the money into parts
                    char[] totalParts;

                    if (total.Contains("."))
                    {
                        totalParts = total.Replace(".", "").ToArray();
                    }
                    else
                    {
                        totalParts = total.Replace(".", "").PadRight(total.Length + 2, '0').ToArray();
                    }

                    //match the position of the digit with the 90 degree total
                    switch (totalParts.Length)
                    {
                        case 10:
                            values[15] = totalParts[9].ToString().Trim();
                            values[14] = totalParts[8].ToString().Trim();
                            values[13] = totalParts[7].ToString().Trim();
                            values[12] = totalParts[6].ToString().Trim();
                            values[11] = totalParts[5].ToString().Trim();
                            values[10] = totalParts[4].ToString().Trim();
                            values[9] = totalParts[3].ToString().Trim();
                            values[8] = totalParts[2].ToString().Trim();
                            values[7] = totalParts[1].ToString().Trim();
                            values[6] = totalParts[0].ToString().Trim();
                            break;
                        case 9:
                            values[15] = totalParts[8].ToString().Trim();
                            values[14] = totalParts[7].ToString().Trim();
                            values[13] = totalParts[6].ToString().Trim();
                            values[12] = totalParts[5].ToString().Trim();
                            values[11] = totalParts[4].ToString().Trim();
                            values[10] = totalParts[3].ToString().Trim();
                            values[9] = totalParts[2].ToString().Trim();
                            values[8] = totalParts[1].ToString().Trim();
                            values[7] = totalParts[0].ToString().Trim();
                            break;
                        case 8:
                            values[15] = totalParts[7].ToString().Trim();
                            values[14] = totalParts[6].ToString().Trim();
                            values[13] = totalParts[5].ToString().Trim();
                            values[12] = totalParts[4].ToString().Trim();
                            values[11] = totalParts[3].ToString().Trim();
                            values[10] = totalParts[2].ToString().Trim();
                            values[9] = totalParts[1].ToString().Trim();
                            values[8] = totalParts[0].ToString().Trim();
                            break;
                        case 7:
                            values[15] = totalParts[6].ToString().Trim();
                            values[14] = totalParts[5].ToString().Trim();
                            values[13] = totalParts[4].ToString().Trim();
                            values[12] = totalParts[3].ToString().Trim();
                            values[11] = totalParts[2].ToString().Trim();
                            values[10] = totalParts[1].ToString().Trim();
                            values[9] = totalParts[0].ToString().Trim();
                            break;
                        case 6:
                            values[15] = totalParts[5].ToString().Trim();
                            values[14] = totalParts[4].ToString().Trim();
                            values[13] = totalParts[3].ToString().Trim();
                            values[12] = totalParts[2].ToString().Trim();
                            values[11] = totalParts[1].ToString().Trim();
                            values[10] = totalParts[0].ToString().Trim();
                            break;
                        case 5:
                            values[15] = totalParts[4].ToString().Trim();
                            values[14] = totalParts[3].ToString().Trim();
                            values[13] = totalParts[2].ToString().Trim();
                            values[12] = totalParts[1].ToString().Trim();
                            values[11] = totalParts[0].ToString().Trim();
                            break;
                        case 4:
                            values[15] = totalParts[3].ToString().Trim();
                            values[14] = totalParts[2].ToString().Trim();
                            values[13] = totalParts[1].ToString().Trim();
                            values[12] = totalParts[0].ToString().Trim();
                            break;
                        case 3:
                            values[15] = totalParts[2].ToString().Trim();
                            values[14] = totalParts[1].ToString().Trim();
                            values[13] = totalParts[0].ToString().Trim();
                            break;
                        case 2:
                            values[15] = totalParts[1].ToString().Trim();
                            values[14] = totalParts[0].ToString().Trim();
                            break;
                        case 1:
                            values[15] = totalParts[0].ToString().Trim();
                            break;
                    }

                    values[95] = locTbl.Rows[0]["DEPOSITID"].ToString().Trim();
                    values[96] = locTbl.Rows[0]["SITEID"].ToString().Trim().PadLeft(3, '0');

                    //blank out any strings that are null
                    for (int i = values.Length - 1; i >= 0; i--)
                    {
                        if (values[i] == null)
                        {
                            values[i] = "";
                        }
                    }

                    Dictionary<string, string> parameterValues = new Dictionary<string, string>();

                    try
                    {
                        //get the bank account and routing number
                        locTbl = Form1.sqlTransactions.ExecuteQuery($@"DECLARE	@return_value int,
                                                @bankAccount nvarchar(max),
                                                @bankRoutingNumber nvarchar(max)

                                        EXEC	@return_value = [dbo].[HS_GetBankAccountAndRouting]
                                                @bankId = N'{locTbl.Rows[0]["BANKID"].ToString().Trim()}',
                                                @bankAccount = @bankAccount OUTPUT,
                                                @bankRoutingNumber = @bankRoutingNumber OUTPUT

                                        SELECT	@bankAccount as N'@bankAccount',
                                                @bankRoutingNumber as N'@bankRoutingNumber'");

                        //match the bank account and routing number
                        if (locTbl.Rows.Count > 0)
                        {
                            values[4] = locTbl.Rows[0]["@bankAccount"].ToString().Trim();
                            values[5] = locTbl.Rows[0]["@bankRoutingNumber"].ToString().Trim();

                            if (locTbl.Rows[0]["@bankAccount"].ToString().Trim() == "9854533776")
                            {
                                values[0] = "Peak Management LLC";
                                values[1] = "YourSpace Storage";
                                values[2] = @"Resources\220px-M&T_Bank_wordmark.svg.png";
                            }
                        }
                        else
                        {
                            values[0] = "";
                            values[1] = "";
                            values[2] = "";
                        }
                    }
                    catch (SqlException err)
                    {
                        MessageBox.Show("The a problem printing the deposit slip. Please contact IT for further assistance.", "Attention!!!",
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);

                        //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                        //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                        HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                        return;
                    }

                    string newHtml = printOut;

                    //replace the data in HTML
                    for (int i = values.Length - 1; i >= 0; i--)
                    {
                        newHtml = newHtml.Replace("{" + i + "}", values[i].Trim());
                    }

                    //write the html to an output file
                    File.WriteAllText(@"\\hmfile01.hillmgt.com\Finance\Applications\printOut.html", newHtml);

                    //display the print out in chrome
                    System.Diagnostics.Process.Start(@"chrome.exe", @"\\hmfile01.hillmgt.com\Finance\Applications\printOut.html");
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            finally
            {
                this.Useable();
            }
        }

        private void cboDateRanges_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if the date range selected is not set to custom range
                if (cboDateRanges.Text.Trim() != "Custom")
                {
                    int startDay;
                    int endDay;

                    //display the range based on what is selected
                    switch (cboDateRanges.Text.Trim().ToUpper())
                    {
                        case "THIS WEEK":
                            startDay = DayOfWeek.Sunday - DateTime.Now.Date.DayOfWeek;
                            endDay = DayOfWeek.Saturday - DateTime.Now.Date.DayOfWeek;

                            dtpStartDate.Value = DateTime.Now.AddDays(startDay);
                            dtpEndDate.Value = DateTime.Now.AddDays(endDay);
                            break;
                        case "LAST WEEK":
                            startDay = (DayOfWeek.Sunday - DateTime.Now.Date.DayOfWeek) - 7;
                            endDay = (DayOfWeek.Saturday - DateTime.Now.Date.DayOfWeek) - 7;

                            dtpStartDate.Value = DateTime.Now.AddDays(startDay);
                            dtpEndDate.Value = DateTime.Now.AddDays(endDay);
                            break;
                        case "THIS MONTH":
                            startDay = -1 * (DateTime.Now.Date.Day - 1);
                            endDay = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) - DateTime.Now.Date.Day;

                            dtpStartDate.Value = DateTime.Now.AddDays(startDay);
                            dtpEndDate.Value = DateTime.Now.AddDays(endDay);
                            break;
                        case "LAST MONTH":
                            startDay = -1 * ((DateTime.Now.Day) + DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
                            endDay = (DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) - DateTime.Now.Date.Day) - DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);

                            dtpStartDate.Value = DateTime.Now.AddDays(startDay);
                            dtpEndDate.Value = DateTime.Now.AddDays(endDay);
                            break;
                        case "THIS YEAR":
                            startDay = -1 * (DateTime.Now.DayOfYear - 1);
                            endDay = 365 - DateTime.Now.DayOfYear;

                            dtpStartDate.Value = DateTime.Now.AddDays(startDay);
                            dtpEndDate.Value = DateTime.Now.AddDays(endDay);
                            break;
                    }
                }
                else
                {
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void dateChange(object sender, EventArgs e)
        {
            try
            {
                //determine if the control that raised the event is a DateTimePicker
                if (sender.GetType() == typeof(DateTimePicker))
                {
                    //set the control a local variable
                    DateTimePicker control = (DateTimePicker)sender;

                    //check if the control is the End Date
                    if (control.Name == dtpEndDate.Name)
                    {
                        //if the start date is greater than the end date
                        if (dtpStartDate.Value > dtpEndDate.Value)
                        {
                            //set the start date back 7 days from the end date
                            dtpStartDate.Value = dtpEndDate.Value.AddDays(-7);
                        }
                    }//check if the control is the Start Date
                    else if(control.Name == dtpStartDate.Name)
                    {
                        //if the start date is greater than the end date
                        if (dtpStartDate.Value > dtpEndDate.Value)
                        {
                            //set the end date to 7 days after the start date
                            dtpEndDate.Value = dtpStartDate.Value.AddDays(7);
                        }
                    }
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void dgvDeposits_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                //if there is at least one record in the grid go in to the code
                if (dgvDeposits.Rows.Count > 0)
                {
                    //handles the operations the based on cell clicked
                    if (e.RowIndex >= 0)
                    {
                        DataGridViewRow theRow = dgvDeposits.CurrentRow;

                        if (e.ColumnIndex == 0)
                        {//first position delete the deposit
                            if (IsDepositScreen)
                            {
                                SetActualDepositDate(theRow);
                            }
                            else
                            {
                                Delete(theRow.Cells["DEPOSITID"].Value.ToString().Trim());
                            }
                        }
                        else if (e.ColumnIndex == 1)
                        {//second position edit the deposit
                            Edit(theRow);
                            RefreshForm(null, null);
                        }
                        else if (e.ColumnIndex == 2)
                        {//thrid position set the Actual Deposit Date and Reference/Receipt Number
                            SetActualDepositDate(theRow);
                        }
                        else if (e.ColumnIndex == 3)
                        {//fourth positon set the Intended Deposition Date
                            SetIntendDepositDate(theRow);
                        }
                    }
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void dgvDeposits_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {//format the Amount column
                if (e.ColumnIndex == dgvDeposits.Columns["AMOUNT"].Index)
                {//set the amount to the 0.00 if the value is null or display the amount
                    e.Value = !(e.Value == DBNull.Value) ? decimal.Parse(e.Value.ToString()).ToString("c2") : "$0.00";
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void dgvDeposits_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                //if there are no selected rows in the grid
                if (dgvDeposits.SelectedRows.Count > 0)
                {
                    //get the first selected row
                    DataGridViewCell dgvCell = dgvDeposits.SelectedRows[0].Cells["AMOUNT"];

                    //attempt to parse the value as a decimal
                    if (decimal.TryParse(dgvCell.Value.ToString().Trim(), out decimal amount))
                    {
                        //if the amount is greater than 0
                        if (amount > 0M)
                        {
                            //allow the deposit slip to be printed
                            btnPrint.Enabled = true;
                        }
                        else
                        {
                            //do not allow the deposit slip to be printed
                            btnPrint.Enabled = false;
                        }
                    }
                    else
                    {//if the cell value is not a decimal
                        //do not allow the deposit slip to be printed
                        btnPrint.Enabled = false;
                    }
                }
                else
                {//if there are no selected rows
                    //do not allow the deposit slip to be printed
                    btnPrint.Enabled = false;
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void frmBankDept_Load(object sender, EventArgs e)
        {
            try
            {
                this.Wait();
                dtpStartDate.Value = DateTime.Now.AddDays(-7);
                dtpEndDate.Value = DateTime.Now;
                CreateDropdowns();
                PopulateData(cboProperty.SelectedValue.ToString().Trim(), dtpStartDate.Value, dtpEndDate.Value, txtDepositId.Text.Trim());

                //set the title bar with the SQL database connected to
                this.Text = this.Text + " (MS SQL Server: " + Form1.sqlTransactions.GetDatabase().Trim() + ")";

                //set the mousewheel event handler on the Property dropdown and Start and End Date Time
                cboProperty.MouseWheel += MouseWheelIgnore;
                cboDateRanges.MouseWheel += MouseWheelIgnore;
                cboDateRanges.SelectedIndexChanged += RefreshForm;

                if (IsDepositScreen)
                {
                    btnAdd.Visible = false;
                }
                else
                {
                    btnAdd.Visible = true;
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            finally
            {
                this.Useable();
            }
        }

        private void MouseWheelIgnore(object sender, MouseEventArgs e)
        {//handles the mouse wheel scrolling the drop down boxes so the user does not accidently switch the item in the drop down
            ((HandledMouseEventArgs)e).Handled = true;
        }

        private void previewbutton_Click(object sender, EventArgs e)
        {
            try
            {
                this.Wait();
                //display the form to add a new Bank Deposit
                frmBankDept01 frmBankDep = new frmBankDept01(frmBankDept01.ACTION.PREVIEW, this);
                frmBankDep.ShowDialog(this);
                RefreshForm(null, null);
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            finally
            {
                this.Useable();
            }
        }

        public void RefreshForm(object sender, EventArgs e)
        {//handles the setting the form to default state
            try
            {
                this.Wait();

                if (txtDepositId.Text.Trim() != "")
                {
                    if (int.TryParse(txtDepositId.Text.Trim(), out int depositId))
                    {

                    }
                    else
                    {
                        MessageBox.Show("Please enter a number for the Deposit Id.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtDepositId.Focus();
                        return;
                    }
                }

                //make sure there is control that cause this to be called
                if (sender != null)
                {
                    if (((Control)sender).Name.Trim() == "dtpStartDate" | ((Control)sender).Name.Trim() == "dtpEndDate")
                    {
                        cboDateRanges.SelectedIndex = 0;
                    }
                }

                PopulateData(cboProperty.SelectedValue.ToString().Trim(), dtpStartDate.Value, dtpEndDate.Value, txtDepositId.Text.Trim());
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            finally
            {
                this.Useable();
            }
        }

        private void txtDepositId_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtDepositId.Text.Trim() != "")
                {
                    if (int.TryParse(txtDepositId.Text.Trim(), out int depositId))
                    {

                    }
                    else
                    {
                        MessageBox.Show("Please enter a number for the Deposit Id.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtDepositId.Focus();
                    }
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }
        #endregion
    }
}