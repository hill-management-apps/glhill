﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using HelpfulClasses;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using SqlDataAccess;
using System.Diagnostics;

namespace GLHill.Forms
{
    public partial class frmSetClsBillDates : Form
    {
        public static OleDbTransactions as400Transactions;

        #region "Enumerations"
        /// <summary>
        /// Represents that state the form is in.
        /// </summary>
        public enum STATE
        {
            /// <summary>
            /// There is no change pending to go to production
            /// </summary>
            NO_CHANGE,
            /// <summary>
            /// There is a change pending to go to production.
            /// </summary>
            CHANGE
        }
        #endregion

        #region "Field Declarations"
        /// <summary>
        /// The state the form is in.
        /// </summary>
        public STATE state = new STATE();
        string strSQL = "";
        #endregion

        #region "Constructors"
        public frmSetClsBillDates()
        {
            InitializeComponent();

            try
            {
                as400Transactions = new OleDbTransactions(Properties.Settings.Default.as400Connection);
                as400Transactions.Connect();
            }
            catch (Exception err)
            {

            }
        }
        #endregion

        #region "Methods"
        /// <summary>
        /// Sets the form back to its default state and controls.
        /// </summary>
        public override void Refresh()
        {
            DataTable locTbl = new DataTable();

            try
            {

                //clear the data grid view
                dgvBillingClosingDates.DataSource = null;

                try
                {
                    //retrieves the current dates
                    strSQL = "SELECT PERIOD, RMBILLING, RMCUTOFF FROM HS_DATESHOLD " +
                             " WHERE PERIOD > '" + DateTime.Now.ToString("yyyyMM") + "' OR SUBDAT IS NULL " +
                             " ORDER BY PERIOD DESC";

                    //handle the exceptions from a SQL query but continue refreshing the form and do not display data in the grid
                    try
                    {
                        locTbl = Form1.sqlTransactions.ExecuteQuery(strSQL);
                    }
                    catch (ArgumentException err)
                    {
                        MessageBox.Show("There was a problem retrieving the dates. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                        //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                        HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                    }
                    catch (InvalidCastException err)
                    {
                        MessageBox.Show("There was a problem retrieving the dates. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                        //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                        HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                    }
                    catch (InvalidOperationException err)
                    {
                        MessageBox.Show("There was a problem retrieving the dates. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                        //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                        HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                    }
                    catch (SystemException err)
                    {
                        MessageBox.Show("There was a problem retrieving the dates. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                        //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                        HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                    }
                } 
                catch (ArgumentOutOfRangeException err)
                {
                    MessageBox.Show("There was a problem retrieving the dates. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                    //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                    HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                }
                catch (FormatException err)
                {
                    MessageBox.Show("There was a problem retrieving the dates. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                    //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                    HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                }

                //display the data in the data grid view
                dgvBillingClosingDates.DataSource = locTbl;

                //check if the there is one record selected and enable the delete button if it is
                if (dgvBillingClosingDates.SelectedRows.Count > 0)
                {
                    btnUpdate.Enabled = true;
                    btnDelete.Enabled = true;
                }
                else
                {
                    btnUpdate.Enabled = false;
                    btnDelete.Enabled = false;
                }

                //formats the header row to readable titles
                foreach (DataGridViewColumn dgvc in dgvBillingClosingDates.Columns)
                {
                    switch (dgvc.HeaderText.Trim())
                    {
                        case "RMCUTOFF":
                            dgvc.HeaderText = "End Of Month Date";
                            break;
                        case "RMBILLING":
                            dgvc.HeaderText = "Billing Date";
                            break;
                        case "PERIOD":
                            dgvc.HeaderText = "Period";
                            break;
                    }
                }

                dgvBillingClosingDates.AutoResizeColumns();
            }
            catch (Exception err)
            {
                MessageBox.Show("There was a problem refreshing the form. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            finally
            {
                locTbl.Dispose();
            }
        }

        /// <summary>
        /// Sends an email to the help desk.
        /// </summary>
        /// <param name="Body">The body of the email.</param>
        /// <param name="Subject">The subject of the email.</param>
        /// <param name="toAddresses">The collection of emails to send the email to.</param>
        private void SendEmail(string Body, string Subject, List<MailAddress> toAddresses)
        {
            SmtpClient email = new SmtpClient();
            MailMessage message = new MailMessage();

            try
            {
                //sets the address and provides credentials
                email.Host = "192.168.100.61";
                email.Credentials = new NetworkCredential("FLDSYNCSL", "PWSL278");
                //add each email address
                foreach (MailAddress a in toAddresses)
                {
                    message.To.Add(a);
                }
                //sets the email from address, body, and subject
                message.Subject = Subject.Trim();
                message.From = new MailAddress("mplus@hillmgt.com");
                message.IsBodyHtml = true;
                message.Body = Body.Trim();

                try
                {
                    //sends the email
                    email.Send(message);
                }
                catch (ArgumentNullException err)
                {
                    MessageBox.Show("There was a problem sending a message to the help desk. Please contact IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                    //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                    HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                }
                catch (ObjectDisposedException err)
                {
                    MessageBox.Show("There was a problem sending a message to the help desk. Please contact IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                    //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                    HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                }
                catch (InvalidOperationException err)
                {
                    MessageBox.Show("There was a problem sending a message to the help desk. Please contact IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                    //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                    HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                }
                catch (SmtpFailedRecipientsException err)
                {
                    MessageBox.Show("There was a problem sending a message to the help desk. Please contact IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                    //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                    HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                }
                catch (SmtpException err)
                {
                    MessageBox.Show("There was a problem sending a message to the help desk. Please contact IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                    //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                    HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                }

                message.Dispose();
                email.Dispose();
            }
            catch (ArgumentNullException err)
            {
                MessageBox.Show("There was a problem sending a message to the help desk. Please contact IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            catch (ArgumentException err)
            {
                MessageBox.Show("There was a problem sending a message to the help desk. Please contact IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            catch (InvalidOperationException err)
            {
                MessageBox.Show("There was a problem sending a message to the help desk. Please contact IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            catch (Exception err)
            {
                MessageBox.Show("There was a problem sending a message to the help desk. Please contact IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            finally
            {
                message.Dispose();
                email.Dispose();
            }
        }

        /// <summary>
        /// Sets up the form in its initial state.
        /// </summary>
        private void SetupForm()
        {
            try
            {
                //sets the form to no change state
                state = STATE.NO_CHANGE;

                Refresh();

                //setup button click events for the button to point to the same method
                btnAdd.Click += new EventHandler(ButtonClick);
                btnUpdate.Click += new EventHandler(ButtonClick);
            }
            catch (Exception err)
            {
                MessageBox.Show("There was a problem with the form. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }
        #endregion

        #region "Event Handlers"
        /// <summary>
        /// A method meant to handle the <see cref="btnAdd"/> and <see cref="btnUpdate"/> <see cref="Control.Click(EventArgs)"/>.
        /// </summary>
        /// <param name="sender">Object parameter for the button clicked.</param>
        /// <param name="e">The event args from the from button clicked.</param>
        private void ButtonClick(object sender, EventArgs e)
        {
            frmSetClsBillDates01 frm;
            Button btn;

            try
            {
                //ensures the control that raises this event is a button
                if (sender.GetType().Name == "Button")
                {
                    //set the control to a local variable button
                    btn = (Button)sender;

                    switch (btn.Name)
                    {
                        //handle if the button clicked was Add
                        case "btnAdd":
                            frm = new frmSetClsBillDates01(frmSetClsBillDates01.Action.ADD, this);
                            frm.ShowDialog();
                            break;
                        //handle if the button clicked was Update
                        case "btnUpdate":
                            frm = new frmSetClsBillDates01(frmSetClsBillDates01.Action.UPDATE, this, dgvBillingClosingDates.SelectedRows[0]);
                            frm.ShowDialog();
                            break;
                    }
                }
            }
            catch (Exception err)
            {
                Dictionary<string, string> extras = new Dictionary<string, string>();

                extras.Add("Button_Clicked", ((Control) sender).Name.Trim());
                
                if(dgvBillingClosingDates.SelectedRows.Count > 0)
                {
                    extras.Add("PERIOD", dgvBillingClosingDates.SelectedRows[0].Cells["PERIOD"].Value.ToString().Trim());
                }

                MessageBox.Show("There was a problem adding or updating the period. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);

                extras = null;
            }
            finally
            {
                //clear the objects that are not used anymore
                btn = null;
                frm = null;
            }
        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            try
            {
                Form1.sqlTransactions.DeleteTableData("HS_DATESHOLD", "PERIOD > '" + DateTime.Now.ToString("yyyyMM") + "'");
                Form1.sqlTransactions.DeleteTableData("HS_DATES", "PERIOD > '" + DateTime.Now.ToString("yyyyMM") + "'");
                as400Transactions.DeleteTableData("HS_DATES", "PERIOD > '" + DateTime.Now.ToString("yyyyMM") + "'");

                state = STATE.CHANGE;

                Refresh();
            }
            catch (ObjectDisposedException err)
            {
                MessageBox.Show("There was a problem removing all the dates. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            catch (InvalidCastException err)
            {
                MessageBox.Show("There was a problem removing all the dates. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            catch (SqlException err)
            {
                MessageBox.Show("There was a problem removing all the dates. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            catch (IOException err)
            {
                MessageBox.Show("There was a problem removing all the dates. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            catch (InvalidOperationException err)
            {
                MessageBox.Show("There was a problem removing all the dates. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            catch (Exception err)
            {
                MessageBox.Show("There was a problem removing all the dates. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                //check that the user does intend to delete the selected record
                DialogResult response = MessageBox.Show("You are about remove this date. Are you sure you want to?", "Attention!!!",
                                                        MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                //if they do want to delete the record, the record is deleted
                if (response == DialogResult.Yes)
                {
                    //setup the where clause field and value where to delete the data
                    Dictionary<string, string> whereValues = new Dictionary<string, string>();
                    string period = dgvBillingClosingDates.SelectedRows[0].Cells["PERIOD"].Value.ToString().Trim();

                    whereValues.Add("PERIOD", period);

                    //delete the data
                    Form1.sqlTransactions.DeleteTableData("HS_DATESHOLD", whereValues);

                    //set the form to pending data change
                    state = STATE.CHANGE;

                    whereValues.Clear();
                }

                Refresh();
            }
            catch (Exception err)
            {
                Dictionary<string, string> extras = new Dictionary<string, string>();

                extras.Add("PERIOD", dgvBillingClosingDates.SelectedRows[0].Cells["PERIOD"].Value.ToString().Trim());

                MessageBox.Show("There was a problem deleting the period. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);

                extras = null;
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string emailBody = "";
            DataTable locTbl = (DataTable)dgvBillingClosingDates.DataSource;
            List<MailAddress> emailAddress = new List<MailAddress>();
            Dictionary<string, string> updateValues = new Dictionary<string, string>();
            Dictionary<string, string> whereValues = new Dictionary<string, string>();
            Dictionary<string, string> insertValues = new Dictionary<string, string>();

            try
            {
                //clear all the future periods in the MRI and AS400 HS_DATES
                Form1.sqlTransactions.DeleteTableData("HS_DATES", "PERIOD > '" + DateTime.Now.ToString("yyyyMM") + "'");
                as400Transactions.DeleteTableData("HS_DATES", "PERIOD > '" + DateTime.Now.ToString("yyyyMM") + "'");

                //setup the beginning part of the email
                emailBody = @"<html>
                            <head>
                                <style>
                                    .cellStyle {
                                        text-align:center;
                                        border: solid 1px;
                                    }
                                </style>
                            </head>
                            <body>
                                <table style='border - collapse:collapse;'>
                                       <tr> 
                                           <td colspan = '3'>    
                                                <h4>
                                                    The Billing and Closing dates have been updated.<br/>
                                                    Please review the list below to see the current list. 
                                                </h4>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style='text-align:center;border: solid 1px;'>Period</td>
                                            <td style='text-align:center;border: solid 1px;'>Billing</td>
                                            <td style='text-align:center;border: solid 1px;'>End Of Month</td>
                                        </tr>";

                //goes through each row in the data grid view 
                foreach (DataRow row in locTbl.Rows)
                {
                    insertValues.Add("PERIOD", row["PERIOD"].ToString().Trim());
                    insertValues.Add("RMCUTOFF", DateTime.Parse(row["RMCUTOFF"].ToString().Trim()).ToString("yyyy-MM-dd"));
                    insertValues.Add("RMBILLING", DateTime.Parse(row["RMBILLING"].ToString().Trim()).ToString("yyyy-MM-dd"));

                    //insert the period dates into MRI and AS400
                    Form1.sqlTransactions.InsertTableData("HS_DATES", insertValues);
                    as400Transactions.InsertTableData("HS_DATES", insertValues);

                    //set the period to update in HS_DATESHOLD
                    whereValues.Add("PERIOD", row["PERIOD"].ToString().Trim());

                    //Set the who submitted the record and when in HS_DATAHOLD
                    updateValues.Add("SUBDAT", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                    updateValues.Add("SUBUSER", System.Security.Principal.WindowsIdentity.GetCurrent().Name);

                    //update the record
                    Form1.sqlTransactions.UpdateTableData("HS_DATESHOLD", updateValues, whereValues);

                    //set the form back to no change
                    state = STATE.NO_CHANGE;

                    //clear the insert, update, and where values
                    insertValues.Clear();
                    updateValues.Clear();
                    whereValues.Clear();

                    //get the dates period and format it
                    DateTime per = DateTime.Parse(row["PERIOD"].ToString().Substring(0, 4) + "-" + row["PERIOD"].ToString().Substring(4, 2) + "-01");

                    //add the set of dates and period to the email body
                    emailBody += string.Format(@"<tr>
                                             <td style='text-align:center;border: solid 1px;'>{0}</td><td style='text-align:center;border: solid 1px;'>{1}</td>
                                             <td style='text-align:center;border: solid 1px;'>{2}</td>
                                             </tr>", per.ToString("MMM") + " " + per.Year,
                                                DateTime.Parse(row["RMBILLING"].ToString().Trim()).ToString("MM/dd/yyyy"), DateTime.Parse(row["RMCUTOFF"].ToString().Trim()).ToString("MM/dd/yyyy"));
                }

                //add the email ending to the email body
                emailBody += "</table></body></html>";

                HelpfulFunctions.CreateTicketOnFreshService(emailBody, "Billing and End Of Month Dates have been changed.");

                Refresh();

                MessageBox.Show("The dates have been successfully saved.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (ArgumentOutOfRangeException err)
            {
                MessageBox.Show("There was a problem submitting the periods. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            catch (FormatException err)
            {
                MessageBox.Show("There was a problem submitting the periods. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            catch (Exception err)
            {
                MessageBox.Show("There was a problem submitting the periods. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);

            }
            finally
            {
                whereValues = null;
                insertValues = null;
                updateValues = null;
                emailAddress = null;
                locTbl = null;
            }
        }

        private void dgvBillingClosingDates_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                //foramts the column headers to be readable
                switch (dgvBillingClosingDates.Columns[e.ColumnIndex].Name)
                {
                    case "RMCUTOFF":
                        e.Value = DateTime.Parse(e.Value.ToString()).ToString("MMMM dd, yyyy");
                        break;
                    case "RMBILLING":
                        e.Value = DateTime.Parse(e.Value.ToString()).ToString("MMMM dd, yyyy");
                        break;
                    case "PERIOD":
                        DateTime per = DateTime.Parse(e.Value.ToString().Substring(0, 4) + "/" + e.Value.ToString().Substring(4, 2) + "/01");

                        e.Value = per.ToString("MMM") + "-" + per.Year;
                        break;
                }
            }
            catch (ArgumentOutOfRangeException err)
            {
                MessageBox.Show("There was a problem formatting the data. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            catch (FormatException err)
            {
                MessageBox.Show("There was a problem submitting the periods. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            catch (Exception err)
            {
                MessageBox.Show("There was a problem submitting the periods. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);

            }
        }

        private void dgvBillingClosingDates_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvBillingClosingDates.SelectedRows.Count > 0)
                {
                    btnUpdate.Enabled = true;
                    btnDelete.Enabled = true;
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("There was a problem with the data. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);

            }
        }

        private void frmSetClsBillDates_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                //if the form is set that something has changed
                if (state == STATE.CHANGE)
                {
                    //display to the user that the data has changed and they need to check before closing
                    DialogResult response = MessageBox.Show("You have changes you have not submitted. Are you sure you want to close this form before doing so?", "Attention!!!",
                                                            MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                    //if the user clicks 'Yes', they do not want to submit before closing
                    if (response == DialogResult.Yes)
                    {

                    }
                    else
                    {
                        //if the user clicks 'No', the user does not want to close before submitting the changes
                        e.Cancel = true;
                        return;
                    }
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("There was a problem closing the form. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);

            }
        }

        private void frmSetClsBillDates_Load(object sender, EventArgs e)
        {
            try
            {
                SetupForm();

                //formats the title bar to show the name and the databases selected
                this.Text = this.Text + " (MS SQL Server: " + Form1.sqlTransactions.GetDatabase().Trim() + " / AS400 Server: " + as400Transactions.GetDatabase().Trim() + ")";
            }
            catch (Exception err)
            {
                MessageBox.Show("There was a problem loading the form. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);

            }
        }
        #endregion
    }
}
