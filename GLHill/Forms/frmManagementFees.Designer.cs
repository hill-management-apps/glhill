﻿
namespace GLHill.Forms
{
    partial class frmManagementFees
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvManagementFees = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.cboEndingPeriod = new System.Windows.Forms.ComboBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnCreateJournalEntries = new System.Windows.Forms.Button();
            this.btnSaveAsExcel = new System.Windows.Forms.Button();
            this.chkHideUnpostableTransactions = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvManagementFees)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvManagementFees
            // 
            this.dgvManagementFees.AllowUserToAddRows = false;
            this.dgvManagementFees.AllowUserToDeleteRows = false;
            this.dgvManagementFees.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvManagementFees.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvManagementFees.Location = new System.Drawing.Point(13, 90);
            this.dgvManagementFees.Margin = new System.Windows.Forms.Padding(4);
            this.dgvManagementFees.Name = "dgvManagementFees";
            this.dgvManagementFees.ReadOnly = true;
            this.dgvManagementFees.RowHeadersWidth = 51;
            this.dgvManagementFees.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvManagementFees.Size = new System.Drawing.Size(1298, 433);
            this.dgvManagementFees.TabIndex = 0;
            this.dgvManagementFees.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvManagementFees_CellFormatting);
            this.dgvManagementFees.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgvManagementFees_MouseClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 11);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Ending Period:";
            // 
            // cboEndingPeriod
            // 
            this.cboEndingPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboEndingPeriod.FormattingEnabled = true;
            this.cboEndingPeriod.Location = new System.Drawing.Point(125, 11);
            this.cboEndingPeriod.Margin = new System.Windows.Forms.Padding(4);
            this.cboEndingPeriod.Name = "cboEndingPeriod";
            this.cboEndingPeriod.Size = new System.Drawing.Size(160, 24);
            this.cboEndingPeriod.TabIndex = 4;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(295, 11);
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(4);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(100, 28);
            this.btnRefresh.TabIndex = 5;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnCreateJournalEntries
            // 
            this.btnCreateJournalEntries.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnCreateJournalEntries.Enabled = false;
            this.btnCreateJournalEntries.Location = new System.Drawing.Point(1052, 11);
            this.btnCreateJournalEntries.Margin = new System.Windows.Forms.Padding(4);
            this.btnCreateJournalEntries.Name = "btnCreateJournalEntries";
            this.btnCreateJournalEntries.Size = new System.Drawing.Size(116, 57);
            this.btnCreateJournalEntries.TabIndex = 6;
            this.btnCreateJournalEntries.Text = "Create Journal Entries";
            this.btnCreateJournalEntries.UseVisualStyleBackColor = true;
            this.btnCreateJournalEntries.Click += new System.EventHandler(this.btnCreateJournalEntries_Click);
            // 
            // btnSaveAsExcel
            // 
            this.btnSaveAsExcel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSaveAsExcel.Enabled = false;
            this.btnSaveAsExcel.Location = new System.Drawing.Point(923, 11);
            this.btnSaveAsExcel.Margin = new System.Windows.Forms.Padding(4);
            this.btnSaveAsExcel.Name = "btnSaveAsExcel";
            this.btnSaveAsExcel.Size = new System.Drawing.Size(111, 57);
            this.btnSaveAsExcel.TabIndex = 7;
            this.btnSaveAsExcel.Text = "Save as Excel";
            this.btnSaveAsExcel.UseVisualStyleBackColor = true;
            this.btnSaveAsExcel.Click += new System.EventHandler(this.btnSaveAsExcel_Click);
            // 
            // chkHideUnpostableTransactions
            // 
            this.chkHideUnpostableTransactions.AutoSize = true;
            this.chkHideUnpostableTransactions.Location = new System.Drawing.Point(13, 47);
            this.chkHideUnpostableTransactions.Margin = new System.Windows.Forms.Padding(4);
            this.chkHideUnpostableTransactions.Name = "chkHideUnpostableTransactions";
            this.chkHideUnpostableTransactions.Size = new System.Drawing.Size(593, 21);
            this.chkHideUnpostableTransactions.TabIndex = 8;
            this.chkHideUnpostableTransactions.Text = "Hide transactions that are less than one dollar. (These will NOT be posted to the" +
    " Journal)";
            this.chkHideUnpostableTransactions.UseVisualStyleBackColor = true;
            this.chkHideUnpostableTransactions.CheckedChanged += new System.EventHandler(this.chkHideUnpostableTransactions_CheckedChanged);
            // 
            // frmManagementFees
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1327, 554);
            this.Controls.Add(this.chkHideUnpostableTransactions);
            this.Controls.Add(this.btnSaveAsExcel);
            this.Controls.Add(this.btnCreateJournalEntries);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.cboEndingPeriod);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgvManagementFees);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmManagementFees";
            this.Text = "Management Fees";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmManagementFees_FormClosing);
            this.Load += new System.EventHandler(this.frmManagementFees_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvManagementFees)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void DgvManagementFees_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            throw new System.NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.DataGridView dgvManagementFees;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboEndingPeriod;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnCreateJournalEntries;
        private System.Windows.Forms.Button btnSaveAsExcel;
        private System.Windows.Forms.CheckBox chkHideUnpostableTransactions;
    }
}