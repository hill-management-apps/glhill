﻿using HelpfulClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GLHill.Forms
{
    public partial class frmSetClsBillDates01 : Form
    {
        #region "Enumerations"
        public enum Action
        {
            ADD,
            UPDATE
        }
        #endregion

        #region "Field Declarations"
        private frmSetClsBillDates frm;
        private DataGridViewRow dgvr;
        private Action action;
        #endregion

        #region "Constructors"
        public frmSetClsBillDates01(Action action, frmSetClsBillDates frm, DataGridViewRow dgvr = null)
        {
            InitializeComponent();

            this.frm = frm;
            this.dgvr = dgvr;
            this.action = action;
        }
        #endregion

        #region "Event Handlers"
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //check that the controls on the form have valid values before changing any information
                if (IsValid())
                {
                    Dictionary<string, string> whereValues = new Dictionary<string, string>();
                    Dictionary<string, string> changeValues = new Dictionary<string, string>();

                    switch (action)
                    {
                        //if the form was called to add a record
                        case Action.ADD:
                            changeValues.Add("PERIOD", txtPeriod.Text.Trim());

                            //check if the period is in the temporary table, MRI, or AS400
                            if (!Form1.sqlTransactions.RecordExists("HS_DATESHOLD", changeValues) &
                                !Form1.sqlTransactions.RecordExists("HS_DATES", changeValues) &
                                !frmSetClsBillDates.as400Transactions.RecordExists("HS_DATES", changeValues))
                            {
                                changeValues.Add("RMCUTOFF", dtpEndOfMonth.Value.ToString("yyyy-MM-dd"));
                                changeValues.Add("RMBILLING", dtpBilling.Value.ToString("yyyy-MM-dd"));

                                Form1.sqlTransactions.InsertTableData("HS_DATESHOLD", changeValues);
                                frm.state = frmSetClsBillDates.STATE.CHANGE;

                                frm.Refresh();
                                Reset();

                                whereValues = null;
                                changeValues = null;
                            }
                            else
                            {
                                MessageBox.Show("This period has already been entered.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }

                            break;
                        case Action.UPDATE:
                            whereValues.Add("PERIOD", txtPeriod.Text.Trim());
                            changeValues.Add("RMCUTOFF", dtpEndOfMonth.Value.ToString("yyyy-MM-dd"));
                            changeValues.Add("RMBILLING", dtpBilling.Value.ToString("yyyy-MM-dd"));

                            //check if the record is MRI or AS400, but no need to check temporary table on update
                            if (!Form1.sqlTransactions.RecordExists("HS_DATES", changeValues) &
                                !frmSetClsBillDates.as400Transactions.RecordExists("HS_DATES", changeValues))
                            {
                                Form1.sqlTransactions.UpdateTableData("HS_DATESHOLD", changeValues, whereValues);
                                frm.state = frmSetClsBillDates.STATE.CHANGE;

                                frm.Refresh();

                                whereValues = null;
                                changeValues = null;

                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("This period is already in use. It cannot be changed.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }

                            break;
                    }
                }
            }
            catch (ArgumentOutOfRangeException err)
            {
                MessageBox.Show("There was a problem saving the record. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            catch (FormatException err)
            {
                MessageBox.Show("There was a problem saving the record. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            catch (Exception err)
            {
                MessageBox.Show("There was a problem saving the record. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);

            }
        }

        private void dtpBilling_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                txtPeriod.Text = dtpBilling.Value.ToString("yyyyMM");
            }
            catch (ArgumentOutOfRangeException err)
            {
                MessageBox.Show("There was a problem selecting a new billing date. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            catch (Exception err)
            {
                MessageBox.Show("There was a problem selecting a new billing date. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);

            }
        }

        private void frmSetClsBillDates01_Load(object sender, EventArgs e)
        {
            try
            {
                Reset();

                this.Text = this.Text + " (MS SQL Server: " + Form1.sqlTransactions.GetDatabase().Trim() + " / AS400 Server: " + frmSetClsBillDates.as400Transactions.GetDatabase().Trim() + ")";
            }
            catch (Exception err)
            {
                MessageBox.Show("There was a problem loading the form. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);

            }
        }
        #endregion

        #region "Methods"
        private bool IsValid()
        {
            try
            {
                int month = 0;
                int year = 0;

                //check that the period field is numeric
                if (!int.TryParse(txtPeriod.Text.Trim(), out _))
                {
                    MessageBox.Show("The period has to be number formatted 4-digit year and 2-digit month.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtPeriod.Focus();
                    return false;
                }
                //check that the period is a 6-digits
                else if (txtPeriod.Text.Length < 6)
                {
                    MessageBox.Show("The period has to be 6 digits and formatted 4-digit year and 2-digit month.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtPeriod.Focus();
                    return false;
                }
                else
                {
                    //set the month and year once it is determind the period is number
                    month = int.Parse(txtPeriod.Text.Substring(4, 2));
                    year = int.Parse(txtPeriod.Text.Substring(0, 4));

                    //check that the period is not too many years in the future
                    if (year > DateTime.Now.Year + 5)
                    {
                        MessageBox.Show("The period is more than 5 years in the future. Please adjust the period.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtPeriod.Focus();
                        return false;
                    }
                    //check that the period is not too many years in the past
                    else if (year < DateTime.Now.Year - 5)
                    {
                        MessageBox.Show("The period is more than 5 years in the past. Please adjust the period.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtPeriod.Focus();
                        return false;
                    }
                    //check that the month is between 1 and 12
                    else if (month < 1 | month > 12)
                    {
                        MessageBox.Show("The period month is not a month. Please adjust the period.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtPeriod.Focus();
                        return false;
                    }
                    //check the End Of Month date is after the the Billing Date
                    else if (dtpBilling.Value > dtpEndOfMonth.Value)
                    {
                        MessageBox.Show("The Billing Date is after the End Of Month Date. The Billing Date must be before the End of Month Date.", "Attention!!!",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        dtpBilling.Focus();
                        return false;
                    }
                    //check that the End Of Month Date month is the same as the period month
                    else if (dtpEndOfMonth.Value.Month != month)
                    {
                        MessageBox.Show("The End Of Month date is not in the same month as the period.", "Attention!!!",
                                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                        dtpEndOfMonth.Focus();
                        return false;
                    }
                    //check that the Billing Date month is the same as the period month
                    else if (dtpBilling.Value.Month != month)
                    {
                        MessageBox.Show("The Billing date is not in the same month as the period.", "Attention!!!",
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                        dtpBilling.Focus();
                        return false;
                    }
                    //check that the End Of Month Date year is the same as the period year
                    else if (dtpEndOfMonth.Value.Year != year)
                    {
                        MessageBox.Show("The End Of Month date is not in the same year as the period.", "Attention!!!",
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                        dtpEndOfMonth.Focus();
                        return false;
                    }
                    //check that the Billing Date year is the same as the period month
                    else if (dtpBilling.Value.Year != year)
                    {
                        MessageBox.Show("The Billing date is not in the same year as the period.", "Attention!!!",
                         MessageBoxButtons.OK, MessageBoxIcon.Error);
                        dtpBilling.Focus();
                        return false;
                    }
                }

                return true;
            }
            catch (ArgumentOutOfRangeException err)
            {
                MessageBox.Show("There was a problem validating the information. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                return false;
            }
            catch (FormatException err)
            {
                MessageBox.Show("There was a problem validating the information. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                return false;
            }
            catch (Exception err)
            {
                MessageBox.Show("There was a problem validating the information. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                return false;

            }
        }

        private void Reset()
        {
            try
            {
                txtPeriod.Text = "";
                dtpEndOfMonth.Value = DateTime.Now;
                dtpBilling.Value = DateTime.Now;

                //setup form based on the calling forms set action
                switch (action)
                {
                    case Action.ADD:
                        this.Text = "ADD";

                        dtpEndOfMonth.Enabled = true;
                        dtpBilling.Enabled = true;

                        break;
                    case Action.UPDATE:
                        this.Text = "UPDATE";

                        txtPeriod.Enabled = false;
                        dtpEndOfMonth.Enabled = true;
                        dtpBilling.Enabled = true;

                        txtPeriod.Text = dgvr.Cells["PERIOD"].Value.ToString().Trim();
                        dtpEndOfMonth.Value = DateTime.Parse(dgvr.Cells["RMCUTOFF"].Value.ToString().Trim());
                        dtpBilling.Value = DateTime.Parse(dgvr.Cells["RMBILLING"].Value.ToString().Trim());

                        break;
                }
            }
            catch (ArgumentOutOfRangeException err)
            {
                MessageBox.Show("There was a problem resetting the form. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            catch (FormatException err)
            {
                MessageBox.Show("There was a problem resetting the form. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            catch (Exception err)
            {
                MessageBox.Show("There was a problem resetting the form. A ticket has been created with IT.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }
    }
    #endregion
}
