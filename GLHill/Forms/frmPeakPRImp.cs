﻿using ExcelDataReader;
using GLHill.Classes;
using HelpfulClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace GLHill.Forms
{
    public partial class frmPeakPRImp : frmImportBase
    {
        private IExcelDataReader excel;
        private string currentName = "";
        private string fileName = "";
        private DateTime currentPayWeekEnding = DateTime.MinValue;
        private DateTime currentPayDate = DateTime.MinValue;
        private List<GeneralLedgerPropertyTotal> generalLedgerPropertyTotals = new List<GeneralLedgerPropertyTotal>();

        public frmPeakPRImp()
        {
            InitializeComponent();

            this.Useable();
        }

        public string CalculateRefNum(string period, string source)
        {
            string retVal = string.Empty;

            try
            {
                string sql = string.Format("select max(ref) from (select max(ref) as ref from journal where source = '{0}' and period = {1} and ref not like '%[^0-9]%'", source.Trim(), period.Trim()) +
                    string.Format("union select max(ref) as ref from ghis where source = '{0}' and period = {1} and ref not like '%[^0-9]%') as refunion HAVING MAX(REF) <= '000099'", source.Trim(), period.Trim());

                DataTable locTbl = Form1.sqlTransactions.ExecuteQuery(sql);

                if (locTbl.Rows.Count < 1)
                {
                    retVal = "000001";
                }
                else
                {
                    // We have a value for max(ref)
                    int numericValue = int.Parse(locTbl.Rows[0][0].ToString());
                    numericValue++;

                    if (numericValue <= 99)
                    {
                        retVal = numericValue.ToString().PadLeft(6, '0');
                    }
                    else
                    {
                        int x;

                        for (x = 1; x < 99; x++)
                        {
                            sql = string.Format("select ref from (select ref from journal where source = '{0}' and period = '{1}' and ref not like '%[^0-9]%'", source.Trim(), period.Trim()) +
                            string.Format("union select ref from ghis where source = '{0}' and period = '{1}' and ref not like '%[^0-9]%') as refunion WHERE REF = '0000{2}'", source.Trim(), period.Trim(), x.ToString().PadLeft(2, '0'));

                            locTbl = Form1.sqlTransactions.ExecuteQuery(sql);

                            if (locTbl.Rows.Count == 0)
                            {
                                retVal = x.ToString().PadLeft(6, '0');
                                break;
                            }
                        }

                        if (x >= 99)
                        {
                            throw new Exception(string.Format("There are too many references created for this period. " +
                                " A reference number cannot be 99 or greater. The next one will be {0}.", numericValue));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return retVal;
        }

        private void FormatHeader()
        {
            try
            {
                dgvData.Columns["Property"].HeaderText = "Property";
                dgvData.Columns["Department"].HeaderText = "Department";
                dgvData.Columns["Account"].HeaderText = "Account";
                dgvData.Columns["Credit"].HeaderText = "Credit";
                dgvData.Columns["Debit"].HeaderText = "Debit";
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void RefreshForm()
        {
            try
            {
                btnApprove.Enabled = false;
                dgvData.DataSource = null;
                dgvErrors.DataSource = null;
                currentName = "";
                currentPayDate = DateTime.MinValue;
                currentPayWeekEnding = DateTime.MinValue;
                fileName = "";
                generalLedgerPropertyTotals.Clear();
                if ((excel != null) && (!excel.IsClosed))
                {
                    excel.Close();
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ledgerCode"> HS or WC </param>
        /// <param name="Group">Group of accounts</param>
        /// <param name="entityid">Property # </param>
        /// <returns>Returns specific 12 alpha account </returns>
        private string SetDistinctAccount(string ledgerCode, string Group, string entityid)
        {
            if (Group == "PROPMGR")  // Property Manager Payroll expense.
            {
                string EntityType = entities.Find(x => x.EntityID == entityid).EntityType.Trim();
                if (EntityType == "01" | EntityType == "10")  // Test to see if residential or Mini Storage
                    Group = "640300000000";   // Residential Property manager.
                else
                    Group = "640400000000";  // Commercial Property Manager.
            }
            return Group.Trim();
        }
        private void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                this.Wait();

                OpenFileDialog file = new OpenFileDialog()
                {
                    InitialDirectory = "C:\\",
                    Filter = "Excel files|*.xls;*.xlsx"
                };

                //allow the user to select a file
                file.ShowDialog();

                //ensure the user has selected a file
                if (file.FileName.Trim() != "")
                {
                    //refresh the form if a new spreadsheet was selected
                    RefreshForm();

                    bool WeekEndingFound = false;
                    bool PayWeekFound = false;
                    int spreadSheetCount = 0;
                    int rowCount = 0;
                    int cellCount = 0;
                    DataSet data = new DataSet();
                    DataTable locTblPayroll = new DataTable();

                    try
                    {
                        fileName = file.FileName.Trim();
                        excel = ExcelReaderFactory.CreateReader(File.Open(file.FileName, FileMode.Open, FileAccess.ReadWrite));
                    }
                    catch (IOException err)
                    {
                        MessageBox.Show("This file is already open. You cannot open a file that is already in open.",
                                        "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    data = excel.AsDataSet();
                    locTblPayroll = data.Tables[0].Clone();

                    foreach (DataRow row in data.Tables[0].Rows)
                    {
                        DataRow r;
                        string code = row[0].ToString().Trim();
                        string account = "";
                        double amount = 0D;
                        bool isAllocation = false;
                        bool isJobCost = false;

                        try
                        {
                            isAllocation = (from DataRow percentageRow in locTblAllocations.Rows
                                            where percentageRow["ALLOCCODE"].ToString().Trim() == code.Trim()
                                            select percentageRow).Count() >= 1 ? true : false;
                        }
                        catch (ArgumentNullException err)
                        {

                        }
                        catch (InvalidOperationException err)
                        {

                        }

                        try
                        {
                            isJobCost = IsValidJob(code.Trim().Split(','));
                        }
                        catch (ArgumentNullException err)
                        {

                        }
                        catch (InvalidOperationException err)
                        {

                        }

                        account = row[1].ToString().Trim();
                        double.TryParse(row[2].ToString().Trim(), out amount);

                        if (isAllocation)
                        {
                            string strSQL = $@"EXEC	[dbo].[HS_GetAllocation]
		                                            @amount = {Math.Round(amount, 2)},
		                                            @period = N'{DateTime.Now.Period()}',
		                                            @AllocCode = N'{code.Trim()}',
		                                            @RunForAmount = 1";

                            DataTable locTblAllocations = Form1.sqlTransactions.ExecuteQuery(strSQL.Trim());

                            foreach (DataRow allocationLine in locTblAllocations.Rows)
                            {
                                r = locTblPayroll.NewRow();

                                r[0] = $@"{allocationLine["ENTITYID"].ToString().Trim()}-00";
                                r[1] = account.Trim();
                                r[2] = Math.Round(double.Parse(allocationLine["Amount"].ToString().Trim()), 2);
                                r[3] = Math.Round(double.Parse(allocationLine["Amount"].ToString().Trim()), 2);

                                locTblPayroll.Rows.Add(r);
                            }
                        }
                        else
                        {
                            r = locTblPayroll.NewRow();

                            r[0] = code.Trim();
                            r[1] = account.Trim();
                            r[2] = Math.Round(amount, 2);
                            r[3] = Math.Round(amount, 2);

                            locTblPayroll.Rows.Add(r);
                        }

                        r = null;
                    }

                    //create the datatable for invoice errors
                    DataTable locTblErrors = new DataTable();

                    locTblErrors.Columns.Add("Detail");
                    locTblErrors.Columns.Add("Error");

                    rowCount = 0;
                    string currentAccount = "";
                    string currentDepartment = "";
                    string currentProperty = "";
                    string currentLedgerCode = "";
                    GeneralLedgerAccountLine generalLedgerAccountLine = null;
                    GeneralLedgerAccountTotal generalLedgerAccountTotal = new GeneralLedgerAccountTotal();
                    GeneralLedgerDepartmentTotal generalLedgerDepartmentTotal = new GeneralLedgerDepartmentTotal();
                    GeneralLedgerPropertyTotal generalLedgerPropertyTotal = new GeneralLedgerPropertyTotal();
                    //go through each row in the spreadsheet
                    foreach (DataRow row in locTblPayroll.Rows)
                    {
                        cellCount = 0;
                        //go through each column in the row
                        foreach (object value in row.ItemArray)
                        {
                            //if the cell is not null or contain 'total' in the cell
                            if (value != DBNull.Value && value != string.Empty && !value.ToString().ToUpper().Contains("TOTAL"))
                            {
                                if (rowCount == 0 & cellCount == 0 & currentName.Trim() == "")
                                {
                                    generalLedgerPropertyTotal.Name = value.ToString().Trim();
                                    currentName = value.ToString().Trim();
                                }

                                //find the cell for the pay week ending
                                if (value.ToString().Trim().ToUpper().Contains("WEEK ENDING"))
                                {
                                    //split the pay week ending into 2 parts because it is formatted WEEK ENDING:[value]
                                    string[] payWeekEndingParts = value.ToString().Split(':');

                                    //if the field is not 2 parts, it is not formmatted properly
                                    if (payWeekEndingParts.Length != 2)
                                    {
                                        generalLedgerPropertyTotal.Errors.Add(string.Format("The Pay Week Ending Date for general ledger {0} is not formatted" +
                                                                                 " properly on row {1}. It must entered as 'Week Ending:[Date].",
                                                                                 generalLedgerPropertyTotal.Name.Trim(), rowCount + 1));
                                    }
                                    else if (payWeekEndingParts.Length == 2)
                                    {
                                        //try to parse the second part as a date time
                                        if (DateTime.TryParse(payWeekEndingParts[1], out currentPayWeekEnding))
                                        {
                                            WeekEndingFound = true;
                                            generalLedgerPropertyTotal.PayWeekEnding = currentPayWeekEnding;
                                        }//if it is not a date time, it is not formatted properly
                                        else
                                        {
                                            generalLedgerPropertyTotal.Errors.Add(string.Format("The Pay Week Ending Date for general ledger {0} is not" +
                                                                                     " an date on row {1}. It must be a date.",
                                                                                     generalLedgerPropertyTotal.Name.Trim(), rowCount + 1));
                                        }
                                    }
                                }

                                //find the cell for the pay date
                                if (value.ToString().Trim().ToUpper().Contains("PAY DATE"))
                                {
                                    //split the pay date into 2 parts because it is formatted PAY DATE:[value]
                                    string[] payDateParts = value.ToString().Split(':');

                                    //if the field is not 2 parts, it is not formmatted properly
                                    if (payDateParts.Length != 2)
                                    {
                                        generalLedgerPropertyTotal.Errors.Add(string.Format("The Pay Date for general ledger {0} is not formatted" +
                                                                                 " properly on row {1}. It must entered as 'Pay Date:[Date].",
                                                                                 generalLedgerPropertyTotal.Name.Trim(), rowCount + 1));
                                    }
                                    else if (payDateParts.Length == 2)
                                    {
                                        //try to parse the second part as a date time
                                        if (DateTime.TryParse(payDateParts[1], out currentPayDate))
                                        {
                                            PayWeekFound = true;
                                            generalLedgerPropertyTotal.PayDate = currentPayDate;
                                        }//if it is not a date time, it is not formatted properly
                                        else
                                        {
                                            generalLedgerPropertyTotal.Errors.Add(string.Format("The Pay Date for general ledger {0} is not" +
                                                                                     " an date on row {1}. It must be a date.",
                                                                                     generalLedgerPropertyTotal.Name.Trim(), rowCount + 1));
                                        }
                                    }
                                }

                                //start going through the data at the point the records begin
                                if (rowCount >= 5)
                                {

                                    //check the first column for the property and department 
                                    if (cellCount == 0)
                                    {
                                        //check if the property columns has job cost parts instead
                                        string[] propertyDeptParts;
                                        propertyDeptParts = value.ToString().Trim().Split(',');

                                        generalLedgerAccountLine = new GeneralLedgerAccountLine();

                                        //there are 4 parts to a job
                                        if (propertyDeptParts.Length == 4)
                                        {
                                            if (!IsValidJob(propertyDeptParts))
                                            {
                                                generalLedgerPropertyTotal.Errors.Add(string.Format("The Job is not formatted properly on" +
                                                    " line {0}. It must be formatted [Job Code]-[Phase Code]-[Cost List]-[Cost Code].", rowCount + 1));
                                            }
                                            else
                                            {
                                                string[] jobCodeParts = value.ToString().Trim().Split(',');

                                                currentDepartment = GetJobDepartment(jobCodeParts);
                                                currentProperty = GetJobEntityId(jobCodeParts);
                                                generalLedgerDepartmentTotal.GeneralLedgerAccountTotals.Add(generalLedgerAccountTotal);
                                                generalLedgerPropertyTotal.GeneralLedgerDepartmentTotals.Add(generalLedgerDepartmentTotal);
                                                generalLedgerPropertyTotals.Add(generalLedgerPropertyTotal);
                                                generalLedgerAccountTotal = new GeneralLedgerAccountTotal();
                                                generalLedgerPropertyTotal = new GeneralLedgerPropertyTotal();
                                                generalLedgerDepartmentTotal = new GeneralLedgerDepartmentTotal();

                                                generalLedgerPropertyTotal.Name = GetJobName(value.ToString().Trim().Split(','));
                                                generalLedgerPropertyTotal.PayDate = currentPayDate;
                                                generalLedgerPropertyTotal.PayWeekEnding = currentPayWeekEnding;
                                                generalLedgerPropertyTotal.JobCode = jobCodeParts[0].Trim();
                                                generalLedgerPropertyTotal.PhaseCode = jobCodeParts[1].Trim();
                                                generalLedgerPropertyTotal.CostList = jobCodeParts[2].Trim();
                                                generalLedgerPropertyTotal.CostCode = jobCodeParts[3].Trim();
                                                generalLedgerPropertyTotal.Property = currentProperty;
                                                generalLedgerDepartmentTotal.Department = currentDepartment;
                                                generalLedgerAccountTotal.AccountNumber = GetJobAccount(jobCodeParts);
                                            }
                                        }
                                        else
                                        {//follow the original rules
                                            propertyDeptParts = value.ToString().Trim().Split('-');

                                            if (propertyDeptParts.Length != 2)
                                            {
                                                generalLedgerPropertyTotal.Errors.Add(string.Format("The Property and/or department is not formatted properly on" +
                                                                                      " line {0}. It must be formatted [Property]-[Department].", rowCount + 1));
                                            }
                                            else
                                            {
                                                if (!IsValidProperty(propertyDeptParts[0].Trim()))
                                                {
                                                    generalLedgerAccountLine.Errors.Add(string.Format("The property on line {0} is not a valid property.", rowCount + 1));
                                                }
                                                else if (propertyDeptParts[1] != "00" && !IsValidDepartment(propertyDeptParts[1].Trim()))
                                                {
                                                    generalLedgerAccountLine.Errors.Add(string.Format("The department on line {0} is not a valid property.", rowCount + 1));
                                                }
                                                if (currentProperty == "")
                                                {
                                                    currentDepartment = propertyDeptParts[1].Trim();
                                                    currentProperty = propertyDeptParts[0].Trim();
                                                    generalLedgerPropertyTotal.Property = propertyDeptParts[0].Trim();
                                                    generalLedgerDepartmentTotal.Department = propertyDeptParts[1].Trim();
                                                }
                                                else if (propertyDeptParts[0].Trim() != currentProperty.Trim())
                                                {
                                                    currentDepartment = propertyDeptParts[1].Trim();
                                                    currentProperty = propertyDeptParts[0].Trim();
                                                    generalLedgerDepartmentTotal.GeneralLedgerAccountTotals.Add(generalLedgerAccountTotal);
                                                    generalLedgerPropertyTotal.GeneralLedgerDepartmentTotals.Add(generalLedgerDepartmentTotal);
                                                    generalLedgerPropertyTotals.Add(generalLedgerPropertyTotal);
                                                    generalLedgerAccountTotal = new GeneralLedgerAccountTotal();
                                                    generalLedgerPropertyTotal = new GeneralLedgerPropertyTotal();
                                                    generalLedgerDepartmentTotal = new GeneralLedgerDepartmentTotal();

                                                    generalLedgerPropertyTotal.Name = currentName.Trim();
                                                    generalLedgerPropertyTotal.PayDate = currentPayDate;
                                                    generalLedgerPropertyTotal.PayWeekEnding = currentPayWeekEnding;
                                                    generalLedgerPropertyTotal.Property = currentProperty.Trim();

                                                    generalLedgerDepartmentTotal.Department = currentDepartment.Trim();
                                                }
                                            }
                                        }
                                    }//check the column for the Account Number
                                    else if (cellCount == 1)
                                    {

                                        string ledgerCode = "";
                                        ledgerCode = "HS";

                                        if (currentProperty == "WCC")
                                        {
                                            ledgerCode = "WC";
                                        }
                                        else
                                        {
                                            ledgerCode = "HS";
                                        }


                                        //check the Ledger Code and Account Number is valid
                                        string Account = SetDistinctAccount(ledgerCode, value.ToString().Trim(), currentProperty);
                                        if (!IsValidAccount(ledgerCode, Account))
                                        {
                                            generalLedgerAccountLine.Errors.Add(string.Format("The account on line {0} is not valid.",
                                                                                rowCount + 1));
                                        }
                                        generalLedgerAccountLine.Property = currentProperty.Trim();
                                        generalLedgerAccountLine.Department = currentDepartment.Trim();
                                        generalLedgerAccountLine.LedgerCode = ledgerCode.Trim();
                                        generalLedgerAccountLine.Account = Account;
                                        //if the current Account Number is not equal to the row's Ledger Code and Account Number, 
                                        //it is new Ledger Code Account Number and needs to add the current one to the collection and instatiate new ones
                                        if (currentAccount != Account | currentLedgerCode != ledgerCode.Trim())
                                        {
                                            //add the current Account total to list of department Account totals
                                            generalLedgerDepartmentTotal.GeneralLedgerAccountTotals.Add(generalLedgerAccountTotal);
                                            //create a new Account Total
                                            generalLedgerAccountTotal = new GeneralLedgerAccountTotal();
                                            generalLedgerAccountTotal.LedgerCode = ledgerCode.Trim();
                                        }
                                        currentAccount = Account;
                                        currentLedgerCode = ledgerCode.Trim();
                                        generalLedgerAccountTotal.LedgerCode = currentLedgerCode;
                                        generalLedgerAccountTotal.AccountNumber = currentAccount;
                                    }//checks the column for the GLAmounts
                                    else if (cellCount == 2)
                                    {
                                        if (decimal.TryParse(value.ToString().Trim(), out decimal glAmount))
                                        {
                                            generalLedgerAccountLine.GLAmount = Math.Round(glAmount, 2);
                                        }
                                        else
                                        {
                                            generalLedgerAccountLine.Errors.Add(string.Format("The GL Amount on line {0} is not a number. It must be a number.", rowCount + 1));
                                        }

                                        generalLedgerAccountTotal.GeneralLedgerLines.Add(generalLedgerAccountLine);
                                    }//checks the column for the DeptAmount
                                    else if (cellCount == 3)
                                    {
                                        if (decimal.TryParse(value.ToString().Trim(), out decimal deptAmount))
                                        {
                                            generalLedgerAccountLine.DeptAmount = Math.Round(deptAmount, 2);
                                        }
                                        else
                                        {
                                            generalLedgerAccountLine.Errors.Add(string.Format("The Department Amount on line {0} is not a number. It must be a number.", rowCount + 1));
                                        }
                                    }
                                }
                                cellCount++;
                            }
                            else if (rowCount == 0 & cellCount == 0 & value == DBNull.Value)
                            {
                                DataRow rec;

                                rec = locTblErrors.NewRow();
                                rec["Detail"] = "There is not title in the first cell of the first row.";
                                rec["Error"] = "There needs to be a title in the first cell of the first row. It cannot exceed 29 characters.";

                                locTblErrors.Rows.Add(rec);
                            }
                            else if (value.ToString().ToUpper().Contains("TOTAL"))
                            {
                                rowCount++;
                                break;
                            }
                        }

                        rowCount++;
                    }

                    generalLedgerDepartmentTotal.GeneralLedgerAccountTotals.Add(generalLedgerAccountTotal);
                    generalLedgerPropertyTotal.GeneralLedgerDepartmentTotals.Add(generalLedgerDepartmentTotal);
                    generalLedgerPropertyTotals.Add(generalLedgerPropertyTotal);

                    spreadSheetCount++;

                    //close the excel spreadsheet so that it can be used by other programs
                    excel.Close();

                    DataTable gridData = new DataTable();

                    gridData.Columns.Add("Property");
                    gridData.Columns.Add("Department");
                    gridData.Columns.Add("Account");
                    gridData.Columns.Add("Credit");
                    gridData.Columns.Add("Debit");

                    //display the records for entites not 502 and 451
                    decimal totalCredit = 0M;
                    decimal totalDebit = 0M;
                    decimal batchCredit = 0M;
                    decimal batchDebit = 0M;

                    //go through each invoice headers errors and add to the invoice errors datatable
                    foreach (GeneralLedgerPropertyTotal generalLedgerProperty in generalLedgerPropertyTotals.
                                                                                 Where(x => x.GeneralLedgerDepartmentTotals.Count != 0 &
                                                                                            x.Property.Trim() != "502" & x.Property.Trim() != "451"))
                    {
                        DataRow row;
                        foreach (string errorProperty in generalLedgerProperty.Errors)
                        {
                            row = locTblErrors.NewRow();
                            row["Detail"] = string.Format("Property {0}", generalLedgerProperty.Property.Trim());
                            row["Error"] = errorProperty.Trim();

                            locTblErrors.Rows.Add(row);
                        }
                        foreach (GeneralLedgerDepartmentTotal innerGeneralLedgerDepartmentTotal in generalLedgerProperty.GeneralLedgerDepartmentTotals.
                                                                                              Where(x => x.GeneralLedgerAccountTotals.Count != 0))
                        {
                            DataRow rec;
                            foreach (string errorDepartment in innerGeneralLedgerDepartmentTotal.Errors)
                            {
                                row = locTblErrors.NewRow();
                                row["Detail"] = string.Format("Property {0} Department {1}", generalLedgerProperty.Property.Trim(),
                                                                                             innerGeneralLedgerDepartmentTotal.Department.Trim());
                                row["Error"] = errorDepartment.Trim();

                                locTblErrors.Rows.Add(row);
                            }
                            foreach (GeneralLedgerAccountTotal innerGeneralLedgerAccountTotal in innerGeneralLedgerDepartmentTotal.GeneralLedgerAccountTotals.
                                                                  Where(x => x.GeneralLedgerLines.Count != 0))
                            {
                                foreach (string errorAccount in innerGeneralLedgerAccountTotal.Errors)
                                {
                                    row = locTblErrors.NewRow();
                                    row["Detail"] = string.Format("Property {0} Department {1} Account {2}", generalLedgerProperty.Property.Trim(),
                                                                                                             innerGeneralLedgerDepartmentTotal.Department.Trim(),
                                                                                                             innerGeneralLedgerAccountTotal.Account.Trim());
                                    row["Error"] = errorAccount.Trim();

                                    locTblErrors.Rows.Add(row);
                                }
                                int x = 0;
                                foreach (GeneralLedgerAccountLine innerGeneralLedgerAccountLine in innerGeneralLedgerAccountTotal.GeneralLedgerLines)
                                {
                                    foreach (string errorAccountLine in innerGeneralLedgerAccountLine.Errors)
                                    {
                                        row = locTblErrors.NewRow();
                                        row["Detail"] = string.Format("Property {0} Department {1} Account {2} down {3} line(s).", innerGeneralLedgerAccountLine.Property.Trim(),
                                                                                                                                   innerGeneralLedgerAccountLine.Department.Trim(),
                                                                                                                                   innerGeneralLedgerAccountLine.Account.Trim(),
                                                                                                                                   x);
                                        row["Error"] = errorAccountLine.Trim();

                                        locTblErrors.Rows.Add(row);
                                    }


                                    x++;
                                }

                                batchCredit += innerGeneralLedgerAccountTotal.Total;

                                rec = gridData.NewRow();
                                if (generalLedgerPropertyTotal.IsJobCode)
                                {
                                    rec["Property"] = string.Format("{0}-{1}-{2}-{3}", generalLedgerPropertyTotal.JobCode.Trim(),
                                                                                       generalLedgerPropertyTotal.PhaseCode.Trim(),
                                                                                       generalLedgerPropertyTotal.CostList.Trim(),
                                                                                       generalLedgerPropertyTotal.CostCode.Trim());
                                    rec["Department"] = "";
                                    rec["Account"] = "";
                                }
                                else
                                {
                                    rec["Property"] = generalLedgerProperty.Property.Trim();
                                    rec["Department"] = innerGeneralLedgerDepartmentTotal.Department.Trim();
                                    rec["Account"] = innerGeneralLedgerAccountTotal.Account.Trim();
                                }
                                rec["Credit"] = innerGeneralLedgerAccountTotal.Total;
                                rec["Debit"] = 0;

                                gridData.Rows.Add(rec);
                            }
                            batchDebit += innerGeneralLedgerDepartmentTotal.Total;

                            rec = gridData.NewRow();
                            if (generalLedgerPropertyTotal.IsJobCode)
                            {
                                rec["Property"] = string.Format("{0}-{1}-{2}-{3}", generalLedgerPropertyTotal.JobCode.Trim(),
                                                                                   generalLedgerPropertyTotal.PhaseCode.Trim(),
                                                                                   generalLedgerPropertyTotal.CostList.Trim(),
                                                                                   generalLedgerPropertyTotal.CostCode.Trim());
                                rec["Department"] = "";
                                rec["Account"] = "";
                            }
                            else
                            {
                                rec["Property"] = generalLedgerProperty.Property.Trim();
                                rec["Department"] = innerGeneralLedgerDepartmentTotal.Department.Trim();
                                rec["Account"] = "HS360100000000";
                            }
                            rec["Credit"] = 0;
                            rec["Debit"] = innerGeneralLedgerDepartmentTotal.Total;

                            gridData.Rows.Add(rec);
                        }
                    }

                    DataRow lastRec = gridData.NewRow();
                    lastRec["Property"] = "";
                    lastRec["Department"] = "";
                    lastRec["Account"] = "Total: ";
                    lastRec["Credit"] = batchCredit;
                    lastRec["Debit"] = batchDebit;

                    gridData.Rows.Add(lastRec);

                    totalCredit += batchCredit;
                    totalDebit += batchDebit;

                    batchCredit = 0;
                    batchDebit = 0;

                    //go through each invoice headers errors and add to the invoice errors datatable
                    foreach (GeneralLedgerPropertyTotal generalLedgerProperty in generalLedgerPropertyTotals.
                                                                                 Where(x => x.GeneralLedgerDepartmentTotals.Count != 0 &
                                                                                            (x.Property.Trim() == "502" | x.Property.Trim() == "451")))
                    {
                        DataRow row;
                        foreach (string errorProperty in generalLedgerProperty.Errors)
                        {
                            row = locTblErrors.NewRow();
                            row["Detail"] = string.Format("Property {0}", generalLedgerProperty.Property.Trim());
                            row["Error"] = errorProperty.Trim();

                            locTblErrors.Rows.Add(row);
                        }
                        foreach (GeneralLedgerDepartmentTotal innerGeneralLedgerDepartmentTotal in generalLedgerProperty.GeneralLedgerDepartmentTotals.
                                                                                              Where(x => x.GeneralLedgerAccountTotals.Count != 0))
                        {
                            DataRow rec;
                            foreach (string errorDepartment in innerGeneralLedgerDepartmentTotal.Errors)
                            {
                                row = locTblErrors.NewRow();
                                row["Detail"] = string.Format("Property {0} Department {1}", generalLedgerProperty.Property.Trim(),
                                                                                             innerGeneralLedgerDepartmentTotal.Department.Trim());
                                row["Error"] = errorDepartment.Trim();

                                locTblErrors.Rows.Add(row);
                            }
                            foreach (GeneralLedgerAccountTotal innerGeneralLedgerAccountTotal in innerGeneralLedgerDepartmentTotal.GeneralLedgerAccountTotals.
                                                                  Where(x => x.GeneralLedgerLines.Count != 0))
                            {
                                foreach (string errorAccount in innerGeneralLedgerAccountTotal.Errors)
                                {
                                    row = locTblErrors.NewRow();
                                    row["Detail"] = string.Format("Property {0} Department {1} Account {2}", generalLedgerProperty.Property.Trim(),
                                                                                                             innerGeneralLedgerDepartmentTotal.Department.Trim(),
                                                                                                             innerGeneralLedgerAccountTotal.Account.Trim());
                                    row["Error"] = errorAccount.Trim();

                                    locTblErrors.Rows.Add(row);
                                }
                                int x = 0;
                                foreach (GeneralLedgerAccountLine innerGeneralLedgerAccountLine in innerGeneralLedgerAccountTotal.GeneralLedgerLines)
                                {
                                    foreach (string errorAccountLine in innerGeneralLedgerAccountLine.Errors)
                                    {
                                        row = locTblErrors.NewRow();
                                        row["Detail"] = string.Format("Property {0} Department {1} Account {2} down {3} line(s).", innerGeneralLedgerAccountLine.Property.Trim(),
                                                                                                                                   innerGeneralLedgerAccountLine.Department.Trim(),
                                                                                                                                   innerGeneralLedgerAccountLine.Account.Trim(),
                                                                                                                                   x);
                                        row["Error"] = errorAccountLine.Trim();

                                        locTblErrors.Rows.Add(row);
                                    }


                                    x++;
                                }

                                batchCredit += innerGeneralLedgerAccountTotal.Total;

                                rec = gridData.NewRow();
                                if (generalLedgerPropertyTotal.IsJobCode)
                                {
                                    rec["Property"] = string.Format("{0}-{1}-{2}-{3}", generalLedgerPropertyTotal.JobCode.Trim(),
                                                                                       generalLedgerPropertyTotal.PhaseCode.Trim(),
                                                                                       generalLedgerPropertyTotal.CostList.Trim(),
                                                                                       generalLedgerPropertyTotal.CostCode.Trim());
                                    rec["Department"] = "";
                                    rec["Account"] = "";
                                }
                                else
                                {
                                    rec["Property"] = generalLedgerProperty.Property.Trim();
                                    rec["Department"] = innerGeneralLedgerDepartmentTotal.Department.Trim();
                                    rec["Account"] = innerGeneralLedgerAccountTotal.Account.Trim();
                                }
                                rec["Credit"] = innerGeneralLedgerAccountTotal.Total;
                                rec["Debit"] = 0;

                                gridData.Rows.Add(rec);
                            }
                            batchDebit += innerGeneralLedgerDepartmentTotal.Total;

                            rec = gridData.NewRow();
                            if (generalLedgerPropertyTotal.IsJobCode)
                            {
                                rec["Property"] = string.Format("{0}-{1}-{2}-{3}", generalLedgerPropertyTotal.JobCode.Trim(),
                                                                                   generalLedgerPropertyTotal.PhaseCode.Trim(),
                                                                                   generalLedgerPropertyTotal.CostList.Trim(),
                                                                                   generalLedgerPropertyTotal.CostCode.Trim());
                                rec["Department"] = "";
                                rec["Account"] = "";
                            }
                            else
                            {
                                rec["Property"] = generalLedgerProperty.Property.Trim();
                                rec["Department"] = innerGeneralLedgerDepartmentTotal.Department.Trim();
                                rec["Account"] = "HS360100000000";
                            }
                            rec["Credit"] = 0;
                            rec["Debit"] = innerGeneralLedgerDepartmentTotal.Total;

                            gridData.Rows.Add(rec);
                        }
                    }

                    //if the sheet is missing the Week Ending date
                    if (!WeekEndingFound)
                    {
                        DataRow lastError = locTblErrors.NewRow();
                        lastError["Detail"] = "Missing Week Ending.";
                        lastError["Error"] = "Missing Week Ending date from the sheet.";

                        locTblErrors.Rows.Add(lastError);
                    }

                    //if the sheet is missing the Pay Week date
                    if (!PayWeekFound)
                    {
                        DataRow lastError = locTblErrors.NewRow();
                        lastError["Detail"] = "Pay Week Ending.";
                        lastError["Error"] = "Pay Week Ending date from the sheet.";

                        locTblErrors.Rows.Add(lastError);
                    }

                    //if the Debits and Credits do not match
                    if (totalDebit != totalCredit)
                    {
                        DataRow lastError = locTblErrors.NewRow();
                        lastError["Detail"] = string.Format("Debit total {0} does not equal the Credit total {1}.", totalDebit.ToString("c2"), totalCredit.ToString("c2"));
                        lastError["Error"] = "The Debits and Credits do not match.";

                        locTblErrors.Rows.Add(lastError);
                    }

                    lastRec = gridData.NewRow();
                    lastRec["Property"] = "";
                    lastRec["Department"] = "";
                    lastRec["Account"] = "Total: ";
                    lastRec["Credit"] = batchCredit;
                    lastRec["Debit"] = batchDebit;

                    gridData.Rows.Add(lastRec);

                    totalCredit += batchCredit;
                    totalDebit += batchDebit;

                    lastRec = gridData.NewRow();
                    lastRec["Property"] = "";
                    lastRec["Department"] = "";
                    lastRec["Account"] = "Grand Total: ";
                    lastRec["Credit"] = totalCredit;
                    lastRec["Debit"] = totalDebit;

                    gridData.Rows.Add(lastRec);

                    batchCredit = 0;
                    batchDebit = 0;

                    //if there is at least one invoice with no errors and has at least one line, enable the approve button to allow the invoices to be sent to MRI
                    if (generalLedgerPropertyTotals.Where(x => x.HasErrors).Count() == 0 & WeekEndingFound & PayWeekFound & (totalDebit == totalCredit))
                    {
                        btnApprove.Enabled = true;
                    }

                    //set the invoice errors datatable to the grid
                    dgvErrors.DataSource = locTblErrors;

                    //set the invoice datatable to the grid
                    dgvData.DataSource = gridData;

                    FormatHeader();

                    dgvErrors.AutoResizeColumns();

                    dgvData.AutoResizeColumns();

                    data.Clear();
                    data.Dispose();
                }

                file.Dispose();
                file = null;
            }
            catch (IOException err)
            {
                MessageBox.Show("This file is already open. You cannot open a file that is already in open.",
                                "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception err)
            {
                if ((excel != null) && (!excel.IsClosed))
                {
                    excel.Close();
                }
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
            finally
            {
                if ((excel != null) && (!excel.IsClosed))
                {
                    excel.Close();
                }
                this.Useable();
            }
        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            RefreshForm();
        }

        private void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                //check if the file is use before the data is 
                try
                {
                    File.Open(fileName.Trim(), FileMode.Open, FileAccess.ReadWrite).Close();
                }
                catch (IOException err)
                {
                    MessageBox.Show("This file is already open. You cannot open a file that is already in open.",
                                    "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                this.Wait();

                string source = "";

                if (generalLedgerPropertyTotals[0].Property != "WCC")
                {
                    source = "GJ";
                }
                else
                {
                    source = "GW";
                }

                //create batch for entities not 502 and 451
                Form1.sqlTransactions.Dispose();

                Form1.sqlTransactions = new SqlDataAccess.SqlTransactions(Properties.Settings.Default.sqlConnection.Trim());

                Form1.sqlTransactions.BeginTransaction(IsolationLevel.ReadUncommitted);

                string refNum = "000000";
                string firstRefNum = "";
                string secondRefNum = "";
                Dictionary<string, string> insertValues = new Dictionary<string, string>();
                DataTable locTbl = new DataTable();

                //call the stored procedure to get the next ref number and store the output
                locTbl = Form1.sqlTransactions.ExecuteQuery($@"exec dbo.HS_GetNextRefNum '{currentPayDate.Period()}', '{source.Trim()}', 0, 100");
                //if a record is returned
                if (locTbl.Rows.Count > 0)
                {
                    refNum = locTbl.Rows[0][0].ToString().Trim();
                    firstRefNum = refNum.Trim();
                }

                insertValues.Add("period", currentPayDate.Period());
                insertValues.Add("ref", refNum.Trim());
                insertValues.Add("source", source);
                insertValues.Add("siteid", "@");
                insertValues.Add("actiontype", "CREATE");
                insertValues.Add("lastdate", DateTime.Now.ToString());
                insertValues.Add("userid", HelpfulFunctions.UserID.Trim());

                Form1.sqlTransactions.InsertTableData("GL_Journal_History", insertValues);

                insertValues = new Dictionary<string, string>();

                insertValues.Add("glsource", source);
                insertValues.Add("glref", refNum.Trim());
                insertValues.Add("period", currentPayDate.Period());
                insertValues.Add("basis", "C");
                insertValues.Add("batchamt", generalLedgerPropertyTotals.Where(x => x.Property.Trim() != "502" & x.Property.Trim() != "451")
                                                                               .Sum(x => x.Total).ToString().Trim());
                insertValues.Add("release", "N");
                insertValues.Add("sent", "N");
                insertValues.Add("descr", $"PAYROLL BI-WEEK ENDING {currentPayWeekEnding.ToString("MM/dd/yy")}");
                insertValues.Add("USERID", HelpfulFunctions.UserID.Trim());

                Form1.sqlTransactions.InsertTableData("HS_GLBNCTL", insertValues);

                insertValues = new Dictionary<string, string>();

                int i = 1;
                foreach (GeneralLedgerPropertyTotal generalLedgerPropertyTotal in
                         generalLedgerPropertyTotals.FindAll(x => x.Property.Trim() != "502" & x.Property.Trim() != "451"))
                {
                    //set all the '00' departments to '@'
                    generalLedgerPropertyTotal.GeneralLedgerDepartmentTotals.FindAll(x => x.Department == "00").ForEach(x => x.Department = "@");

                    foreach (GeneralLedgerDepartmentTotal generalLedgerDepartmentTotal in generalLedgerPropertyTotal.GeneralLedgerDepartmentTotals)
                    {
                        //remove the extra account
                        generalLedgerDepartmentTotal.GeneralLedgerAccountTotals.RemoveAll(x => x.AccountNumber == "");

                        foreach (GeneralLedgerAccountTotal generalLedgerAccountTotal in generalLedgerDepartmentTotal.GeneralLedgerAccountTotals)
                        {
                            if (generalLedgerAccountTotal.Total != 0M)
                            {
                                insertValues = new Dictionary<string, string>();

                                insertValues.Add("period", generalLedgerPropertyTotal.PayDate.Period());
                                insertValues.Add("ref", refNum.Trim());
                                insertValues.Add("source", source);
                                insertValues.Add("siteid", "@");
                                insertValues.Add("item", i.ToString().Trim());
                                if (generalLedgerPropertyTotal.IsJobCode)
                                {
                                    insertValues.Add("JOBCODE", generalLedgerPropertyTotal.JobCode.Trim());
                                    insertValues.Add("JC_PHASECODE", generalLedgerPropertyTotal.PhaseCode.Trim());
                                    insertValues.Add("JC_COSTLIST", generalLedgerPropertyTotal.CostList.Trim());
                                    insertValues.Add("JC_COSTCODE", generalLedgerPropertyTotal.CostCode.Trim());
                                }
                                insertValues.Add("entityid", generalLedgerPropertyTotal.Property);
                                insertValues.Add("acctnum", generalLedgerAccountTotal.Account);

                                if (int.Parse(generalLedgerAccountTotal.Account.Substring(2, 6)) >= 499999)
                                {
                                    insertValues.Add("department", generalLedgerDepartmentTotal.Department.Trim());
                                }
                                else
                                {
                                    insertValues.Add("department", "@");
                                }
                                insertValues.Add("amt", generalLedgerAccountTotal.Total.ToString().Trim());
                                insertValues.Add("entrdate", DateTime.Now.ToString().Trim());
                                insertValues.Add("reversal", "N");
                                insertValues.Add("status", "P");
                                insertValues.Add("userid", HelpfulFunctions.UserID.Trim());
                                insertValues.Add("basis", "C");
                                insertValues.Add("lastdate", DateTime.Now.ToString().Trim());
                                insertValues.Add("HS_JVNO", "71");
                                insertValues.Add("DESCRPN", $"PAYROLL BI-WEEK ENDING {currentPayWeekEnding.ToString("MM/dd/yy")}");

                                Form1.sqlTransactions.InsertTableData("JOURNAL", insertValues);

                                i++;
                            }
                        }

                        if (generalLedgerDepartmentTotal.Total != 0M)
                        {
                            insertValues = new Dictionary<string, string>();

                            insertValues.Add("period", generalLedgerPropertyTotal.PayDate.Period());
                            insertValues.Add("ref", refNum.Trim());
                            insertValues.Add("source", source);
                            insertValues.Add("siteid", "@");
                            if (generalLedgerPropertyTotal.IsJobCode)
                            {
                                insertValues.Add("JOBCODE", generalLedgerPropertyTotal.JobCode.Trim());
                                insertValues.Add("JC_PHASECODE", generalLedgerPropertyTotal.PhaseCode.Trim());
                                insertValues.Add("JC_COSTLIST", generalLedgerPropertyTotal.CostList.Trim());
                                insertValues.Add("JC_COSTCODE", generalLedgerPropertyTotal.CostCode.Trim());
                            }
                            insertValues.Add("entityid", generalLedgerPropertyTotal.Property);
                            if (source.Trim() == "GW")
                            {
                                insertValues.Add("acctnum", "WC360100000000");
                            }
                            else
                            {
                                insertValues.Add("acctnum", "HS360100000000");
                            }
                            insertValues.Add("department", "@");
                            insertValues.Add("amt", (-1 * generalLedgerDepartmentTotal.Total).ToString().Trim());
                            insertValues.Add("entrdate", DateTime.Now.ToString().Trim());
                            insertValues.Add("reversal", "N");
                            insertValues.Add("status", "P");
                            insertValues.Add("userid", HelpfulFunctions.UserID.Trim());
                            insertValues.Add("basis", "C");
                            insertValues.Add("item", i.ToString().Trim());
                            insertValues.Add("lastdate", DateTime.Now.ToString().Trim());
                            insertValues.Add("HS_JVNO", "71");
                            insertValues.Add("DESCRPN", $"PAYROLL BI-WEEK ENDING {currentPayWeekEnding.ToString("MM/dd/yy")}");

                            Form1.sqlTransactions.InsertTableData("JOURNAL", insertValues);

                            i++;
                        }
                    }
                }

                if (generalLedgerPropertyTotals.FindAll(x => x.Property.Trim() == "502" | x.Property.Trim() == "451").Count > 0)
                {
                    //call the stored procedure to get the next ref number and store the output
                    locTbl = Form1.sqlTransactions.ExecuteQuery($@"exec dbo.HS_GetNextRefNum '{currentPayDate.Period()}', '{source.Trim()}', 0, 100");
                    //if a record is returned
                    if (locTbl.Rows.Count > 0)
                    {
                        refNum = locTbl.Rows[0][0].ToString().Trim();
                        secondRefNum = refNum.Trim();
                    }

                    insertValues = new Dictionary<string, string>();

                    insertValues.Add("period", currentPayDate.Period());
                    insertValues.Add("ref", refNum.Trim());
                    insertValues.Add("source", source);
                    insertValues.Add("siteid", "@");
                    insertValues.Add("actiontype", "CREATE");
                    insertValues.Add("lastdate", DateTime.Now.ToString());
                    insertValues.Add("userid", HelpfulFunctions.UserID.Trim());

                    Form1.sqlTransactions.InsertTableData("GL_Journal_History", insertValues);

                    insertValues = new Dictionary<string, string>();

                    insertValues.Add("glsource", source);
                    insertValues.Add("glref", refNum.Trim());
                    insertValues.Add("period", currentPayDate.Period());
                    insertValues.Add("basis", "C");
                    insertValues.Add("batchamt", generalLedgerPropertyTotals.Where(x => x.Property.Trim() == "502" | x.Property.Trim() == "451")
                                                                                   .Sum(x => x.Total).ToString().Trim());
                    insertValues.Add("release", "I");
                    insertValues.Add("sent", "N");
                    insertValues.Add("descr", $"PAYROLL BI-WEEK ENDING {currentPayWeekEnding.ToString("MM/dd/yy")}");
                    insertValues.Add("USERID", HelpfulFunctions.UserID.Trim());

                    Form1.sqlTransactions.InsertTableData("HS_GLBNCTL", insertValues);

                    insertValues = new Dictionary<string, string>();

                    i = 1;
                    foreach (GeneralLedgerPropertyTotal generalLedgerPropertyTotal in
                             generalLedgerPropertyTotals.FindAll(x => x.Property.Trim() == "502" | x.Property.Trim() == "451"))
                    {
                        //set all the '00' departments to '@'
                        generalLedgerPropertyTotal.GeneralLedgerDepartmentTotals.FindAll(x => x.Department == "00").ForEach(x => x.Department = "@");

                        foreach (GeneralLedgerDepartmentTotal generalLedgerDepartmentTotal in generalLedgerPropertyTotal.GeneralLedgerDepartmentTotals)
                        {
                            //remove the extra account
                            generalLedgerDepartmentTotal.GeneralLedgerAccountTotals.RemoveAll(x => x.AccountNumber == "");

                            foreach (GeneralLedgerAccountTotal generalLedgerAccountTotal in generalLedgerDepartmentTotal.GeneralLedgerAccountTotals)
                            {
                                if (generalLedgerAccountTotal.Total != 0M)
                                {
                                    insertValues = new Dictionary<string, string>();

                                    insertValues.Add("period", generalLedgerPropertyTotal.PayDate.Period());
                                    insertValues.Add("ref", refNum.Trim());
                                    insertValues.Add("source", source);
                                    insertValues.Add("siteid", "@");
                                    insertValues.Add("item", i.ToString().Trim());
                                    if (generalLedgerPropertyTotal.IsJobCode)
                                    {
                                        insertValues.Add("JOBCODE", generalLedgerPropertyTotal.JobCode.Trim());
                                        insertValues.Add("JC_PHASECODE", generalLedgerPropertyTotal.PhaseCode.Trim());
                                        insertValues.Add("JC_COSTLIST", generalLedgerPropertyTotal.CostList.Trim());
                                        insertValues.Add("JC_COSTCODE", generalLedgerPropertyTotal.CostCode.Trim());
                                    }
                                    insertValues.Add("entityid", generalLedgerPropertyTotal.Property);
                                    insertValues.Add("acctnum", generalLedgerAccountTotal.Account);

                                    if (int.Parse(generalLedgerAccountTotal.Account.Substring(2, 6)) >= 499999)
                                    {
                                        insertValues.Add("department", generalLedgerDepartmentTotal.Department.Trim());
                                    }
                                    else
                                    {
                                        insertValues.Add("department", "@");
                                    }
                                    insertValues.Add("amt", generalLedgerAccountTotal.Total.ToString().Trim());
                                    insertValues.Add("entrdate", DateTime.Now.ToString().Trim());
                                    insertValues.Add("reversal", "N");
                                    insertValues.Add("status", "P");
                                    insertValues.Add("userid", HelpfulFunctions.UserID.Trim());
                                    insertValues.Add("basis", "C");
                                    insertValues.Add("lastdate", DateTime.Now.ToString().Trim());
                                    insertValues.Add("HS_JVNO", "71");
                                    insertValues.Add("DESCRPN", $"PAYROLL BI-WEEK ENDING {currentPayWeekEnding.ToString("MM/dd/yy")}");

                                    Form1.sqlTransactions.InsertTableData("JOURNAL", insertValues);

                                    i++;
                                }
                            }

                            if (generalLedgerDepartmentTotal.Total != 0M)
                            {
                                insertValues = new Dictionary<string, string>();

                                insertValues.Add("period", generalLedgerPropertyTotal.PayDate.Period());
                                insertValues.Add("ref", refNum.Trim());
                                insertValues.Add("source", source);
                                insertValues.Add("siteid", "@");
                                if (generalLedgerPropertyTotal.IsJobCode)
                                {
                                    insertValues.Add("JOBCODE", generalLedgerPropertyTotal.JobCode.Trim());
                                    insertValues.Add("JC_PHASECODE", generalLedgerPropertyTotal.PhaseCode.Trim());
                                    insertValues.Add("JC_COSTLIST", generalLedgerPropertyTotal.CostList.Trim());
                                    insertValues.Add("JC_COSTCODE", generalLedgerPropertyTotal.CostCode.Trim());
                                }
                                insertValues.Add("entityid", generalLedgerPropertyTotal.Property);
                                if (source.Trim() == "GW")
                                {
                                    insertValues.Add("acctnum", "WC360100000000");
                                }
                                else
                                {
                                    insertValues.Add("acctnum", "HS360100000000");
                                }
                                insertValues.Add("department", "@");
                                insertValues.Add("amt", (-1 * generalLedgerDepartmentTotal.Total).ToString().Trim());
                                insertValues.Add("entrdate", DateTime.Now.ToString().Trim());
                                insertValues.Add("reversal", "N");
                                insertValues.Add("status", "P");
                                insertValues.Add("userid", HelpfulFunctions.UserID.Trim());
                                insertValues.Add("basis", "C");
                                insertValues.Add("item", i.ToString().Trim());
                                insertValues.Add("lastdate", DateTime.Now.ToString().Trim());
                                insertValues.Add("HS_JVNO", "71");
                                insertValues.Add("DESCRPN", $"PAYROLL BI-WEEK ENDING {currentPayWeekEnding.ToString("MM/dd/yy")}");

                                Form1.sqlTransactions.InsertTableData("JOURNAL", insertValues);

                                i++;
                            }
                        }
                    }
                }

                Form1.sqlTransactions.CommitTransaction();

                MessageBox.Show(string.Format("The payroll was imported successfuly. It was added with to Journal with the reference(s) {0} {1}.", firstRefNum.Trim(), secondRefNum.Trim()),
                                "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                File.Move(fileName.Trim(), string.Format("{0} {1} {2}.xlsx", fileName.Trim().Replace(Path.GetExtension(fileName), ""), firstRefNum.Trim(), secondRefNum.Trim()));
                File.Delete(fileName.Trim());

                RefreshForm();
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);

                Form1.sqlTransactions.RollbackTransaction();
            }
            finally
            {
                Form1.sqlTransactions = new SqlDataAccess.SqlTransactions(Properties.Settings.Default.sqlConnection.Trim());
                Form1.sqlTransactions.Connect();

                this.Useable();
            }
        }

        private void frmGenLdgImp_Load(object sender, EventArgs e)
        {
            try
            {
                this.Wait();

                //formats the title bar to show the name and the databases selected
                this.Text = this.Text + " (MS SQL Server: " + Form1.sqlTransactions.GetDatabase().Trim() + ")";
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
                this.Close();
            }
            finally
            {
                this.Useable();
            }
        }

        private void dgvData_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                //if nothing in the cell then ignore formatting
                if (e.Value.ToString().Trim() != "")
                {
                    //if column is the Account column
                    if (e.ColumnIndex == dgvData.Columns["Account"].Index)
                    {
                        //if the specific exception is thrown, it because the account is not found
                        try
                        {
                            //get substring of the account if it long enough, other wise just write the value
                            e.Value = e.Value.ToString().Trim().Length >= 8 ?
                                e.Value.ToString().Trim().Substring(2, 6) + " - " +
                                accounts.Find(x => x.Accountnumber == e.Value.ToString().Trim()).Accountname.Trim() :
                                e.Value.ToString().Trim();
                        }
                        catch (NullReferenceException err)
                        {

                        }
                    }//if column is the Property column
                    else if (e.ColumnIndex == dgvData.Columns["Property"].Index)
                    {
                        //if the specific exception is thrown, it is because the entity is not found
                        try
                        {
                            e.Value = e.Value.ToString().Trim() + " - " + entities.Find(x => x.EntityID == e.Value.ToString().Trim()).EntityName.Trim();
                        }
                        catch (NullReferenceException err)
                        {

                        }
                    }//if the column is the Credit column
                    else if (e.ColumnIndex == dgvData.Columns["Credit"].Index)
                    {
                        //format the value as a currency
                        e.Value = decimal.Parse(e.Value.ToString()).ToString("c2");
                        e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    }//if the column is the debit column
                    else if (e.ColumnIndex == dgvData.Columns["Debit"].Index)
                    {
                        //format the column as a currency
                        e.Value = decimal.Parse(e.Value.ToString()).ToString("c2");
                        e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    }
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }
    }
}
