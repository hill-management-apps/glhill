﻿using HelpfulClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GLHill.Forms
{
    public partial class frmManagementFees : Form
    {
        #region "Fields"
        private const bool DisplayToUser = true;
        #endregion

        #region "Constructors"
        public frmManagementFees()
        {
            InitializeComponent();
        }
        #endregion

        #region "Methods"
        public string CalculateRefNum(string period, string source)
        {
            string retVal = string.Empty;

            try
            {
                string sql = string.Format("select max(ref) from (select max(ref) as ref from journal where source = '{0}' and period = '{1}' and ref not like '%[^0-9]%'", source.Trim(), period.Trim()) +
                    string.Format("union select max(ref) as ref from ghis where source = '{0}' and period = '{1}' and ref not like '%[^0-9]%') as refunion HAVING MAX(REF) <= '000099'", source.Trim(), period.Trim());

                DataTable locTbl = Form1.sqlTransactions.ExecuteQuery(sql);

                if (locTbl.Rows.Count < 1)
                {
                    retVal = "000001";
                }
                else
                {
                    // We have a value for max(ref)
                    int numericValue = int.Parse(locTbl.Rows[0][0].ToString());
                    numericValue++;

                    if (numericValue <= 99)
                    {
                        retVal = numericValue.ToString().PadLeft(6, '0');
                    }
                    else
                    {
                        int x;

                        for (x = 1; x < 99; x++)
                        {
                            sql = string.Format("select ref from (select ref from journal where source = '{0}' and period = '{1}' and ref not like '%[^0-9]%'", source.Trim(), period.Trim()) +
                            string.Format("union select ref from ghis where source = '{0}' and period = '{1}' and ref not like '%[^0-9]%') as refunion WHERE REF = '0000{2}'", source.Trim(), period.Trim(), x.ToString().PadLeft(2, '0'));

                            locTbl = Form1.sqlTransactions.ExecuteQuery(sql);

                            if (locTbl.Rows.Count == 0)
                            {
                                retVal = x.ToString().PadLeft(6, '0');
                                break;
                            }
                        }

                        if (x >= 99)
                        {
                            throw new Exception(string.Format("There are too many references created for this period. " +
                                " A reference number cannot be 99 or greater. The next one will be {0}.", numericValue));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return retVal;
        }

        /// <summary>
        /// Creates the value to select from in the dropdown list. It adds the current period formatted [yyyyMM] and goes back 2 periods.
        /// </summary>
        private void CreateDropDownList()
        {
            try
            {
                DataTable locTblPeriods = new DataTable();
                locTblPeriods.Columns.Add("DISPLAYTEXT");
                locTblPeriods.Columns.Add("VALUESELECTED");

                for (int i = -3; i < 0; i++)
                {
                    DataRow dr = locTblPeriods.NewRow();

                    dr["DISPLAYTEXT"] = DateTime.Now.AddMonths(i).ToString("MM/yy");
                    dr["VALUESELECTED"] = DateTime.Now.AddMonths(i).ToString("yyyyMM");

                    locTblPeriods.Rows.Add(dr);
                }

                cboEndingPeriod.DataSource = locTblPeriods;
                cboEndingPeriod.DisplayMember = "DISPLAYTEXT";
                cboEndingPeriod.ValueMember = "VALUESELECTED";
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void FilterGrid()
        {
            //goes through each transaction
            foreach (DataGridViewRow row in dgvManagementFees.Rows)
            {
                //if the fee due is null or less than or equal to 1 and override amount is null or less than or equal to 1, should be invisible
                if (((row.Cells["feeduemain"].Value == DBNull.Value || (decimal.Parse(row.Cells["feeduemain"].Value.ToString().Trim()) <= 1M & 
                       decimal.Parse(row.Cells["feeduemain"].Value.ToString().Trim()) >= -1M)) &&
                    (row.Cells["feeduerec"].Value == DBNull.Value || (decimal.Parse(row.Cells["feeduerec"].Value.ToString().Trim()) <= 1M &
                       decimal.Parse(row.Cells["feeduerec"].Value.ToString().Trim()) >= -1M)) &&
                    (row.Cells["feeduemgt"].Value == DBNull.Value || (decimal.Parse(row.Cells["feeduemgt"].Value.ToString().Trim()) <= 1M &
                       decimal.Parse(row.Cells["feeduemgt"].Value.ToString().Trim()) >= -1M)) &&
                       ((row.Cells["ovrAmt"].Value == DBNull.Value) || (decimal.Parse(row.Cells["ovrAmt"].Value.ToString().Trim()) <= 1M &
                       decimal.Parse(row.Cells["ovrAmt"].Value.ToString().Trim()) >= -1M))))
                {
                    CurrencyManager currencyManager1 = (CurrencyManager)BindingContext[dgvManagementFees.DataSource];
                    currencyManager1.SuspendBinding();
                    dgvManagementFees.Rows[row.Index].Visible = !chkHideUnpostableTransactions.Checked;
                    currencyManager1.ResumeBinding();
                }
            }

            int recordsMoreThanADollar = (from DataGridViewRow row in dgvManagementFees.Rows
                                          where ((row.Cells["OVRAMT"].Value != DBNull.Value &&
                                                 (decimal.Parse(row.Cells["OVRAMT"].Value.ToString().Trim()) >= 1 | decimal.Parse(row.Cells["OVRAMT"].Value.ToString().Trim()) <= -1)) |
                                                 (row.Cells["feeduemain"].Value != DBNull.Value &&
                                                 (decimal.Parse(row.Cells["feeduemain"].Value.ToString().Trim()) >= 1 | decimal.Parse(row.Cells["feeduemain"].Value.ToString().Trim()) <= -1)) |
                                                 (row.Cells["feeduemgt"].Value != DBNull.Value &&
                                                 (decimal.Parse(row.Cells["feeduemgt"].Value.ToString().Trim()) >= 1 | decimal.Parse(row.Cells["feeduemgt"].Value.ToString().Trim()) <= -1)) |
                                                 (row.Cells["feeduerec"].Value != DBNull.Value &&
                                                 (decimal.Parse(row.Cells["feeduerec"].Value.ToString().Trim()) >= 1 | decimal.Parse(row.Cells["feeduerec"].Value.ToString().Trim()) <= -1)))
                                          select row).Count();

            if (recordsMoreThanADollar > 0)
            {
                btnCreateJournalEntries.Enabled = true;
                btnSaveAsExcel.Enabled = true;
            }
            else
            {
                btnCreateJournalEntries.Enabled = false;
                btnSaveAsExcel.Enabled = false;
            }
        }

        /// <summary>
        /// Formats the header information to display the columns in clear text to the user.
        /// </summary>
        private void FormatHeader()
        {
            try
            {
                dgvManagementFees.Columns["startPeriod"].HeaderText = "Start Pd";
                dgvManagementFees.Columns["currPeriod"].HeaderText = "Curr Pd";
                dgvManagementFees.Columns["entityid"].HeaderText = "Entity Id";
                dgvManagementFees.Columns["income"].HeaderText = "Income";
                dgvManagementFees.Columns["mgmtrate"].HeaderText = "Mgt Rate";
                dgvManagementFees.Columns["mgmtraterec"].HeaderText = "Mgt Rec Rate";
                dgvManagementFees.Columns["feeAmtMain"].HeaderText = "YTD Fee Amt";
                dgvManagementFees.Columns["feeAmtMgt"].HeaderText = "YTD Mgt Fee Amt";
                dgvManagementFees.Columns["feeAmtRec"].HeaderText = "YTD Rec Fee Amt";
                dgvManagementFees.Columns["feedueMain"].HeaderText = "Fee Due";
                dgvManagementFees.Columns["feedueMgt"].HeaderText = "Fee Due Mgt";
                dgvManagementFees.Columns["feedueRec"].HeaderText = "Fee Due Rec";
                dgvManagementFees.Columns["feechargedMain"].HeaderText = "YTD Fee Charged";
                dgvManagementFees.Columns["feechargedMgt"].HeaderText = "YTD Fee Chg Mgt";
                dgvManagementFees.Columns["feechargedRec"].HeaderText = "YTD Fee Chg Rec";
                dgvManagementFees.Columns["ovrAmt"].HeaderText = "Override Amount";

                dgvManagementFees.AutoResizeColumns();
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Gets the data and displays it in the grid based on the ending period.
        /// </summary>
        /// <param name="endingPeriod">The ending period.</param>
        private void GetData(string endingPeriod)
        {
            try
            {
                DataTable locTbl = new DataTable();

                //get the bank account and routing number
                locTbl = Form1.sqlTransactions.ExecuteQuery($@"
                                        EXEC	[dbo].[HS_GetManagementFees]
                                                @startPeriod = N'{endingPeriod.Trim().Substring(0,4)}01',
                                                @currPeriod = N'{endingPeriod.Trim()}'");

                locTbl.Columns.Add("ovrAmt");

                foreach(DataRow row in locTbl.Rows)
                {
                    row["ovrAmt"] = DBNull.Value;
                }

                dgvManagementFees.DataSource = locTbl;

                FormatHeader();
                FilterGrid();
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }
        #endregion

        #region "Event Handlers"
        private void btnCreateJournalEntries_Click(object sender, EventArgs e)
        {
            try
            {
                this.Wait();
                Form1.sqlTransactions.BeginTransaction();

                Dictionary<string, string> insertValues = new Dictionary<string, string>();

                string source = "GJ";
                string refNum = CalculateRefNum(cboEndingPeriod.SelectedValue.ToString().Trim(), source);

                insertValues.Add("period", cboEndingPeriod.SelectedValue.ToString().Trim());
                insertValues.Add("ref", refNum.Trim());
                insertValues.Add("source", source);
                insertValues.Add("siteid", "@");
                insertValues.Add("actiontype", "CREATE");
                insertValues.Add("lastdate", DateTime.Now.ToString());
                insertValues.Add("userid", HelpfulFunctions.UserID.Trim());

                //insert the record in the history table
                Form1.sqlTransactions.InsertTableData("GL_Journal_History", insertValues);

                //add a credit and debit for each record in the grid
                int i = 1;
                decimal totalAmount = 0M;

                foreach (DataGridViewRow row in dgvManagementFees.Rows)
                {
                    decimal feeDueMain = 0M;
                    decimal feeDueRec = 0M;
                    decimal feeDueMgt = 0M;
                    decimal ovrAmt = 0M;
                    decimal mgt115 = 0M;
                    decimal mgt572 = 0M;

                    decimal.TryParse(row.Cells["ovrAmt"].Value.ToString(), out ovrAmt);
                    decimal.TryParse(row.Cells["feeDueMain"].Value.ToString(), out feeDueMain); 
                    decimal.TryParse(row.Cells["feeDueRec"].Value.ToString(), out feeDueRec);
                    decimal.TryParse(row.Cells["feeDueMgt"].Value.ToString(), out feeDueMgt);
                    if (ovrAmt != 0)
                    {
                        decimal feeAmtMain = 0M;
                        decimal.TryParse(row.Cells["feeAmtMain"].Value.ToString(), out feeAmtMain);
                        if (feeAmtMain != 0)
                        {
                            feeDueMain = feeDueMain + ovrAmt-feeAmtMain;
                        }
                        else
                        {
                            decimal feeAmtRec = 0M;
                            decimal.TryParse(row.Cells["feeAmtRec"].Value.ToString(), out feeAmtRec);
                            decimal feeAmtMgt = 0M;
                            decimal.TryParse(row.Cells["feeAmtMgt"].Value.ToString(), out feeAmtMgt);
                            decimal total = 0M;
                            total = ovrAmt - (feeAmtRec + feeAmtMgt);
                            if (feeAmtRec ==0) { feeAmtRec = 1;  }
                            feeDueRec = Math.Round(feeDueRec + total * (feeAmtMgt / feeAmtRec),2);
                            feeDueMgt  = total  - feeDueRec;
                        }
                    }
                    // if feeduemain - write 623000
                    // if feeduerec - write 2 subs 

                    if (feeDueMain != 0)
                    {
                        if (feeDueMain >= 1M | feeDueMain <= -1M)
                        {
                            i = InsertManagementFee(refNum, i, row, row.Cells["entityid"].Value.ToString().Trim(),
                                 "HS212000000000", $"Management Fee", -1 * feeDueMain);

                            i = InsertManagementFee(refNum, i, row, row.Cells["entityid"].Value.ToString().Trim(),
                                 "HS623000000000", $"Management Fee", feeDueMain);
                            mgt115 += feeDueMain;
                            mgt572 += -feeDueMain;
                            totalAmount += feeDueMain;
                        }
                    }
                    if (feeDueRec != 0)
                    {
                        if (feeDueRec >= 1M | feeDueRec <= -1M)
                        {
                            i = InsertManagementFee(refNum, i, row, row.Cells["entityid"].Value.ToString().Trim(),
                                 "HS212000000000", $"Management Fee", -1 * feeDueRec);

                            i = InsertManagementFee(refNum, i, row, row.Cells["entityid"].Value.ToString().Trim(),
                                 "HS623000FEEREC", $"Management Fee", feeDueRec);
                            mgt115 += feeDueRec;
                            mgt572 += -feeDueRec;

                            totalAmount += feeDueRec;
                        }
                        if (feeDueMgt >= 1M | feeDueMgt <= -1M)
                        {
                            i = InsertManagementFee(refNum, i, row, row.Cells["entityid"].Value.ToString().Trim(),
                                 "HS212000000000", $"Management Fee", -1 * feeDueMgt);

                            i = InsertManagementFee(refNum, i, row, row.Cells["entityid"].Value.ToString().Trim(),
                                 "HS623000FEEMGT", $"Management Fee", feeDueMgt);
                            mgt115 += feeDueMgt;
                            mgt572 += -feeDueMgt;

                            totalAmount += feeDueMgt;
                        }
                    }
                    if ((mgt115 != 0 | mgt572 != 0))
                    {
                            i = InsertManagementFee(refNum, i, row, "600",
                                 "HS572000000000", $"Management Fee for {row.Cells["entityid"].Value.ToString().Trim()}", mgt572);

                            i = InsertManagementFee(refNum, i, row, "600",
                                 "HS115000000000", $"Management Fee for {row.Cells["entityid"].Value.ToString().Trim()}", mgt115);
                            mgt115 = 0;
                            mgt572 = 0;
                    }
                    
                }

                insertValues = new Dictionary<string, string>();

                insertValues.Add("glsource", source);
                insertValues.Add("glref", refNum.Trim());
                insertValues.Add("period", cboEndingPeriod.SelectedValue.ToString().Trim());
                insertValues.Add("basis", "C");
                insertValues.Add("batchamt", totalAmount.ToString("n2"));
                insertValues.Add("release", "N");
                insertValues.Add("sent", "N");
                insertValues.Add("descr", $"MANAGEMENT FEE ENDING {cboEndingPeriod.Text}");
                insertValues.Add("USERID", HelpfulFunctions.UserID.Trim());

                //add the batch sum to the batch control table
                Form1.sqlTransactions.InsertTableData("HS_GLBNCTL", insertValues);

                Form1.sqlTransactions.CommitTransaction();

                MessageBox.Show($"The journal entries {refNum.Trim()}  have been submitted successfully.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                dgvManagementFees.DataSource = null;
            }
            catch (Exception err)
            {
                Form1.sqlTransactions.RollbackTransaction();

                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);

                dgvManagementFees.DataSource = null;
            }
            finally
            {
                this.Useable();
            }
        }
        private int InsertManagementFee(string refNum, int i, DataGridViewRow row, string EntityId, string AcctNum, string Descrpn, decimal Amount)
        {
            Dictionary<string, string> insertValues = new Dictionary<string, string>();

            insertValues.Add("period", cboEndingPeriod.SelectedValue.ToString().Trim());
            insertValues.Add("ref", refNum.Trim());
            insertValues.Add("source", "GJ");
            insertValues.Add("siteid", "@");
            insertValues.Add("item", i.ToString().Trim());
            insertValues.Add("entityid", EntityId);
            insertValues.Add("acctnum", AcctNum);
            if (EntityId != "322")
            {
                insertValues.Add("department", "@");
            }
            else
            {
                insertValues.Add("department", AcctNum.CompareTo( HillConstants.LastBalanceSheetAccount) >  0 ? "01" : "@");
            }
            insertValues.Add("amt", Amount.ToString("N2"));
            insertValues.Add("entrdate", DateTime.Now.ToString().Trim());
            insertValues.Add("reversal", "N");
            insertValues.Add("status", "P");
            insertValues.Add("userid", HelpfulFunctions.UserID.Trim());
            insertValues.Add("basis", "C");
            insertValues.Add("lastdate", DateTime.Now.ToString().Trim());
            insertValues.Add("DESCRPN", Descrpn);
            insertValues.Add("HS_JVNO", "505");

            Form1.sqlTransactions.InsertTableData("JOURNAL", insertValues);
            return ++i;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Form1.sqlTransactions.BeginTransaction(IsolationLevel.ReadUncommitted);

                string endPeriod = cboEndingPeriod.SelectedValue.ToString().Trim();

                GetData(endPeriod);

                int recordsMoreThanADollar = (from DataGridViewRow row in dgvManagementFees.Rows
                                              where ((row.Cells["OVRAMT"].Value != DBNull.Value &&
                                                     (decimal.Parse(row.Cells["OVRAMT"].Value.ToString().Trim()) >= 1 | decimal.Parse(row.Cells["OVRAMT"].Value.ToString().Trim()) <= -1)) |
                                                     (row.Cells["feeduemain"].Value != DBNull.Value &&
                                                     (decimal.Parse(row.Cells["feeduemain"].Value.ToString().Trim()) >= 1 | decimal.Parse(row.Cells["feeduemain"].Value.ToString().Trim()) <= -1)))
                                              select row).Count();

                if (recordsMoreThanADollar > 0)
                {
                    btnCreateJournalEntries.Enabled = true;
                    btnSaveAsExcel.Enabled = true;
                } 
                else
                {
                    btnCreateJournalEntries.Enabled = false;
                    btnSaveAsExcel.Enabled = false;
                }

                Form1.sqlTransactions.CommitTransaction();
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void btnSaveAsExcel_Click(object sender, EventArgs e)
        {
            DialogResult result;
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Comma Separated Values file (.csv)|.csv";
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            saveFileDialog.FileName = string.Format("Management Fees {0}.csv", cboEndingPeriod.Text.Trim().Replace("/", ""));
            result = saveFileDialog.ShowDialog();
            StreamWriter writer;

            try
            {

                //if the user selects to save the file
                if (result == DialogResult.OK)
                {
                    File.Delete(saveFileDialog.FileName.Trim());

                    //create a ".csv" file and write the records to it
                    writer = new StreamWriter(File.OpenWrite(saveFileDialog.FileName.Trim()));

                    writer.WriteLine(string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", "Start Period", "Current Period", "Entity Id",
                                                   "Income", "Management Rate", "Fee Amount",
                                                   "Fee Charged", "Fee Due"));

                    foreach (DataGridViewRow row in dgvManagementFees.Rows)
                    {
                        try
                        {
                            //write an individual Journal Entry to the Excel
                            writer.WriteLine(string.Format("{0},{1},{2},{3},{4},{5},{6},{7}",
                               row.Cells["startPeriod"].Value.ToString().Trim(), row.Cells["currPeriod"].Value.ToString().Trim(),
                               row.Cells["entityid"].Value.ToString().Trim(), decimal.Parse(row.Cells["income"].Value != DBNull.Value ? row.Cells["income"].Value.ToString() : 0M.ToString()).ToString().Trim().Replace("-", "").Replace("(", "").Replace(")", "").Replace(",", ""),
                               decimal.Parse(row.Cells["mgmtrate"].Value != DBNull.Value ? row.Cells["mgmtrate"].Value.ToString().Trim() : 0M.ToString()).ToString().Trim().Replace("-", "").Replace("(", "").Replace(")", "").Replace(",", ""), decimal.Parse(row.Cells["feeAmt"].Value != DBNull.Value ? row.Cells["feeAmt"].Value.ToString().Trim() : 0M.ToString()).ToString().Trim().Replace("-", "").Replace("-", "").Replace("(", "").Replace(")", "").Replace(",", ""),
                               decimal.Parse(row.Cells["feecharged"].Value != DBNull.Value ? row.Cells["feecharged"].Value.ToString().Trim() : 0M.ToString()).ToString().Trim().Replace("-", "").Replace("-", "").Replace("(", "").Replace(")", "").Replace(",", ""), decimal.Parse(row.Cells["feeduemain"].Value != DBNull.Value ? row.Cells["feeduemain"].Value.ToString().Trim() : 0M.ToString().Trim()).ToString().Trim().Replace("-", "").Replace("-", "").Replace("(", "").Replace(")", "").Replace(",", "")));
                        }
                        catch (Exception err)
                        {
                            writer.Close();

                            throw err;
                        }
                    }

                    //close the writer
                    writer.Close();

                    //notify the user the file was saved
                    MessageBox.Show("This file was saved successfully.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    Form1.sqlTransactions.CommitTransaction();
                }
            }
            catch (Exception err)
            {
                Form1.sqlTransactions.RollbackTransaction();

                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void chkHideUnpostableTransactions_CheckedChanged(object sender, EventArgs e)
        {
            FilterGrid();
        }

        private void dgvManagementFees_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                //if value of the cell is not null
                if (e.Value != null)
                {
                    //if the cell is in the "income" column
                    if (e.ColumnIndex == dgvManagementFees.Columns["income"].Index)
                    {
                        //if the value is decimal
                        if (decimal.TryParse(e.Value.ToString().Trim(), out decimal amount))
                        {
                            //format the decimal to 2 decimal places
                            e.Value = amount.ToString("c2");
                            e.CellStyle.Format = "#.##";
                            e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        }
                    }//if the cell is in the "feeAmt" column
                    else if (e.ColumnIndex == dgvManagementFees.Columns["feeAmtmain"].Index ||
                            e.ColumnIndex == dgvManagementFees.Columns["feeAmtmgt"].Index ||
                        e.ColumnIndex == dgvManagementFees.Columns["feeAmtrec"].Index )
                    {
                        //if the value is decimal
                        if (decimal.TryParse(e.Value.ToString().Trim(), out decimal amount))
                        {
                            //format the decimal to 2 decimal places
                            e.Value = amount.ToString("c2");
                            e.CellStyle.Format = "#.##";
                            e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        }
                    }//if the cell is in the "feedue column
                    else if (e.ColumnIndex == dgvManagementFees.Columns["feeduemain"].Index ||
                            e.ColumnIndex == dgvManagementFees.Columns["feeduemgt"].Index ||
                        e.ColumnIndex == dgvManagementFees.Columns["feeduerec"].Index )
                    {
                        //if the value is decimal
                        if (decimal.TryParse(e.Value.ToString().Trim(), out decimal amount))
                        {
                            //format the decimal to 2 decimal places
                            e.Value = amount.ToString("c2");
                            e.CellStyle.Format = "#.##";
                            e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        }
                    }//if the cell is in the "feecharged" column
                    else if (e.ColumnIndex == dgvManagementFees.Columns["feechargedmain"].Index ||
                            e.ColumnIndex == dgvManagementFees.Columns["feechargedmgt"].Index ||
                            e.ColumnIndex == dgvManagementFees.Columns["feechargedrec"].Index )
                    {
                        //if the value is decimal
                        if (decimal.TryParse(e.Value.ToString().Trim(), out decimal amount))
                        {
                            //format the decimal to 2 decimal places
                            e.Value = amount.ToString("c2");
                            e.CellStyle.Format = "#.##";
                            e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        }
                    }//if the cell is in the "mgmtrate" column
                    else if (e.ColumnIndex == dgvManagementFees.Columns["mgmtrate"].Index ||
                        e.ColumnIndex == dgvManagementFees.Columns["mgmtraterec"].Index )
                    {
                        //if the value is decimal
                        if (decimal.TryParse(e.Value.ToString().Trim(), out decimal amount))
                        {
                            //format the decimal to 2 decimal places
                            e.Value = (amount * .01M).ToString("p2");
                            e.CellStyle.Format = "#.##";
                            e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        }
                    }//if the cell is in the "ovrAmt" column
                    else if (e.ColumnIndex == dgvManagementFees.Columns["ovrAmt"].Index)
                    {
                        //if the value is decimal
                        if (decimal.TryParse(e.Value.ToString().Trim(), out decimal amount))
                        {
                            //format the decimal to 2 decimal places
                            e.Value = amount.ToString("c2");
                            e.CellStyle.Format = "#.##";
                            e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        }
                    }
                    else if (e.ColumnIndex == dgvManagementFees.Columns["startPeriod"].Index)
                    {
                        e.Value = string.Format("{0}/{1}", e.Value.ToString().Substring(4, 2), e.Value.ToString().Substring(2, 2));
                    }
                    else if (e.ColumnIndex == dgvManagementFees.Columns["currPeriod"].Index)
                    {
                        e.Value = string.Format("{0}/{1}", e.Value.ToString().Substring(4, 2), e.Value.ToString().Substring(2, 2));
                    }
                }
            }
            catch (Exception err)
            {
                //HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                //                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().ToString().Trim(), true, "", (Dictionary<string, string>)err.Data, true);
                HelpfulFunctions.LogMessage(err.Message.Trim(), EventLogEntryType.Error);
            }
        }

        private void dgvManagementFees_MouseClick(object sender, MouseEventArgs e)
        {
            dgvManagementFees.ClearSelection();

            DataGridView.HitTestInfo hit = dgvManagementFees.HitTest(e.X, e.Y);
            if (hit.RowIndex > -1)
            {
                dgvManagementFees.Rows[hit.RowIndex].Selected = true;

                if (dgvManagementFees.SelectedRows.Count > 0)
                {
                    //if the grid is right clicked
                    if (e.Button == MouseButtons.Right)
                    {
                        //prompt the user to what amount the want to override the selected "Fee due" amount to
                        Prompt prompt = new Prompt("Please enter the amount you want to change the fee due to.", "Attention!!!");
                        prompt.StartPosition = FormStartPosition.CenterScreen;

                        //create a menu item to display in the menu list
                        MenuItem menuItem = new MenuItem();
                        //create a delegate method to handle overriding the "fee due" amount
                        menuItem.Click += delegate
                        {
                            tryAgain:
                            prompt.ShowDialog(this);
                            DataGridViewRow selMngmFee = new DataGridViewRow();

                        //if the user select submit
                        if (prompt.Result == DialogResult.OK)
                            {
                            //parse out if the user enter a decimal
                            if (decimal.TryParse(prompt.Response.Trim(), out decimal ovrAmt))
                                {
                                //set the selected "fee due" to the override amount
                                selMngmFee = dgvManagementFees.SelectedRows[0];
                                    selMngmFee.Cells["ovrAmt"].Value = prompt.Response.Trim();
                                }
                                else
                                {
                                //if the user entered something that is not numeric, warn them and prompt them again
                                MessageBox.Show("Please enter only a numeric value.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    goto tryAgain;
                                }
                            }
                        };
                        menuItem.Text = "Override Fee Due";

                        //create the menu to display to user when the right click on the grid
                        ContextMenu menu = new ContextMenu();
                        menu.MenuItems.Add(menuItem);
                        menu.Show((Control)sender, e.Location);
                    }
                }
            }
        }

        private void frmManagementFees_FormClosing(object sender, FormClosingEventArgs e)
        {
            //recreate the connection the SQL server
            Form1.sqlTransactions = new SqlDataAccess.SqlTransactions(Properties.Settings.Default.sqlConnection.Trim());
            Form1.sqlTransactions.Connect();
        }

        private void frmManagementFees_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + " (MS SQL Server: " + Form1.sqlTransactions.GetDatabase().Trim() + ")";

            CreateDropDownList();

            //recreate the connection the SQL server
            Form1.sqlTransactions = new SqlDataAccess.SqlTransactions(Properties.Settings.Default.sqlConnection.Trim());
        }
        #endregion
    }
}
