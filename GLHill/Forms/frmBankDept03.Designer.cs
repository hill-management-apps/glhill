﻿
namespace GLHill.Forms
{
    partial class frmBankDept03
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpActualDepositDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.txtRefRecNum = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(346, 10);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(79, 46);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Business Deposit Date:";
            // 
            // dtpActualDepositDate
            // 
            this.dtpActualDepositDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpActualDepositDate.Location = new System.Drawing.Point(152, 16);
            this.dtpActualDepositDate.Name = "dtpActualDepositDate";
            this.dtpActualDepositDate.Size = new System.Drawing.Size(97, 20);
            this.dtpActualDepositDate.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Bank Reference/Receipt:";
            // 
            // txtRefRecNum
            // 
            this.txtRefRecNum.Location = new System.Drawing.Point(135, 42);
            this.txtRefRecNum.Name = "txtRefRecNum";
            this.txtRefRecNum.Size = new System.Drawing.Size(205, 20);
            this.txtRefRecNum.TabIndex = 11;
            // 
            // frmBankDept03
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 116);
            this.Controls.Add(this.txtRefRecNum);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpActualDepositDate);
            this.MaximizeBox = false;
            this.Name = "frmBankDept03";
            this.Text = "SiteLink Bank Deposits";
            this.Load += new System.EventHandler(this.frmBankDept03_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpActualDepositDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtRefRecNum;
    }
}