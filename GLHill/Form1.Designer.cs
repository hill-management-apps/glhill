﻿
namespace GLHill
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.utilitiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.equipmentRentalInvoiceImportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.siteLinkDepositSlipsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enterDepositsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setDepositDatesAndReceiptsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblCopyright = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.utilitiesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(93, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // utilitiesToolStripMenuItem
            // 
            this.utilitiesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.equipmentRentalInvoiceImportToolStripMenuItem,
            this.siteLinkDepositSlipsToolStripMenuItem});
            this.utilitiesToolStripMenuItem.Name = "utilitiesToolStripMenuItem";
            this.utilitiesToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.utilitiesToolStripMenuItem.Text = "Utilities";
            // 
            // equipmentRentalInvoiceImportToolStripMenuItem
            // 
            this.equipmentRentalInvoiceImportToolStripMenuItem.Name = "equipmentRentalInvoiceImportToolStripMenuItem";
            this.equipmentRentalInvoiceImportToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.equipmentRentalInvoiceImportToolStripMenuItem.Text = "Equipment Rental Invoice Import";
            this.equipmentRentalInvoiceImportToolStripMenuItem.Click += new System.EventHandler(this.equipmentRentalInvoiceImportToolStripMenuItem_Click);
            // 
            // siteLinkDepositSlipsToolStripMenuItem
            // 
            this.siteLinkDepositSlipsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enterDepositsToolStripMenuItem,
            this.setDepositDatesAndReceiptsToolStripMenuItem});
            this.siteLinkDepositSlipsToolStripMenuItem.Name = "siteLinkDepositSlipsToolStripMenuItem";
            this.siteLinkDepositSlipsToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
            this.siteLinkDepositSlipsToolStripMenuItem.Text = "SiteLink Deposit Slips";
            // 
            // enterDepositsToolStripMenuItem
            // 
            this.enterDepositsToolStripMenuItem.Name = "enterDepositsToolStripMenuItem";
            this.enterDepositsToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.enterDepositsToolStripMenuItem.Text = "Enter Deposits";
            this.enterDepositsToolStripMenuItem.Click += new System.EventHandler(this.enterDepositsToolStripMenuItem_Click);
            // 
            // setDepositDatesAndReceiptsToolStripMenuItem
            // 
            this.setDepositDatesAndReceiptsToolStripMenuItem.Name = "setDepositDatesAndReceiptsToolStripMenuItem";
            this.setDepositDatesAndReceiptsToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.setDepositDatesAndReceiptsToolStripMenuItem.Text = "Set Deposit Dates and Receipts";
            this.setDepositDatesAndReceiptsToolStripMenuItem.Click += new System.EventHandler(this.setDepositDatesAndReceiptsToolStripMenuItem_Click);
            // 
            // lblCopyright
            // 
            this.lblCopyright.AutoSize = true;
            this.lblCopyright.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCopyright.ForeColor = System.Drawing.Color.Navy;
            this.lblCopyright.Location = new System.Drawing.Point(12, 428);
            this.lblCopyright.Name = "lblCopyright";
            this.lblCopyright.Size = new System.Drawing.Size(41, 13);
            this.lblCopyright.TabIndex = 1;
            this.lblCopyright.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.BackgroundImage = global::GLHill.Properties.Resources.Hill;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblCopyright);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Financial Portal";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Label lblCopyright;
        private System.Windows.Forms.ToolStripMenuItem utilitiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem equipmentRentalInvoiceImportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem siteLinkDepositSlipsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setDepositDatesAndReceiptsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enterDepositsToolStripMenuItem;
    }
}

