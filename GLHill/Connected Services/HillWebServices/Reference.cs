﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GLHill.HillWebServices {
    using System.Data;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="HillWebServices.HillWebServicesSoap")]
    public interface HillWebServicesSoap {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/HelloWorld", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        string HelloWorld();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/HelloWorld", ReplyAction="*")]
        System.Threading.Tasks.Task<string> HelloWorldAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetAllocations", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Data.DataTable GetAllocations(string period, decimal amount, string allocationCode);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetAllocations", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Data.DataTable> GetAllocationsAsync(string period, decimal amount, string allocationCode);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetJobCodes", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Data.DataTable GetJobCodes();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetJobCodes", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Data.DataTable> GetJobCodesAsync();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface HillWebServicesSoapChannel : GLHill.HillWebServices.HillWebServicesSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class HillWebServicesSoapClient : System.ServiceModel.ClientBase<GLHill.HillWebServices.HillWebServicesSoap>, GLHill.HillWebServices.HillWebServicesSoap {
        
        public HillWebServicesSoapClient() {
        }
        
        public HillWebServicesSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public HillWebServicesSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public HillWebServicesSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public HillWebServicesSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string HelloWorld() {
            return base.Channel.HelloWorld();
        }
        
        public System.Threading.Tasks.Task<string> HelloWorldAsync() {
            return base.Channel.HelloWorldAsync();
        }
        
        public System.Data.DataTable GetAllocations(string period, decimal amount, string allocationCode) {
            return base.Channel.GetAllocations(period, amount, allocationCode);
        }
        
        public System.Threading.Tasks.Task<System.Data.DataTable> GetAllocationsAsync(string period, decimal amount, string allocationCode) {
            return base.Channel.GetAllocationsAsync(period, amount, allocationCode);
        }
        
        public System.Data.DataTable GetJobCodes() {
            return base.Channel.GetJobCodes();
        }
        
        public System.Threading.Tasks.Task<System.Data.DataTable> GetJobCodesAsync() {
            return base.Channel.GetJobCodesAsync();
        }
    }
}
