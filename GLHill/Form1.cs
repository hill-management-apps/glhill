﻿using GLHill.Forms;
using HelpfulClasses;
using SqlDataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GLHill
{
    public partial class Form1 : Form
    {
        public static SqlTransactions sqlTransactions;

        public Form1()
        {
            InitializeComponent();

            try
            {
                sqlTransactions = new SqlTransactions(Properties.Settings.Default.sqlConnection);
                sqlTransactions.Connect();
            }
            catch (Exception err)
            {

            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void importToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = @"\\hmfile01\Finance\Applications\JournalImport\JournalImport.exe";
            Process.Start(startInfo);
        }

        private void summaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = @"\\hmfile01\Finance\Applications\JournalImport\JournalImport.exe";
            startInfo.Arguments = "summary";
            Process.Start(startInfo);
        }

        private void importClickPayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = @"\\hmfile01\Finance\Applications\CPImport\CPImport.exe";
            Process.Start(startInfo);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string version = Application.ProductVersion;
            this.Text = this.Text + " - V: " + version;
            
            string copyrightMessage = $"© {DateTime.Now.ToString("MMMMM")} {DateTime.Now.Year}";
            lblCopyright.Text = copyrightMessage;
        }

        private void compareToAS400ToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = @"\\hmfile01\Finance\Applications\GLCompare\GLCompare.exe";
            Process.Start(startInfo);
        }

        private void siteLinkInterfaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = @"\\hmfile01\Finance\Applications\JournalImport\JournalImport.exe";
            startInfo.Arguments = "sitelink";
            Process.Start(startInfo);
        }

        private void conserviceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = @"\\hmfile01\Finance\Applications\JournalImport\JournalImport.exe";
            startInfo.Arguments = "conservice";
            Process.Start(startInfo);
        }

        private void setClosingBillingDatesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSetClsBillDates frmClosingBillingDates = new frmSetClsBillDates();
            frmClosingBillingDates.ShowDialog();
        }

        private void payrollInvoiceImportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmInvcImp invoiceImport = new frmInvcImp();
            invoiceImport.StartPosition = FormStartPosition.CenterParent;
            invoiceImport.ShowDialog(this);
        }

        private void equipmentRentalInvoiceImportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEqpmntRntInvcImp equipmentRentalInvoiceImport = new frmEqpmntRntInvcImp();
            equipmentRentalInvoiceImport.StartPosition = FormStartPosition.CenterParent;
            equipmentRentalInvoiceImport.ShowDialog(this);
        }

        private void generalLedgerImportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor; 
            frmPeakPRImp genLdgImp = new frmPeakPRImp();
            genLdgImp.StartPosition = FormStartPosition.CenterParent;
            genLdgImp.ShowDialog(this);
            this.Cursor = Cursors.Default;
        }

        private void setDepositDatesAndReceiptsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            frmBankDept bankDept = new frmBankDept(true);
            bankDept.StartPosition = FormStartPosition.CenterParent;
            bankDept.ShowDialog(this);
            this.Cursor = Cursors.Default;
        }

        private void enterDepositsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            frmBankDept bankDept = new frmBankDept();
            bankDept.StartPosition = FormStartPosition.CenterParent;
            bankDept.ShowDialog(this);
            this.Cursor = Cursors.Default;
        }

        private void createManagementFeeJournalEntriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Wait();
            frmManagementFees managementFees = new frmManagementFees();
            managementFees.StartPosition = FormStartPosition.CenterParent;
            managementFees.ShowDialog(this);
            this.Useable();
        }

        private void createManagementFeeInvoiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Wait();
            frmManagementInvoice managementInvoices = new frmManagementInvoice();
            managementInvoices.StartPosition = FormStartPosition.CenterParent;
            managementInvoices.ShowDialog(this);
            this.Useable();
        }
    }
}
