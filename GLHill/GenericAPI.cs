﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace HillAPI
{
    public abstract class GenericAPI
    {
        #region "Fields"
        private string _address = "";
        private string _clientId = "";
        private string _clientSecret = "";
        private string _endPoint = "";
        private WebRequest _httpRequest;
        private string _key = "";
        private int _duration = 0;
        private string _queryString = "";
        private string _refreshKey = "";
        private WEB_METHOD _webMethod;
        #endregion

        #region "Properties"
        /// <summary>
        /// The URL where the API is located.
        /// </summary>
        protected string Address { get => _address; set => _address = value; }
        /// <summary>
        /// The client id. This used to authenticate.
        /// </summary>
        protected string ClientId { get => _clientId; set => _clientId = value; }
        /// <summary>
        /// The client secret. This used to authenticate.
        /// </summary>
        protected string ClientSecret { get => _clientSecret; set => _clientSecret = value; }
        /// <summary>
        /// The end of the URL. It directs the web call to the specific resource on the server.
        /// </summary>
        protected string EndPoint { get => _endPoint; set => _endPoint = value; }
        /// <summary>
        /// The web call object that will send the command to the server.
        /// </summary>
        protected WebRequest HttpRequest { get => _httpRequest; set => _httpRequest = value; }
        /// <summary>
        /// The authentication key.
        /// </summary>
        protected string Key { get => _key; set => _key = value; }
        public int Duration { get => _duration; set => _duration = value; }
        /// <summary>
        /// The query string to retrieve specific records on a GET.
        /// </summary>
        protected string QueryString { get => _queryString; set => _queryString = value; }
        /// <summary>
        /// This is the refresh key. Used to period refresh API's authentication keys.
        /// </summary>
        protected string RefreshKey { get => _refreshKey; set => _refreshKey = value; }
        /// <summary>
        /// A concatination of address, end point, and query string.
        /// </summary>
        protected string Url
        {
            get
            {
                string urlReturn = "";
                urlReturn = _address.Trim() + _endPoint.Trim();

                if (_queryString.Trim() != "")
                {
                    urlReturn += _queryString.Trim();
                }

                return urlReturn.Trim();
            }
        }
        internal WEB_METHOD WebMethod { get => _webMethod; set => _webMethod = value; }
        #endregion

        #region "Constructors"
        /// <summary>
        /// Instantiates the Generic API class and the inherited class with an API's URL, that uses a Client ID, CLient Secret, the current Key, and Refresh Key.
        /// </summary>
        /// <param name="address">The API URL.</param>
        /// <param name="clientId">The Client ID.</param>
        /// <param name="clientSecret">The Client Secret.</param>
        /// <param name="key">The current authentication key.</param>
        /// <param name="refreshKey">The current refresh key.</param>
        protected GenericAPI(string address, string clientId, string clientSecret, string key, string refreshKey)
        {
            Address = address;
            ClientId = clientId;
            ClientSecret = clientSecret;
            Key = key;
            RefreshKey = refreshKey;
        }

        /// <summary>
        /// Instantiates the Generic API class and the inherited class with an API's URL, that uses a Client ID, CLient Secret, the current Key, and Refresh Key.
        /// </summary>
        /// <param name="address">The API URL.</param>
        /// <param name="clientId">The Client ID.</param>
        /// <param name="clientSecret">The Client Secret.</param>
        /// <param name="key">The current authentication key.</param>
        /// <param name="refreshKey">The current refresh key.</param>
        /// <param name="duration">The number of seconds until the key needs to be refeshed for new one. It is 0 unless specified otherwise.</param>
        protected GenericAPI(string address, string clientId, string clientSecret, string key, string refreshKey, int duration = 0)
        {
            Address = address;
            ClientId = clientId;
            ClientSecret = clientSecret;
            Key = key;
            Duration = duration;
            RefreshKey = refreshKey;
        }

        /// <summary>
        /// Instantiates a new Generic API and inherited class with an API's URL.
        /// </summary>
        /// <param name="address">The current address.</param>
        protected GenericAPI(string address)
        {
            Address = address;
        }

        /// <summary>
        /// Instantiates a new Generic API and inherited class with an API's URL and the current Authentication key.
        /// </summary>
        /// <param name="address">The API's URL.</param>
        /// <param name="key">The current Authentication key.</param>
        protected GenericAPI(string address, string key)
        {
            Address = address;
            Key = key;
        }
        #endregion

        #region "Enumerations"
        /// <summary>
        /// The web methods or action that can be done with an API's resource.
        /// </summary>
        internal enum WEB_METHOD
        {
            DELETE,
            GET,
            PATCH,
            POST,
            PUT
        }
        #endregion

        #region "Methods"
        public void Dispose()
        {
            Address = "";
            ClientId = "";
            ClientSecret = "";
            Key = "";
            EndPoint = "";
            Duration = 0;
            RefreshKey = "";
            HttpRequest = null;
            QueryString = "";
        }
        /// <summary>
        /// Creates an authentication key using Basic Authentication. Only use when the API uses Basic Authentication.
        /// </summary>
        /// <param name="username">The username of the account the key is for.</param>
        /// <param name="password">The passsword of the account the key is for.</param>
        /// <returns>Returns a string value of the key.</returns>
        /// <remarks>It is typically added to the web call header to authenticate the web call was done by an autheorized user.</remarks>
        public static string CreateBasicAuthenticationKey(string username, string password)
        {
            return Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));
        }
        internal abstract void SetupHeader(WEB_METHOD wm);
        #endregion
    }
}
