﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GLHill.Classes
{
    class GeneralLedgerAccountTotal
    {
        private string _ledgerCode = "";
        private string _accountNumber = "";
        private List<GeneralLedgerAccountLine> _generalLedgerLines = new List<GeneralLedgerAccountLine>();

        public GeneralLedgerAccountTotal()
        {
            Errors = new List<string>();
        }

        public List<GeneralLedgerAccountLine> GeneralLedgerLines { get => _generalLedgerLines; set => _generalLedgerLines = value; }
        public string LedgerCode { get => _ledgerCode; set => _ledgerCode = value; }
        public string AccountNumber { get => _accountNumber; set => _accountNumber = value; }
        public string Account
        {
            get
            {
                return LedgerCode.Trim() + AccountNumber.Trim().PadRight(12, '0');
            }
        }
        public List<string> Errors { get; set; }
        public bool HasErrors
        {
            get
            {
                if ((Errors.Count > 0) | (from GeneralLedgerAccountLine generalLedgerAccountLine in _generalLedgerLines
                                          where generalLedgerAccountLine.HasErrors == true
                                          select generalLedgerAccountLine).Count() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public decimal Total
        {
            get
            {
                decimal amount;

                amount = (from GeneralLedgerAccountLine generalLedgerAccountLine in _generalLedgerLines
                          select generalLedgerAccountLine.GLAmount).Sum();

                return amount;
            }
        }

        public override string ToString()
        {
            string retVal = "";

            retVal = string.Format("{0}\n", ("Account " + Account.Trim()));

            foreach (GeneralLedgerAccountLine generalLedgerAccountLine in GeneralLedgerLines)
            {
                retVal += generalLedgerAccountLine.ToString().Trim() + "\n";
            }

            return retVal.Trim();
        }
    }
}
