﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GLHill.Classes
{
    class GeneralLedgerDepartmentTotal
    {
        private string _department = "";
        private List<GeneralLedgerAccountTotal> _generalLedgerAccountTotals = new List<GeneralLedgerAccountTotal>();

        public GeneralLedgerDepartmentTotal()
        {
            Errors = new List<string>();
        }

        public string Department { get => _department; set => _department = value; }
        public List<GeneralLedgerAccountTotal> GeneralLedgerAccountTotals { get => _generalLedgerAccountTotals; set => _generalLedgerAccountTotals = value; }
        public List<string> Errors { get; set; }
        public bool HasErrors
        {
            get
            {
                if ((Errors.Count > 0) | (from GeneralLedgerAccountTotal generalLedgerAccountTotal in GeneralLedgerAccountTotals
                                          where generalLedgerAccountTotal.HasErrors == true
                                          select generalLedgerAccountTotal).Count() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public decimal Total
        {
            get
            {
                decimal amount;

                amount = (from GeneralLedgerAccountTotal generalLedgerAccountTotal in _generalLedgerAccountTotals
                          select generalLedgerAccountTotal.Total).Sum();

                return amount;
            }
        }

        public override string ToString()
        {
            string retVal = "";

            retVal = string.Format("{0}\n", ("Department " + Department.Trim()));

            foreach(GeneralLedgerAccountTotal generalLedgerAccountTotal in GeneralLedgerAccountTotals)
            {
                retVal += generalLedgerAccountTotal.ToString() + "\n";
            }

            return retVal.Trim();
        }

    }
}
