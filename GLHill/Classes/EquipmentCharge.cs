﻿using GLHill.Forms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GLHill.Classes.EquipmentChargeLine;

namespace GLHill.Classes
{
    class EquipmentCharge
    {
        private string _entity = "";
        private string _entityName = "";
        private DateTime _expensePeriod;
        private List<EquipmentChargeLine> _equipmentChargeLines;
        private string _name = "";
        private decimal _rate = 0M;
        private int _payPeriod = 0;
        private DateTime _payWeekEnding;
        private DataTable locTblJobCost = new DataTable();
        private HillWebServices.HillWebServicesSoapClient hillWebService = new HillWebServices.HillWebServicesSoapClient();

        public EquipmentCharge()
        {
            string strSQL = "";

            _equipmentChargeLines = new List<EquipmentChargeLine>();
            Errors = new List<string>();

            //strSQL = $@"SELECT j.jobcode, m.jc_phasecode, m.jc_costlist,
            //            m.jc_costcode, m.entityid, c.NAME, m.department, m.acctnum, j.descrptn
            //            FROM gjob j 
            //            INNER JOIN JC_MAP m on j.jobcode = m.jobcode
            //            LEFT JOIN ENTITY as C ON m.entityid = C.ENTITYID
            //            ORDER BY ENTITYID DESC";

            locTblJobCost = hillWebService.GetJobCodes();
        }

        public string Entity
        {
            get => _entity;
            set
            {
                if (value == "030")
                {
                    _entity = "633";
                }
                else if (value == "701" | value == "721" | value == "722" | value == "726" | value == "734")
                {
                    _entity = "322";
                }
                else
                {
                    _entity = value;
                }
            }
        }
        public List<EquipmentChargeLine> EquipmentChangeLines { get => _equipmentChargeLines; set => _equipmentChargeLines = value; }
        public string Name { get => _name; set => _name = value; }
        public decimal Rate { get => _rate; set => _rate = value; }
        public DateTime PayWeekEnding { get => _payWeekEnding; set => _payWeekEnding = value; }
        public List<string> Errors { get; set; }
        public bool HasErrors
        {
            get
            {
                if ((Errors.Count > 0) | (from EquipmentChargeLine equipmentChargeLine in EquipmentChangeLines
                                          where equipmentChargeLine.HasErrors == true
                                          select equipmentChargeLine).Count() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public string EntityName { get => _entityName; set => _entityName = value; }
        public string Invoice
        {
            get
            {
                string[] entityParts = Entity.Trim().Split(',');

                //check if it is possibly a job code 
                if (entityParts.Length == 4)
                {
                    return GetJobEntityId(entityParts).Trim() + "-" + ExpensePeriod.ToString("yy") + "-" + PayPeriod;
                }
                else
                {
                    return Entity.Trim() + "-" + ExpensePeriod.ToString("yy") + "-" + PayPeriod;
                }
            }
        }

        public DateTime ExpensePeriod { get => _expensePeriod; set => _expensePeriod = value; }
        public int PayPeriod { get => _payPeriod; set => _payPeriod = value; }

        public decimal GetTotalHours()
        {
            decimal retVal = 0M;

            retVal = (from EquipmentChargeLine line in EquipmentChangeLines
                      select line.Hours).Sum();

            return retVal;
        }

        private string GetJobEntityId(string[] jobCostParts)
        {
            try
            {
                string entityId = "";

                if (jobCostParts.Length == 4)
                {
                    try
                    {
                        entityId = (from DataRow jobCostRow in locTblJobCost.Rows
                                    where jobCostRow["JOBCODE"].ToString().Trim() == jobCostParts[0].Trim() &
                                          jobCostRow["JC_PHASECODE"].ToString().Trim() == jobCostParts[1].Trim() &
                                          jobCostRow["JC_COSTLIST"].ToString().Trim() == jobCostParts[2].Trim() &
                                          jobCostRow["JC_COSTCODE"].ToString().Trim() == jobCostParts[3].Trim()
                                    select jobCostRow["ENTITYID"].ToString().Trim()).Single();
                    }
                    catch (ArgumentNullException err)
                    {

                    }
                    catch (InvalidOperationException err)
                    {

                    }

                    return entityId.Trim();
                }
                else
                {
                    return entityId.Trim();
                }
            }
            catch (Exception err)
            {
                return "";
            }
        }

        public string GetJobEntityInfo(string[] jobCostParts)
        {
            try
            {
                string entityId = "";

                if (jobCostParts.Length == 4)
                {
                    try
                    {
                        entityId = (from DataRow jobCostRow in locTblJobCost.Rows
                                    where jobCostRow["JOBCODE"].ToString().Trim() == jobCostParts[0].Trim() &
                                          jobCostRow["JC_PHASECODE"].ToString().Trim() == jobCostParts[1].Trim() &
                                          jobCostRow["JC_COSTLIST"].ToString().Trim() == jobCostParts[2].Trim() &
                                          jobCostRow["JC_COSTCODE"].ToString().Trim() == jobCostParts[3].Trim()
                                    select jobCostRow["ENTITYID"].ToString().Trim() + "-" + jobCostRow["NAME"].ToString().Trim()).Single();
                    }
                    catch (ArgumentNullException err)
                    {

                    }
                    catch (InvalidOperationException err)
                    {

                    }

                    return entityId.Trim();
                }
                else
                {
                    return entityId.Trim();
                }
            }
            catch (Exception err)
            {
                return "";
            }
        }

        public decimal GetTotalCharges()
        {
            decimal retVal = 0;

            retVal = Rate * GetTotalHours();

            return retVal;
        }
    }
}
