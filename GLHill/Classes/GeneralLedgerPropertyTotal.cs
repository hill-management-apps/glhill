﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GLHill.Classes
{
    class GeneralLedgerPropertyTotal
    {
        private string _property = "";
        private List<GeneralLedgerDepartmentTotal> _generalLedgerDepartmentTotals = new List<GeneralLedgerDepartmentTotal>();
        private string _name = "";
        private DateTime _payWeekEnding = DateTime.MinValue;
        private DateTime _payDate = DateTime.MinValue;
        private string _jobCode = "";
        private string _phaseCode = "";
        private string _costList = "";
        private string _costCode = "";

        public GeneralLedgerPropertyTotal()
        {
            Errors = new List<string>();
        }

        public string Property
        {
            get => _property;
            set
            {
                if (value == "030")
                {
                    _property = "633";
                }
                else if (value == "701" | value == "721" | value == "722" | value == "726" | value == "734")
                {
                    _property = "322";
                }
                else
                {
                    _property = value;
                }
            }
        }
        public List<GeneralLedgerDepartmentTotal> GeneralLedgerDepartmentTotals { get => _generalLedgerDepartmentTotals; set => _generalLedgerDepartmentTotals = value; }
        public List<string> Errors { get; set; }
        public string Name { get => _name; set => _name = value; }
        public DateTime PayWeekEnding { get => _payWeekEnding; set => _payWeekEnding = value; }
        public DateTime PayDate { get => _payDate; set => _payDate = value; }

        public bool HasErrors
        {
            get
            {
                if ((Errors.Count > 0) | (from GeneralLedgerDepartmentTotal generalLedgerDepartmentTotal in _generalLedgerDepartmentTotals
                                          where generalLedgerDepartmentTotal.HasErrors == true
                                          select generalLedgerDepartmentTotal).Count() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public decimal Total
        {
            get
            {
                decimal amount;

                amount = (from GeneralLedgerDepartmentTotal generalLedgerDepartmentTotal in _generalLedgerDepartmentTotals
                          select generalLedgerDepartmentTotal.Total).Sum();

                return amount;
            }
        }

        public string JobCode
        {
            get
            {
                return _jobCode.Trim();
            }
            set
            {
                _jobCode = value.Trim();
            }
        }

        public string PhaseCode
        {
            get
            {
                return _phaseCode.Trim();
            }
            set
            {
                _phaseCode = value.Trim();
            }
        }

        public string CostList
        {
            get
            {
                return _costList.Trim();
            }
            set
            {
                _costList = value.Trim();
            }
        }

        public string CostCode
        {
            get
            {
                return _costCode.Trim();
            }
            set
            {
                _costCode = value.Trim();
            }
        }

        public bool IsJobCode
        {
            get
            {
                if(_jobCode.Trim() != "" | _phaseCode.Trim() != "" | _costList.Trim() != "" | _costCode.Trim() != "")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public override string ToString()
        {
            string retVal = "";

            retVal = string.Format("{0}\n", ("Property " + Property.Trim()));

            foreach (GeneralLedgerDepartmentTotal generalLedgerDepartmentTotal in GeneralLedgerDepartmentTotals)
            {
                retVal += generalLedgerDepartmentTotal.ToString();
            }

            return retVal.Trim();
        }
    }
}
