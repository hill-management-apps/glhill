﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GLHill.Classes
{
    public class Employee
    {
        private string _lastName;
        private string _firstName;
        private string _middleName;
        private string _suffix;
        private string _fullName;

        public Employee()
        {

        }

        public Employee(string FullName)
        {
            string[] nameParts = FullName.Split(' ');

            if(nameParts.Length > 0)
            {
                _lastName = nameParts[0].Trim();
            }

            if (nameParts.Length > 1)
            {
                _firstName = nameParts[1].Trim();
            }

            if (nameParts.Length > 2)
            {
                _middleName = nameParts[2].Trim();
            }

            if (nameParts.Length > 3)
            {
                _suffix = nameParts[3].Trim();
            }
        }

        public string LastName { get => _lastName; set => _lastName = value; }
        public string FirstName { get => _firstName; set => _firstName = value; }
        public string MiddleName { get => _middleName; set => _middleName = value; }
        public string Suffix { get => _suffix; set => _suffix = value; }
        public string FullName { 
            get
            {
                return _lastName.Trim() + ", " + _firstName.Trim() + " " + _middleName.Trim() + " " + _suffix.Trim();
            }
        }
    }
}
