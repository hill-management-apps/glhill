﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GLHill.Classes
{
    class GeneralLedgerAccountLine
    {
        private string _property = "";
        private string _department = "";
        private string _ledgerCode = "";
        private string _account = "";
        private decimal _glAmount = 0M;
        private decimal _deptAmount = 0M;

        public GeneralLedgerAccountLine()
        {
            Errors = new List<string>();
        }

        public List<string> Errors { get; set; }
        public bool HasErrors
        {
            get
            {
                if (Errors.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public string Property
        {
            get => _property;
            set
            {
                if(value == "030")
                {
                    _property = "633";
                }
                else if(value == "701" | value == "721" | value == "722" | value == "726" | value == "734")
                {
                    _property = "322";
                }
                else
                {
                    _property = value;
                }
            }
        }
        public string Department { get => _department; set => _department = value; }
        public string LedgerCode { get => _ledgerCode; set => _ledgerCode = value; }
        public string Account { get => _account; set => _account = value; }
        public decimal GLAmount { get => _glAmount; set => _glAmount = value; }
        public decimal DeptAmount { get => _deptAmount; set => _deptAmount = value; }

        public override string ToString()
        {
            string retVal = "";

            retVal += string.Format("{0}", GLAmount.ToString().Trim());

            return retVal.Trim();
        }
    }
}
