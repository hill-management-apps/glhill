﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace GLHill.Forms
{
    public class Invoice
    {
        public string InvoiceDescription { get; set; }
        public string _Invoice { get; set; }
        public DateTime WeekEnding { get; set; }
        public DateTime PayDate { get; set; }
        public List<string> Errors { get; set; }
        public bool HasErrors
        {
            get
            {
                if ((Errors.Count > 0) | (from InvoiceLine invoiceLine in InvoiceLines
                                          where invoiceLine.HasErrors == true
                                          select invoiceLine).Count() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public Invoice()
        {
            InvoiceLines = new List<InvoiceLine>();
            Errors = new List<string>();
        }

        public Invoice(string invoiceDescription, DateTime weekEnding, DateTime payDate)
        {
            InvoiceDescription = invoiceDescription;
            WeekEnding = weekEnding;
            PayDate = payDate;
            Errors = new List<string>();
        }

        public List<InvoiceLine> InvoiceLines { get; set; }

        public decimal GetAccountSummaryByCompany(string Company, string LedgerCode, int Account)
        {
            decimal retVal = (from InvoiceLine invoice in InvoiceLines
                              where invoice.Company == Company.Trim() & invoice.LedgerCode == LedgerCode.Trim() & invoice.Account == Account
                              select invoice.Amount).Sum();

            return retVal;
        }

        public decimal GetAccountSummaryByProject(string Company, string Project, string LedgerCode, int Account)
        {
            decimal retVal = (from InvoiceLine invoice in InvoiceLines
                              where invoice.Company == Company.Trim() &
                              invoice.Project == Project & invoice.LedgerCode == LedgerCode.Trim() & invoice.Account == Account
                              select invoice.Amount).Sum();

            return retVal;
        }

        public List<InvoiceLine> GetInvoiceLinesByCompany(string Company)
        {
            return (from InvoiceLine invoice in InvoiceLines
                    where invoice.Company == Company.Trim()
                    select invoice).ToList();
        }

        public List<InvoiceLine> GetInvoiceLinesByProject(string Company, string Project)
        {
            return (from InvoiceLine invoice in InvoiceLines
                    where invoice.Company == Company.Trim() & invoice.Project == Project
                    select invoice).ToList();
        }

        public decimal GetSummaryByCompany(string Company, string LedgerCode)
        {
            decimal retVal = (from InvoiceLine invoice in InvoiceLines
                              where invoice.Company == Company.Trim() & invoice.LedgerCode == LedgerCode.Trim()
                              select invoice.Amount).Sum();

            return retVal;
        }

        public decimal GetSummaryByProject(string Company, string Project)
        {
            decimal retVal = (from InvoiceLine invoice in InvoiceLines
                              where invoice.Company == Company.Trim() &
                              invoice.Project == Project
                              select invoice.Amount).Sum();

            return retVal;
        }

        public decimal GetSummaryByAccount(string LedgerCode, int Account)
        {
            decimal retVal = (from InvoiceLine invoice in InvoiceLines
                              where invoice.LedgerCode == LedgerCode.Trim() & invoice.Account == Account
                              select invoice.Amount).Sum();

            return retVal;
        }
        
        public string WriteOutInvoice()
        {
            int headerLength = 265;
            string formattedInvoice = "";

            if (InvoiceLines.Count < 1)
            {
                formattedInvoice = "There are not lines to display.";
            }
            else 
            {
                for (int c = 0; c < headerLength; c++)
                {
                    formattedInvoice += "=";
                }

                formattedInvoice += "\n" + string.Format("| {0} | {1} | {2} | {4} | {5} | {6} | {7} | {8} |", "Company".PadLeft(30, ' '),
                                                         "Project".ToString().PadLeft(30, ' '), 
                                                         "Project Description".Trim().PadLeft(30, ' '),
                                                         "Employee".ToString().Trim().PadLeft(30, ' '), 
                                                         "Ledger Type".Trim().PadLeft(30, ' '),
                                                         "Account".ToString().PadLeft(30, ' '),
                                                         "Ledger Code".Trim().PadLeft(30, ' '), 
                                                         "Amount".ToString().PadLeft(30, ' '),
                                                         "Check".ToString().PadLeft(30, ' ')) +"\n";

                for (int c = 0; c < headerLength; c++)
                {
                    formattedInvoice += "=";
                }

                foreach (InvoiceLine invoiceLine in InvoiceLines)
                {
                    formattedInvoice += "\n" + invoiceLine.WriteOutInvoiceLine();
                }

                formattedInvoice += "\n";

                for (int c = 0; c < headerLength; c++)
                {
                    formattedInvoice += "=";
                }
            }

            return formattedInvoice;
        }
    }
}
