﻿using System.Collections.Generic;

namespace GLHill.Forms
{
    public class InvoiceLine
    {
        private string[] _jobCostParts = new string[4];

        public string Company { get; set; }
        public string Department { get; set; }
        public string Project { get; set; }
        public string ProjectDescription { get; set; }
        public int Employee { get; set; }
        public string LedgerType { get; set; }
        public int Account { get; set; }
        public string LedgerCode { get; set; }
        public decimal Amount { get; set; }
        public int Check { get; set; }
        public List<string> Errors { get; set; }
        public string JobCostParts
        {
            set
            {
                _jobCostParts = value.Split(',');
            }
        }


        public bool HasErrors
        {
            get
            {
                if (Errors.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public string JobCode
        {
            get
            {
                return _jobCostParts[0] ?? "";
            }
        }
        public string PhaseCode
        {
            get
            {
                return _jobCostParts[1] ?? "";
            }
        }
        public string CostList
        {
            get
            {
                return _jobCostParts[2] ?? "";
            }
        }
        public string CostCode
        {
            get
            {
                return _jobCostParts[3] ?? "";
            }
        }

        public InvoiceLine()
        {
            Company = "";
            Project = "";
            ProjectDescription = "";
            Employee = 0;
            LedgerType = "";
            Account = 0;
            LedgerCode = "";
            Amount = 0M;
            Check = 0;
            Errors = new List<string>();
        }

        public InvoiceLine(string company,
                           string project, string projectDescription, int employee, string ledgerType,
                           int account, string ledgerCode, decimal amount, int check)
        {
            Company = company;
            Project = project;
            ProjectDescription = projectDescription;
            Employee = employee;
            LedgerType = ledgerType;
            Account = account;
            LedgerCode = ledgerCode;
            Amount = amount;
            Check = check;
            Errors = new List<string>();
        }

        public string WriteOutInvoiceLine()
        {
            return string.Format("| {0} | {1} | {2} | {4} | {5} | {6} | {7} | {8} |", Company.Trim().PadLeft(30, ' '),
                                 Project.ToString().PadLeft(30, ' '), ProjectDescription.Trim().PadLeft(30, ' '),
                                 Employee.ToString().Trim().PadLeft(30, ' '), LedgerType.Trim().PadLeft(30, ' '),
                                 Account.ToString().PadLeft(30, ' '),
                                 LedgerCode.Trim().PadLeft(30, ' '), Amount.ToString().PadLeft(30, ' '),
                                 Check.ToString().PadLeft(30, ' '));
        }
    }
}
