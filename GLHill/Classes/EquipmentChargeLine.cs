﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GLHill.Classes
{
    public class EquipmentChargeLine
    {
        public enum PAYCATEGORY
        {
            OT,
            REG
        }

        private PAYCATEGORY _payCategory;
        private Employee _employeeName;
        private decimal _hours;
        private string _job;

        public EquipmentChargeLine()
        {
            Errors = new List<string>();
        }

        public PAYCATEGORY PayCategory { get => _payCategory; set => _payCategory = value; }
        public Employee EmployeeName { get => _employeeName; set => _employeeName = value; }
        public decimal Hours { get => _hours; set => _hours = value; }
        public string Job { get => _job; set => _job = value; }
        public List<string> Errors { get; set; }
        public bool HasErrors
        {
            get
            {
                if ((Errors.Count > 0))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
